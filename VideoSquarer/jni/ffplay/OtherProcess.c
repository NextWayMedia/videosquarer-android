#include <jni.h>
#include <stdio.h>
#include <android/bitmap.h>
#include <cpu-features.h>

#include <android/log.h>

#include <avcodec.h>


JNIEXPORT int JNICALL Java_com_videosquarer_squareimage_gui_highencoder_mmaudio_nativeMergeWaveData( JNIEnv* env, jobject thiz, jbyteArray pbDestData, jbyteArray pbSrcData, int cbData1, int cbData2, int step )
{
	int i;
	void *pbSrc;
	void *pbDest;

	if (step == 1)
	{
		pbSrc = (jbyte*) (*env)->GetPrimitiveArrayCritical(env, pbSrcData, 0);
		pbDest = (jbyte*) (*env)->GetPrimitiveArrayCritical(env, pbDestData, 0);
	}
	else if (step == 2)
	{
		pbSrc = (jshort*) (*env)->GetPrimitiveArrayCritical(env, pbSrcData, 0);
		pbDest = (jshort*) (*env)->GetPrimitiveArrayCritical(env, pbDestData, 0);
	}

	if(cbData1 > 0)
	{
		for (i = 0; i<cbData1; i+=step)
		{
			int srcSample, destSample;
			if (step == 1) // and signed
			{
				destSample = *((jbyte*)pbDest + i);
				srcSample = *((jbyte*)pbSrc + i);
				destSample += srcSample;
				if (destSample > 127)	destSample = 127;
				if (destSample < -128)	destSample = -128;
				*((jbyte*)pbDest + i) = destSample;
			}
			else if (step == 2) // and signed
			{
				destSample = *((jshort*)pbDest + (i / 2));
				srcSample = *((jshort*)pbSrc + (i / 2));
				destSample += srcSample;
				if (destSample > 32767)	 destSample = 32767;
				if (destSample < -32768) destSample = -32768;

				*((jshort*)pbDest + (i / 2)) = (jshort)destSample;
			}
		}
	}
	else
		memcpy(pbDest, pbSrc, cbData2);

	(*env)->ReleasePrimitiveArrayCritical(env, pbSrcData, pbSrc, 0);
	(*env)->ReleasePrimitiveArrayCritical(env, pbDestData, pbDest, 0);

	return cbData1;
}

JNIEXPORT int JNICALL Java_com_videosquarer_squareimage_gui_highencoder_mmaudio_nativeProcessVolume( JNIEnv* env, jobject thiz, jbyteArray pbDestData, int cbData, int step, int nVolume )
{
	int i;
	void *pbDest;

	if (step == 1)
	{
		pbDest = (jbyte*) (*env)->GetPrimitiveArrayCritical(env, pbDestData, 0);
	}
	else if (step == 2)
	{
		pbDest = (jshort*) (*env)->GetPrimitiveArrayCritical(env, pbDestData, 0);
	}
	else
	{
		return -1;
	}
	
	for (i = 0; i<cbData; i+=step)
	{
		int destSample;
		if (step == 1) // and signed
		{
			destSample = *((int8_t*)pbDest + i);
			destSample = (int) (nVolume * destSample / 100);

			if (destSample > 127)	destSample = 127;
			if (destSample < -128)	destSample = -128;
			*((int8_t*)pbDest + i) = destSample;
		}
		else if (step == 2) // and signed
		{
			destSample = *((int16_t*)pbDest + (i / 2));
			destSample = (int) (nVolume * destSample / 100);

			if (destSample > 32767)	 destSample = 32767;
			if (destSample < -32768) destSample = -32768;

			*((int16_t*)pbDest + (i / 2)) = (int16_t)destSample;
		}
	}

	(*env)->ReleasePrimitiveArrayCritical(env, pbDestData, pbDest, 0);

	return cbData;
}
