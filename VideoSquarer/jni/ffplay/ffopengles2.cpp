#include "ffopengles2.h"

GLuint g_VertexShader, g_ImageVertexShader, g_BlurVertexShader = 0;
GLuint g_FragmentShader, g_ImageFragmentShader, g_BlurFragmentShader = 0;

GLuint g_Program, g_ImageProgram, g_BlurProgram = 0;

GLuint g_BlurFirstProgram = 0, g_BlurSecondProgram = 0;
GLint filterPositionAttribute, filterTextureCoordinateAttribute;
GLint secondFilterPositionAttribute, secondFilterTextureCoordinateAttribute;

GLint filterInputTextureUniform, secondFilterInputTextureUniform;
GLint verticalPassTexelWidthOffsetUniform, verticalPassTexelHeightOffsetUniform, horizontalPassTexelWidthOffsetUniform, horizontalPassTexelHeightOffsetUniform;

GLuint g_vPositionHandle, g_vTexPos;
GLuint g_vImagePositionHandle, g_vImageTexPos;
GLuint g_vBlurPositionHandle, g_vBlurTexPos;

GLuint g_firstFrameBuffer = 0, g_firstTexture = 0;
GLuint g_secondFrameBuffer = 0, g_secondTexture = 0;

GLint g_yLoc, g_uLoc, g_vLoc, g_vImageLoc, g_yBlurLoc, g_uBlurLoc, g_vBlurLoc;
GLuint g_maskTexture = 0;
int g_maskWidth, g_maskHeight;

GLuint	g_FrameBuff;
GLuint	g_FrameTexture;
bool	g_FrameCreated = false;

extern int g_nTextureWidth, g_nTextureHeight;
extern int g_isBlur = 0;
extern int g_blurRadius = 1;
extern char *g_blurOpVertexShaderString = NULL;
extern char *g_blurOpFragmentShaderString = NULL;

unsigned char *maskImage = NULL;

static const char g_VertexShaderStr[] = 
"attribute vec4 vPosition;    \n"  
"attribute vec2 a_texCoord;   \n"  
"varying vec2 tc;     \n"  
"void main()                  \n"  
"{                            \n"  
"   gl_Position = vPosition;  \n"  
"   tc = a_texCoord;  \n"  
"}                            \n";  

static const char g_FragmentShaderStr[] = 
"precision mediump float;\n"  
"uniform sampler2D tex_y;                 \n"  
"uniform sampler2D tex_u;                 \n"  
"uniform sampler2D tex_v;                 \n"  
"varying vec2 tc;                         \n"  
"void main()                                  \n"  
"{                                            \n"  
"float y = texture2D(tex_y, tc).r; \n"
"float u = texture2D( tex_u, tc ).r; \n"
"float v = texture2D( tex_v, tc ).r; \n"
"y = 1.1643 * ( y - 0.0625 ); \n"
"u = u - 0.5; \n"
"v = v - 0.5; \n"
"float r = y + 1.5958 * v; \n"
"float g = y - 0.39173 * u - 0.81290 * v; \n"
"float b = y + 2.017 * u; \n"
"gl_FragColor = vec4(r,g,b, 1.0); \n"
"}                                            \n";  

static const char g_ImageVertexShaderStr[] =
"attribute vec4 vPosition;    \n"
"attribute vec2 a_texCoord;   \n"
"varying vec2 tc;     \n"
"void main()                  \n"
"{                            \n"
"   gl_Position = vPosition;  \n"
"   tc = a_texCoord;  \n"
"}                            \n";

static const char g_ImageFragmentShaderStr[] =
"precision mediump float;\n"
"uniform sampler2D effect_tex;                 \n"
"varying vec2 tc;                         \n"
"void main()                                  \n"
"{                                            \n"
"vec4 color = texture2D(effect_tex, tc); \n"
"gl_FragColor = vec4(color.r, color.g, color.b, color.a); \n"
"}                                            \n";


static const char g_BlurVertexShaderStr[] =
"attribute vec4 position;\n"
"attribute vec4 inputTextureCoordinate;\n"
"varying vec2 v_texCoord;\n"
"void main()		\n"
"{					\n"
"gl_Position=vPosition;\n"
"v_texCoord=inputTextureCoordinate.xy; \n"
"}						\n";

static const char g_BlurFragmentShaderStr[] =
"precision mediump float;\n"
"uniform sampler2D inputImageTexture; \n"
"uniform float texelWidthOffset; \n"
"uniform float texelHeightOffset; \n"
"varying vec2 v_texCoord;\n"
"float blurSize = 1.0 / 640.0; \n"
"void main(){\n"
"vec4 color = vec4(0.0); \n"

"color += texture2D( inputImageTexture, vec2(v_texCoord.x - 4.0*blurSize, v_texCoord.y - 4.0*blurSize)) * 0.05; \n"
"color += texture2D( inputImageTexture, vec2(v_texCoord.x - 3.0*blurSize, v_texCoord.y - 3.0*blurSize)) * 0.09; \n"
"color += texture2D( inputImageTexture, vec2(v_texCoord.x - 2.0*blurSize, v_texCoord.y - 2.0*blurSize)) * 0.12; \n"
"color += texture2D( inputImageTexture, vec2(v_texCoord.x - blurSize, v_texCoord.y - blurSize)) * 0.15; \n"
"color += texture2D( inputImageTexture, v_texCoord) * 0.16; \n"
"color += texture2D( inputImageTexture, vec2(v_texCoord.x + blurSize, v_texCoord.y + blurSize)) * 0.15; \n"
"color += texture2D( inputImageTexture, vec2(v_texCoord.x + 2.0*blurSize, v_texCoord.y + 2.0*blurSize)) * 0.12; \n"
"color += texture2D( inputImageTexture, vec2(v_texCoord.x + 3.0*blurSize, v_texCoord.y + 3.0*blurSize)) * 0.09; \n"
"color += texture2D( inputImageTexture, vec2(v_texCoord.x + 4.0*blurSize, v_texCoord.y + 4.0*blurSize)) * 0.05; \n"

"gl_FragColor = vec4(color.r, color.g, color.b, 1.0); \n"
"}\n";

void OnFragmentUnifom(FrameBuff* frame, int nWidth, int nHeight)
{
	if(!frame->yTexture)
		glGenTextures(1, &frame->yTexture);
	glBindTexture(GL_TEXTURE_2D, frame->yTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, nWidth, nHeight, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame->pBuff);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );  
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ); 

	if(!frame->uTexture)
		glGenTextures(1, &frame->uTexture);
	glBindTexture(GL_TEXTURE_2D, frame->uTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, nWidth / 2, nHeight / 2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame->pBuff + nWidth * nHeight);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ); 

	if(!frame->vTexture)
		glGenTextures(1, &frame->vTexture);
	glBindTexture(GL_TEXTURE_2D, frame->vTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, nWidth / 2, nHeight / 2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame->pBuff + nWidth * nHeight * 5 / 4);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );  
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ); 

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, frame->yTexture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, frame->uTexture);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, frame->vTexture);

	glUniform1i(g_yLoc, 0);
	glUniform1i(g_uLoc, 1);
	glUniform1i(g_vLoc, 2);
}

void OnBlurFragmentUnifom(FrameBuff* frame, int nWidth, int nHeight)
{
	if(!frame->yTexture)
		glGenTextures(1, &frame->yTexture);
	glBindTexture(GL_TEXTURE_2D, frame->yTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, nWidth, nHeight, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame->pBuff);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

	if(!frame->uTexture)
		glGenTextures(1, &frame->uTexture);
	glBindTexture(GL_TEXTURE_2D, frame->uTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, nWidth / 2, nHeight / 2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame->pBuff + nWidth * nHeight);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

	if(!frame->vTexture)
		glGenTextures(1, &frame->vTexture);
	glBindTexture(GL_TEXTURE_2D, frame->vTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, nWidth / 2, nHeight / 2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame->pBuff + nWidth * nHeight * 5 / 4);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, frame->yTexture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, frame->uTexture);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, frame->vTexture);

	glUniform1i(g_yBlurLoc, 0);
	glUniform1i(g_uBlurLoc, 1);
	glUniform1i(g_vBlurLoc, 2);
}

GLuint LoadShader(GLenum type, const char *shaderSrc)
{
	GLuint shader;
	GLint compiled;

	// Create the shader object
	shader = glCreateShader(type);

	if(shader == 0)
		return 0;

	// Load the shader source
	glShaderSource(shader, 1, &shaderSrc, NULL);

	//Compile the shader
	glCompileShader(shader);

	// Check the compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if(!compiled)
	{
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}

GLuint CreateProgram(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint programObject;
	GLint linked;

	// Create the program object
	programObject = glCreateProgram();

	if(programObject == 0)
		return 0;

	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// Link the program
	glLinkProgram(programObject);

	// Check the link status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

	if(!linked)
	{
		glDeleteProgram(programObject);
		return 0;
	}
	return programObject;
}

int GetGLBpp()
{
    GLint readType, readFormat;
    glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_TYPE, &readType);
    glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_FORMAT, &readFormat);
    int bpp = 4;
    if(readType == GL_UNSIGNED_SHORT_5_6_5 && readFormat == GL_RGB)
        bpp = 2;
    else if(readType == GL_UNSIGNED_BYTE && readFormat == GL_RGBA)
        bpp = 4;
    return bpp;
}

void GetGLColorBuffer(char* pGLBuffer, int nGLScreenWidth, int nGLScreenHeight)
{
	GLint readType, readFormat;
	glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_TYPE, &readType);
	glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_FORMAT, &readFormat);
	glReadPixels(0, 0, nGLScreenWidth, nGLScreenHeight, readFormat, readType, pGLBuffer);
//	glReadPixels(0, 0, nGLScreenWidth, nGLScreenHeight, GL_RGBA, GL_UNSIGNED_BYTE, pGLBuffer);
}

int _getGLScreenBuffer(char* pGLBuffer, int nGLScreenWidth, int nGLScreenHeight)
{
	GLint readType, readFormat;
	GLubyte *pixels;

	glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_TYPE, &readType);
	glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_FORMAT, &readFormat);

	glReadPixels(0, 0, nGLScreenWidth, nGLScreenHeight, readFormat, readType, pGLBuffer);

	return 0;
}

void renderMaskImage(unsigned char *pImageBuffer, int width, int height)
{
	if (maskImage == NULL)
	{
		maskImage = (unsigned char*)malloc(width*height*4);
	}
	memcpy(maskImage, pImageBuffer, width*height*4);
	if(g_maskTexture != 0)
		glDeleteTextures(1, &g_maskTexture);
	glGenTextures(1, &g_maskTexture);
	g_maskWidth = width;
	g_maskHeight = height;
	__android_log_print(ANDROID_LOG_DEBUG, "test", "renderMaskImage");
}

void OnGLESRender(FrameBuff** pVideoBuff, ImageBuff** pImageBuff)
{
	int i;

	glClearColor(0, 0, 0, 1.0f);
	glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	float texs[8] = {0.f, 0.f, 0.f, 1.f, 1.f, 0.f, 1.f, 1.f}, vtTri[8] = {-1.f, -1.f, -1.f, 1.f, 1.f, -1.f, 1.f, 1.f}, tmp1, tmp2;

	// Draw Images

	if(!g_isBlur)
	{
		glUseProgram(g_ImageProgram);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glVertexAttribPointer(g_vImagePositionHandle, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
		glEnableVertexAttribArray(g_vImagePositionHandle);
		glVertexAttribPointer(g_vImageTexPos, 2, GL_FLOAT, GL_FALSE, 0, texs);
		glEnableVertexAttribArray(g_vImageTexPos);

		for (i=0; i<OPEN_LIMIT; i++)
		{
			if (pImageBuff[i] != NULL)
			{
				texs[0] = texs[2] = (float)pImageBuff[i]->framePosX / (float)pImageBuff[i]->width;
				texs[4] = texs[6] = (float)(pImageBuff[i]->framePosX + pImageBuff[i]->drawWidth) / (float)pImageBuff[i]->width;
				texs[1] = texs[5] = (float)pImageBuff[i]->framePosY / (float)pImageBuff[i]->height;
				texs[3] = texs[7] = (float)(pImageBuff[i]->framePosY + pImageBuff[i]->drawHeight) / (float)pImageBuff[i]->height;

				vtTri[0] = vtTri[2] = ((float)pImageBuff[i]->showPosX / (float)g_nTextureWidth) * 2 - 1;
				vtTri[4] = vtTri[6] = ((float)(pImageBuff[i]->showPosX + pImageBuff[i]->frameWidth) / (float)g_nTextureWidth) * 2 - 1;
				vtTri[1] = vtTri[5] = 1.0f - ((float)pImageBuff[i]->showPosY / (float)g_nTextureHeight) * 2;
				vtTri[3] = vtTri[7] = 1.0f - ((float)(pImageBuff[i]->showPosY + pImageBuff[i]->frameWidth * pImageBuff[i]->drawHeight
						/ pImageBuff[i]->drawWidth) / (float)g_nTextureHeight) * 2;

				if (pImageBuff[i]->pBuff != NULL)
				{
					if(pImageBuff[i]->texture != 0)
						glDeleteTextures(1, &pImageBuff[i]->texture);
					glGenTextures(1, &pImageBuff[i]->texture);
					glBindTexture(GL_TEXTURE_2D, pImageBuff[i]->texture);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pImageBuff[i]->width, pImageBuff[i]->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pImageBuff[i]->pBuff);
					free(pImageBuff[i]->pBuff);
					pImageBuff[i]->pBuff = NULL;
				}

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, pImageBuff[i]->texture);
				glUniform1i(g_vImageLoc, 0);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			}
		}
		glDisable(GL_BLEND);
	}
	else // g_isBlur is 1
	{
		if(g_BlurFirstProgram)
			glDeleteProgram(g_BlurFirstProgram);
		if(g_BlurSecondProgram)
			glDeleteProgram(g_BlurSecondProgram);

		if(g_BlurVertexShader)
			glDeleteShader(g_BlurVertexShader);
		if(g_BlurFragmentShader)
			glDeleteShader(g_BlurFragmentShader);

		g_BlurVertexShader = LoadShader(GL_VERTEX_SHADER, g_blurOpVertexShaderString);
		g_BlurFragmentShader = LoadShader(GL_FRAGMENT_SHADER, g_blurOpFragmentShaderString);

		g_BlurFirstProgram = CreateProgram(g_BlurVertexShader, g_BlurFragmentShader);

		filterPositionAttribute = glGetAttribLocation(g_BlurFirstProgram, "position");
		filterTextureCoordinateAttribute = glGetAttribLocation(g_BlurFirstProgram, "inputTextureCoordinate");

		filterInputTextureUniform = glGetUniformLocation(g_BlurFirstProgram, "inputImageTexture");
		verticalPassTexelHeightOffsetUniform = glGetUniformLocation(g_BlurFirstProgram, "texelHeightOffset");
		verticalPassTexelWidthOffsetUniform = glGetUniformLocation(g_BlurFirstProgram, "texelWidthOffset");

		g_BlurSecondProgram = CreateProgram(g_BlurVertexShader, g_BlurFragmentShader);

		secondFilterPositionAttribute = glGetAttribLocation(g_BlurSecondProgram, "position");
		secondFilterTextureCoordinateAttribute = glGetAttribLocation(g_BlurSecondProgram, "inputTextureCoordinate");

		secondFilterInputTextureUniform = glGetUniformLocation(g_BlurSecondProgram, "inputImageTexture");
		horizontalPassTexelHeightOffsetUniform = glGetUniformLocation(g_BlurSecondProgram, "texelHeightOffset");
		horizontalPassTexelWidthOffsetUniform = glGetUniformLocation(g_BlurSecondProgram, "texelWidthOffset");

		for (i=0; i<OPEN_LIMIT; i++)
		{
			if (pVideoBuff[i] != NULL)
			{
				int width = pVideoBuff[i]->width, height = pVideoBuff[i]->height;

				//glActiveTexture(GL_TEXTURE3);

				if(!g_firstFrameBuffer)
				{
					glGenFramebuffers(1, &g_firstFrameBuffer);
					glBindFramebuffer(GL_FRAMEBUFFER, g_firstFrameBuffer);
					glGenTextures(1, &g_firstTexture);
					glBindTexture(GL_TEXTURE_2D, g_firstTexture);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
					glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
					glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_firstTexture, 0);
				}

				glBindFramebuffer(GL_FRAMEBUFFER, g_firstFrameBuffer);
				glViewport(0, 0, width, height);
//				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//				glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

				glUseProgram(g_Program);
				glVertexAttribPointer(g_vPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
				glEnableVertexAttribArray(g_vPositionHandle);
				glVertexAttribPointer(g_vTexPos, 2, GL_FLOAT, GL_FALSE, 0, texs);
				glEnableVertexAttribArray(g_vTexPos);

				OnFragmentUnifom(pVideoBuff[i], width, height);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				//__android_log_print(ANDROID_LOG_DEBUG, "cyj", "radius: %d", g_blurRadius);
				if(g_blurRadius > 1)
				{
					/* apply vertical blur effect */
					/* Render first blur */
					//glActiveTexture(GL_TEXTURE4);
					if(!g_secondFrameBuffer)
					{
						glGenFramebuffers(1, &g_secondFrameBuffer);
						glBindFramebuffer(GL_FRAMEBUFFER, g_secondFrameBuffer);
						glGenTextures(1, &g_secondTexture);
						glBindTexture(GL_TEXTURE_2D, g_secondTexture);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
						glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
						glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
						glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
						glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_secondTexture, 0);

					}

					glBindFramebuffer(GL_FRAMEBUFFER, g_secondFrameBuffer);
					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

					glUseProgram(g_BlurFirstProgram);
					glVertexAttribPointer(filterPositionAttribute, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
					glEnableVertexAttribArray(filterPositionAttribute);
					glVertexAttribPointer(filterTextureCoordinateAttribute, 2, GL_FLOAT, GL_FALSE, 0, texs);
					glEnableVertexAttribArray(filterTextureCoordinateAttribute);

					glActiveTexture(GL_TEXTURE4);
					glBindTexture(GL_TEXTURE_2D, g_firstTexture);
					glUniform1i(filterInputTextureUniform, 4);

					glUniform1f(verticalPassTexelWidthOffsetUniform, 0.f);
					glUniform1f(verticalPassTexelHeightOffsetUniform, 1.f / (float)height);

					glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

					/* Apply Horizontal Blur Effect */

					glBindFramebuffer(GL_FRAMEBUFFER, g_firstFrameBuffer);
//					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//					glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

					glUseProgram(g_BlurSecondProgram);
					glVertexAttribPointer(secondFilterPositionAttribute, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
					glEnableVertexAttribArray(secondFilterPositionAttribute);
					glVertexAttribPointer(secondFilterTextureCoordinateAttribute, 2, GL_FLOAT, GL_FALSE, 0, texs);
					glEnableVertexAttribArray(secondFilterTextureCoordinateAttribute);

					glActiveTexture(GL_TEXTURE4);
					glBindTexture(GL_TEXTURE_2D, g_secondTexture);
					glUniform1i(secondFilterInputTextureUniform, 4);

					glUniform1f(horizontalPassTexelWidthOffsetUniform, 1.f / (float)width);
					glUniform1f(horizontalPassTexelHeightOffsetUniform, 0.f);

					glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				}
				/* Draw result blur image */
				//glActiveTexture(GL_TEXTURE5);
				glBindFramebuffer(GL_FRAMEBUFFER, g_FrameBuff);
			    glViewport(0, 0, g_nTextureWidth, g_nTextureHeight);
//			    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//			    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
				glUseProgram(g_ImageProgram);

				glVertexAttribPointer(g_vImagePositionHandle, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
				glEnableVertexAttribArray(g_vImagePositionHandle);
				glVertexAttribPointer(g_vImageTexPos, 2, GL_FLOAT, GL_FALSE, 0, texs);
				glEnableVertexAttribArray(g_vImageTexPos);

//				glUseProgram(g_BlurProgram);
//				glVertexAttribPointer(g_vBlurPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
//				glEnableVertexAttribArray(g_vBlurPositionHandle);
//				glVertexAttribPointer(g_vBlurTexPos, 2, GL_FLOAT, GL_FALSE, 0, texs);
//				glEnableVertexAttribArray(g_vBlurTexPos);

				if(pVideoBuff[i]->rotation % 180)
				{
					width = pVideoBuff[i]->height;
					height = pVideoBuff[i]->width;
				}

				int small_size = 0;
				if(width > height)
				{
					small_size = height * 8 / 10;
				}
				else if(height > width)
				{
					small_size = width * 8 / 10;
				}
				else
				{
					small_size = width * 8 / 10;
				}

				float framePosX = (width - small_size) / 2;
				float framePosY = (height - small_size) / 2;
				float thumbWidth = width, thumbHeight = height;

				float drawWidth = small_size, drawHeight = small_size;
				float showPosX = 0, showPosY = 0;
				float frameWidth = g_nTextureWidth;

				//smallBitmap = Bitmap.createBitmap(rb, (width - small_size) / 2, (height - small_size) / 2, small_size, small_size);
				texs[0] = texs[2] = framePosX / thumbWidth;
				texs[4] = texs[6] = (framePosX + drawWidth) / thumbWidth;
				texs[1] = texs[5] = framePosY / thumbHeight;
				texs[3] = texs[7] = (framePosY + drawHeight) / thumbHeight;

				if (pVideoBuff[i]->rotation == 270)
				{
					tmp1 = texs[0]; tmp2 = texs[1];
					texs[0] = 1.0 - texs[3]; texs[1] = texs[2];
					texs[2] = 1.0 - texs[7]; texs[3] = texs[6];
					texs[6] = 1.0 - texs[5]; texs[7] = texs[4];
					texs[4] = 1.0 - tmp2; texs[5] = tmp1;
				}
				else if (pVideoBuff[i]->rotation == 180)
				{
					tmp1 = texs[0]; tmp2 = texs[1];
					texs[0] = 1.0 - texs[6]; texs[1] = 1.0 - texs[7];
					texs[6] = 1.0 - tmp1; texs[7] = 1.0 - tmp2;
					tmp1 = texs[2]; tmp2 = texs[3];
					texs[2] = 1.0 - texs[4]; texs[3] = 1.0 - texs[5];
					texs[4] = 1.0 - tmp1; texs[5] = 1.0 - tmp2;
				}
				else if (pVideoBuff[i]->rotation == 90)
				{
					tmp1 = texs[0]; tmp2 = texs[1];
					texs[0] = texs[5]; texs[1] = 1.0 - texs[4];
					texs[4] = texs[7]; texs[5] = 1.0 - texs[6];
					texs[6] = texs[3]; texs[7] = 1.0 - texs[2];
					texs[2] = tmp2; texs[3] = 1.0 - tmp1;
				}

				vtTri[0] = vtTri[2] = (showPosX / (float)g_nTextureWidth) * 2 - 1;
				vtTri[4] = vtTri[6] = ((float)(showPosX + frameWidth) / (float)g_nTextureWidth) * 2 - 1;
				vtTri[1] = vtTri[5] = 1.0f - (showPosY / (float)g_nTextureHeight) * 2;
				vtTri[3] = vtTri[7] = 1.0f - ((float)(showPosY + frameWidth * drawHeight / drawWidth) / (float)g_nTextureHeight) * 2;

				if (pVideoBuff[i]->rotation == 270)
				{
					tmp1 = vtTri[0]; tmp2 = vtTri[1];
					vtTri[0] = vtTri[2]; vtTri[1] = vtTri[3];
					vtTri[2] = vtTri[6]; vtTri[3] = vtTri[7];
					vtTri[6] = vtTri[4]; vtTri[7] = vtTri[5];
					vtTri[4] = tmp1; vtTri[5] = tmp2;
				}
				else if (pVideoBuff[i]->rotation == 180)
				{
					tmp1 = vtTri[0]; tmp2 = vtTri[1];
					vtTri[0] = vtTri[6]; vtTri[1] = vtTri[7];
					vtTri[6] = tmp1; vtTri[7] = tmp2;
					tmp1 = vtTri[2]; tmp2 = vtTri[3];
					vtTri[2] = vtTri[4]; vtTri[3] = vtTri[5];
					vtTri[4] = tmp1; vtTri[5] = tmp2;
				}
				else if (pVideoBuff[i]->rotation == 90)
				{
					tmp1 = vtTri[0]; tmp2 = vtTri[1];
					vtTri[0] = vtTri[4]; vtTri[1] = vtTri[5];
					vtTri[4] = vtTri[6]; vtTri[5] = vtTri[7];
					vtTri[6] = vtTri[2]; vtTri[7] = vtTri[3];
					vtTri[2] = tmp1; vtTri[3] = tmp2;
				}

				//OnBlurFragmentUnifom(pVideoBuff[i], pVideoBuff[i]->width, pVideoBuff[i]->height);
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, g_firstTexture);
				glUniform1i(g_vImageLoc, 3);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			}
		}
	}

	// Draw Videos
	glUseProgram(g_Program);
	glVertexAttribPointer(g_vPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
	glEnableVertexAttribArray(g_vPositionHandle);
	glVertexAttribPointer(g_vTexPos, 2, GL_FLOAT, GL_FALSE, 0, texs);
	glEnableVertexAttribArray(g_vTexPos);

	for (i=0; i<OPEN_LIMIT; i++)
	{
		if (pVideoBuff[i] != NULL)
		{
			texs[0] = texs[2] = (float)pVideoBuff[i]->framePosX / (float)pVideoBuff[i]->thumbWidth;
			texs[4] = texs[6] = (float)(pVideoBuff[i]->framePosX + pVideoBuff[i]->drawWidth) / (float)pVideoBuff[i]->thumbWidth;
			texs[1] = texs[5] = (float)pVideoBuff[i]->framePosY / (float)pVideoBuff[i]->thumbHeight;
			texs[3] = texs[7] = (float)(pVideoBuff[i]->framePosY + pVideoBuff[i]->drawHeight) / (float)pVideoBuff[i]->thumbHeight;

			if (pVideoBuff[i]->rotation == 270)
			{
				tmp1 = texs[0]; tmp2 = texs[1];
				texs[0] = 1.0 - texs[3]; texs[1] = texs[2];
				texs[2] = 1.0 - texs[7]; texs[3] = texs[6];
				texs[6] = 1.0 - texs[5]; texs[7] = texs[4];
				texs[4] = 1.0 - tmp2; texs[5] = tmp1;
			}
			else if (pVideoBuff[i]->rotation == 180)
			{
				tmp1 = texs[0]; tmp2 = texs[1];
				texs[0] = 1.0 - texs[6]; texs[1] = 1.0 - texs[7];
				texs[6] = 1.0 - tmp1; texs[7] = 1.0 - tmp2;
				tmp1 = texs[2]; tmp2 = texs[3];
				texs[2] = 1.0 - texs[4]; texs[3] = 1.0 - texs[5];
				texs[4] = 1.0 - tmp1; texs[5] = 1.0 - tmp2;
			}
			else if (pVideoBuff[i]->rotation == 90)
			{
				tmp1 = texs[0]; tmp2 = texs[1];
				texs[0] = texs[5]; texs[1] = 1.0 - texs[4];
				texs[4] = texs[7]; texs[5] = 1.0 - texs[6];
				texs[6] = texs[3]; texs[7] = 1.0 - texs[2];
				texs[2] = tmp2; texs[3] = 1.0 - tmp1;
			}

			vtTri[0] = vtTri[2] = ((float)pVideoBuff[i]->showPosX / (float)g_nTextureWidth) * 2 - 1;
			vtTri[4] = vtTri[6] = ((float)(pVideoBuff[i]->showPosX + pVideoBuff[i]->frameWidth) / (float)g_nTextureWidth) * 2 - 1;
			vtTri[1] = vtTri[5] = 1.0f - ((float)pVideoBuff[i]->showPosY / (float)g_nTextureHeight) * 2;
			vtTri[3] = vtTri[7] = 1.0f - ((float)(pVideoBuff[i]->showPosY + pVideoBuff[i]->frameWidth * pVideoBuff[i]->drawHeight
									/ pVideoBuff[i]->drawWidth) / (float)g_nTextureHeight) * 2;

			if (pVideoBuff[i]->rotation == 270)
			{
				tmp1 = vtTri[0]; tmp2 = vtTri[1];
				vtTri[0] = vtTri[2]; vtTri[1] = vtTri[3];
				vtTri[2] = vtTri[6]; vtTri[3] = vtTri[7];
				vtTri[6] = vtTri[4]; vtTri[7] = vtTri[5];
				vtTri[4] = tmp1; vtTri[5] = tmp2;
			}
			else if (pVideoBuff[i]->rotation == 180)
			{
				tmp1 = vtTri[0]; tmp2 = vtTri[1];
				vtTri[0] = vtTri[6]; vtTri[1] = vtTri[7];
				vtTri[6] = tmp1; vtTri[7] = tmp2;
				tmp1 = vtTri[2]; tmp2 = vtTri[3];
				vtTri[2] = vtTri[4]; vtTri[3] = vtTri[5];
				vtTri[4] = tmp1; vtTri[5] = tmp2;
			}
			else if (pVideoBuff[i]->rotation == 90)
			{
				tmp1 = vtTri[0]; tmp2 = vtTri[1];
				vtTri[0] = vtTri[4]; vtTri[1] = vtTri[5];
				vtTri[4] = vtTri[6]; vtTri[5] = vtTri[7];
				vtTri[6] = vtTri[2]; vtTri[7] = vtTri[3];
				vtTri[2] = tmp1; vtTri[3] = tmp2;
			}
			OnFragmentUnifom(pVideoBuff[i], pVideoBuff[i]->width, pVideoBuff[i]->height);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
	}

	/* Draw Mask Image */

	if (maskImage)
	{
	    glBindTexture(GL_TEXTURE_2D, g_maskTexture);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, g_maskWidth, g_maskHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, maskImage);
	    free(maskImage);
	    maskImage = NULL;
	}

	if(g_maskTexture > 0)
	{
		texs[0] = 0; texs[1] = 1; texs[2] = 0; texs[3] = 0; texs[4] = 1; texs[5] = 1; texs[6] = 1; texs[7] = 0;
		vtTri[0] = -1; vtTri[1] = -1; vtTri[2] = -1; vtTri[3] = 1; vtTri[4] = 1; vtTri[5] = -1; vtTri[6] = 1; vtTri[7] = 1;

		glUseProgram(g_ImageProgram);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glVertexAttribPointer(g_vImagePositionHandle, 2, GL_FLOAT, GL_FALSE, 0, vtTri);
		glEnableVertexAttribArray(g_vImagePositionHandle);
		glVertexAttribPointer(g_vImageTexPos, 2, GL_FLOAT, GL_FALSE, 0, texs);
		glEnableVertexAttribArray(g_vImageTexPos);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, g_maskTexture);

		glUniform1i(g_vImageLoc, 0);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glDisable(GL_BLEND);
	}
}

void InitShader()
{
	glClearColor(0, 0, 0, 1);
	g_VertexShader = LoadShader(GL_VERTEX_SHADER, g_VertexShaderStr);
	g_FragmentShader = LoadShader(GL_FRAGMENT_SHADER, g_FragmentShaderStr);
	g_Program = CreateProgram(g_VertexShader, g_FragmentShader);

	g_ImageVertexShader = LoadShader(GL_VERTEX_SHADER, g_ImageVertexShaderStr);
	g_ImageFragmentShader = LoadShader(GL_FRAGMENT_SHADER, g_ImageFragmentShaderStr);
	g_ImageProgram = CreateProgram(g_ImageVertexShader, g_ImageFragmentShader);

	g_BlurVertexShader = LoadShader(GL_VERTEX_SHADER, g_BlurVertexShaderStr);
	g_BlurFragmentShader = LoadShader(GL_FRAGMENT_SHADER, g_BlurFragmentShaderStr);
	g_BlurProgram = CreateProgram(g_BlurVertexShader, g_BlurFragmentShader);

	g_vPositionHandle = glGetAttribLocation(g_Program, "vPosition");
	g_vTexPos = glGetAttribLocation(g_Program, "a_texCoord");
	g_yLoc = glGetUniformLocation(g_Program, "tex_y");
	g_uLoc = glGetUniformLocation(g_Program, "tex_u");
	g_vLoc = glGetUniformLocation(g_Program, "tex_v");

	g_vImagePositionHandle = glGetAttribLocation(g_ImageProgram, "vPosition");
	g_vImageTexPos = glGetAttribLocation(g_ImageProgram, "a_texCoord");
	g_vImageLoc = glGetUniformLocation(g_ImageProgram, "effect_tex");

	g_vBlurPositionHandle = glGetAttribLocation(g_BlurProgram, "vPosition");
	g_vBlurTexPos = glGetAttribLocation(g_BlurProgram, "a_texCoord");
	g_yBlurLoc = glGetUniformLocation(g_BlurProgram, "tex_y");
	g_uBlurLoc = glGetUniformLocation(g_BlurProgram, "tex_u");
	g_vBlurLoc = glGetUniformLocation(g_BlurProgram, "tex_v");

	g_isBlur = false;
	__android_log_print(ANDROID_LOG_DEBUG, "test", "init shader");
}

void ExitShader()
{
	int i;

	glDeleteProgram(g_Program);
	glDeleteShader(g_VertexShader);
	glDeleteShader(g_FragmentShader);

	glDeleteProgram(g_ImageProgram);
	glDeleteShader(g_ImageVertexShader);
	glDeleteShader(g_ImageFragmentShader);

	if(g_BlurProgram)
	{
		glDeleteProgram(g_BlurProgram);
		glDeleteShader(g_BlurVertexShader);
		glDeleteShader(g_BlurFragmentShader);

		g_BlurProgram = 0;
		g_BlurVertexShader = 0;
		g_BlurFragmentShader = 0;
	}

	if (g_FrameCreated)
	{
		glDeleteTextures(1, &g_FrameTexture);
		glDeleteFramebuffers(1, &g_FrameBuff);
	}

	if(g_blurOpVertexShaderString)
	{
		free(g_blurOpVertexShaderString);
		g_blurOpVertexShaderString = NULL;
	}

	if(g_blurOpFragmentShaderString)
	{
		free(g_blurOpFragmentShaderString);
		g_blurOpFragmentShaderString = NULL;
	}
	if(g_maskTexture != 0)
		glDeleteTextures(1, &g_maskTexture);

	__android_log_print(ANDROID_LOG_DEBUG, "test", "exit shader");
}

void SetBlurParams(int blurRadius, char *vertexShader, char*fragmentShader)
{
	g_blurRadius = blurRadius;

	if(g_blurOpVertexShaderString)
	{
		free(g_blurOpVertexShaderString);
		g_blurOpVertexShaderString = NULL;
	}

	if(g_blurOpFragmentShaderString)
	{
		free(g_blurOpFragmentShaderString);
		g_blurOpFragmentShaderString = NULL;
	}

	g_blurOpVertexShaderString = vertexShader;
	g_blurOpFragmentShaderString = fragmentShader;

}

void CreateFrameTexture(int nWidth, int nHeight)
{
	glGenFramebuffers(1, &g_FrameBuff);
	glBindFramebuffer(GL_FRAMEBUFFER, g_FrameBuff);
	glGenTextures(1, &g_FrameTexture);
	glBindTexture(GL_TEXTURE_2D, g_FrameTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, nWidth, nHeight, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, NULL);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_FrameTexture, 0);

	g_FrameCreated = true;
	glBindFramebuffer(GL_FRAMEBUFFER, g_FrameBuff);
}
