#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "ffplay_common.h"


void		media_queue_init(MediaQueue *q);
void		media_queue_uninit( MediaQueue *q );
void		media_queue_flush(MediaQueue *q);
int			media_queue_empty_index(MediaQueue *q);
void		media_queue_push(MediaQueue *q, struct AVFrame * frame);
int			media_queue_pop(MediaQueue * q, struct AVFrame ** ppFrame);

int			media_queue_get(MediaQueue * q, struct AVFrame ** ppFrame, int index);
int			media_queue_get_with_range(MediaQueue * q, struct AVFrame ** ppFrame, int index, int64_t* ptsmin, int64_t* ptsmax);

int			is_media_queue_full(MediaQueue *q);


#endif //__QUEUE_H__
