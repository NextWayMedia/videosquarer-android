#ifndef _BLUR_H_
#define _BLUR_H_


#include <jni.h>

#define			BITCOUNT		32
#define			BYTEPERPIXEL	4

int		gen_convolve_matrix( float radius, float* cmatrix, int matrix_length );

jobject BitmapLock( JNIEnv* env, jobject thiz, jobject pBitmap, void** pBitmapRefPixelBuffer );
void	BitmapUnlock( JNIEnv* env, jobject thiz, jobject pBitmapRef, void* pBitmapRefPixelBuffer );


#endif //_BLUR_H_
