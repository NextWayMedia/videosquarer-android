#include "queue.h"


//////////////////////////////////////////////////////////////////////////
void media_queue_init(MediaQueue *q)
{
	int i;
	
	q->nArraySize = 0;
	q->nStartIndex = 0;
	
	pthread_mutex_init(&q->mutex, NULL);
}

void media_queue_uninit( MediaQueue *q )
{
	media_queue_flush(q);
	pthread_mutex_destroy(&q->mutex);
}

void media_queue_flush(MediaQueue *q)
{
	int i;
	int nIndex;

	pthread_mutex_lock(&q->mutex);

	for(i = 0; i < q->nArraySize; i++) {
		nIndex = (q->nStartIndex + i) % QUEUE_SIZE;
		SAFE_AV_FREE(q->arrayFrames[nIndex]);
	}

	for (i=0; i < QUEUE_SIZE; i++)
		q->arrayFrames[i] = NULL;

	q->nArraySize = 0;
	q->nStartIndex = 0;

	pthread_mutex_unlock(&q->mutex);
}

void video_queue_flush(MediaQueue *q)
{
	int i;
	int nIndex;

	pthread_mutex_lock(&q->mutex);

	for(i = 0; i < q->nArraySize; i++) {
		nIndex = (q->nStartIndex + i) % QUEUE_SIZE;
		SAFE_AVVIDEO_FREE(q->arrayFrames[nIndex]);
	}

	for (i=0; i < QUEUE_SIZE; i++)
		q->arrayFrames[i] = NULL;

	q->nArraySize = 0;
	q->nStartIndex = 0;

	pthread_mutex_unlock(&q->mutex);
}

int media_queue_empty_index(MediaQueue *q)
{
	int nIndex;

	pthread_mutex_lock(&q->mutex);

	if ( q->nArraySize <= QUEUE_SIZE )
		nIndex = (q->nStartIndex + q->nArraySize) % QUEUE_SIZE;
	else
		nIndex = -1;

	pthread_mutex_unlock(&q->mutex);

	return nIndex;
}

void media_queue_push(MediaQueue *q, AVFrame * frame)
{
	pthread_mutex_lock(&q->mutex);

	int nIndex = (q->nStartIndex + q->nArraySize) % QUEUE_SIZE;

	if (q->nArraySize >= QUEUE_SIZE){

		SAFE_AV_FREE(q->arrayFrames[nIndex]);
		q->nStartIndex = (q->nStartIndex + 1) % QUEUE_SIZE;

	}else{

		q->nArraySize ++;
	}

	q->arrayFrames[nIndex] = frame;
	pthread_mutex_unlock(&q->mutex);

	return;
}

int media_queue_pop(MediaQueue * q, struct AVFrame ** ppFrame)
{
	int ret;

	pthread_mutex_lock(&q->mutex);

	if ( q->nArraySize > 0 )
	{
		*ppFrame = q->arrayFrames[q->nStartIndex];
		q->nArraySize --;
		q->nStartIndex = (q->nStartIndex + 1) % QUEUE_SIZE;
		ret = 1;
	}
	else
	{
		ret = -1;
	}

	pthread_mutex_unlock(&q->mutex);
	
	return ret;
}

int media_queue_get(MediaQueue * q, struct AVFrame ** ppFrame, int index)
{
	int ret;

	if ( index < 0 || index >= QUEUE_SIZE )
		return -1;

	*ppFrame = NULL;

	pthread_mutex_lock(&q->mutex);
	if ( q->nArraySize > 0 && index < q->nArraySize )
	{
		*ppFrame = q->arrayFrames[(q->nStartIndex + index) % QUEUE_SIZE];
		ret = 1;
	}
	else
	{
		ret = -1;
	}

	pthread_mutex_unlock(&q->mutex);

	return ret;
}

int	media_queue_get_with_range(MediaQueue * q, struct AVFrame ** ppFrame, int index, int64_t* ptsmin, int64_t* ptsmax)
{
	int ret = 1;

	if ( index < 0 || index >= QUEUE_SIZE )
		return -1;

	*ppFrame = NULL;

	pthread_mutex_lock(&q->mutex);
	if ( q->nArraySize > 0 && index < q->nArraySize )
	{
		*ppFrame = q->arrayFrames[(q->nStartIndex + index) % QUEUE_SIZE];

		// get min max pts in the frame queue
		AVFrame* pFrameMin = q->arrayFrames[(q->nStartIndex) % QUEUE_SIZE];
		*ptsmin = pFrameMin->pts;

		if (q->nArraySize == 1){
			*ptsmax = *ptsmin + pFrameMin->pkt_duration;
		}else{
			AVFrame* pFrameMax = q->arrayFrames[(q->nStartIndex + q->nArraySize - 1) % QUEUE_SIZE];
			*ptsmax = pFrameMax->pts + pFrameMin->pkt_duration;
		}

		ret = 1;
	}
	else
	{
		ret = -1;
	}

	pthread_mutex_unlock(&q->mutex);

	return ret;
}

int is_media_queue_full(MediaQueue *q)
{
	int ret = 1;

	pthread_mutex_lock(&q->mutex);

	if ( q->nArraySize < QUEUE_SIZE )
		ret = -1;
	else
		ret = 1;

	pthread_mutex_unlock(&q->mutex);

	return ret;
}

