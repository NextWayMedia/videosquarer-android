#ifndef __COMMON_H__
#define __COMMON_H__

#define _ANDROID 1	// windows ffmpeg : 2013.1.20

#include <jni.h>
#include <android/log.h>
#include <pthread.h>
#include <cpu-features.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include "avformat.h"
#include "avcodec.h"
#include "swscale.h"
//#include "cmdutils.h"	
//#include "audioconvert.h"

//////////////////////////////////////////////////////////////////////////
#define  COLOR_TI_FormatYUV420PackedSemiPlanarInterlaced	0x7f000001
#define  COLOR_TI_FormatYUV420PackedSemiPlanar				0x7f000100
#define	 COLOR_FormatYUV420SemiPlanar						0x00000015
#define	 COLOR_FormatYUV420Planar							0x00000013
#define	 COLOR_SONYXPERIA_DECODE_FORMAT						0x7fa30c03
#define	 COLOR_SAMSUNG_GNOTE3_DECODE_CODEC					0x7fa30c04
#define	 HTC_DECODE_CODEC2									0x7fa30c00


#define	YUV420SemiPlanar_BGR	0x10001
#define	YUV420SemiPlanar_RGB	0x10002
#define	YUV420SemiPlanar_HTC	0x10003

//////////////////////////////////////////////////////////////////////////
#define QUEUE_SIZE					5

#define CODEC_TYPE_VIDEO			AVMEDIA_TYPE_VIDEO
#define CODEC_TYPE_AUDIO			AVMEDIA_TYPE_AUDIO

#define VIDEO_AUDIO_ID				0
#define AUDIO_ID					1
#define VIDEO_ID					2

#define AUDIO_DATA_ID   			1
#define VIDEO_DATA_ID   			2

#define VIDEO_TYPE_MP4				1
#define VIDEO_TYPE_3GP				2
#define VIDEO_TYPE_UNKNOWN			-1

#define	ERR_SUCCESS					1
#define ERR_FILENAME				-1000
#define ERR_NO_OPENINDEX			-1001
#define ERR_OPEN_INPUT_FILE			-1002
#define ERR_FIND_STREAM_INFO		-1003
#define ERR_ALLOCATE_FRAME			-1004
#define ERR_NO_MEDIA_TYPE			-1005
#define ERR_NO_DECODER_CODEC		-1006
#define ERR_OPEN_DECODER_CODEC		-1007

#define STREAM_PIX_FMT				PIX_FMT_YUV420P /* default pix_fmt */

#define SAFE_FREE(v)	if(v) { free(v);v=NULL;}
#define SAFE_AV_FREE(v)	if(v) { av_free(v);v=NULL;}
#define SAFE_AVVIDEO_FREE(v)	if(v) { av_frame_unref(v); av_frame_free(&v);v=NULL;}
//#define SAFE_AVVIDEO_FREE(v)	if(v) { av_frame_free(&v);v=NULL;}


// ----------------------- LOG PRINT -----------------------//
#define RepostWhizLog(...)				//__android_log_print(ANDROID_LOG_DEBUG, "VideoSquarerFFMpeg", __VA_ARGS__)
#define RepostWhizErr(...)				__android_log_print(ANDROID_LOG_ERROR, "VideoSquarerFFMpeg", __VA_ARGS__)
#define RepostWhizDbg(...)				__android_log_print(ANDROID_LOG_DEBUG, "VideoSquarerFFMpeg", __VA_ARGS__)

#define  LOG_TAG    				"FFmpegBasic"
#define  LOGI(...)  				//__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define  LOGD(...)  				__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  				__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)


//////////////////////////////////////////////////////////////////////////
// define struct

typedef struct _MediaQueue
{
	AVFrame	*					arrayFrames[QUEUE_SIZE];
	int							nStartIndex;
	int							nArraySize;

	pthread_mutex_t				mutex;
} MediaQueue;

typedef struct _MediaContext
{
	struct AVFormatContext *		formatCtx;
	struct AVCodecContext *			codecCtx;
	struct SwsContext *				swsCtx;

	MediaQueue						mediaQueue;
	struct AVFrame *				audio_frame;

	int								ref_count;
	int								eof;

	int								sample_fmt;
	int								sample_rate;
	int								channels;
	struct AVAudioConvert *			reformat_ctx;
	struct ReSampleContext *		resample_ctx;

	int								streamIdx;
	double							totalTime;

	int								mediaType;

	int								skip_request;
	int								abort_request;
	
	int								seek_request;
	int								seek_flags;
	int64_t							seek_pos;
	int64_t							seek_rel;

	int64_t							pts_request;
	double							time_base;
	double							time_base_rev;
	int								frame_rate;
	int 							isH264;

	pthread_mutex_t					mutex_seek;
	pthread_mutex_t					mutex_free;

	pthread_t						read_thread;

} MediaContext;

typedef struct _AudioFrame
{
    double time;
    char*  pBuff;
    int	   size;
}AudioFrame;

#define AUDIO_QSIZE 20
typedef struct _AudioQueue
{
    AudioFrame*	pArrayFrames;
    int			nArraySize;
} AudioQueue;


 
#endif //__COMMON_H__
