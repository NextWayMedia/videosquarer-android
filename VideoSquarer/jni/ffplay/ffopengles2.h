#include <stdlib.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

typedef struct _FrameBuff
{
	int width;		// real video width
	int height;		// real video height
	int nBuffSize;	// video frame buffer size
	char* pBuff;	// video frame buffer
	int	use;
	int thumbWidth;
	int thumbHeight;
	int drawWidth;
	int drawHeight;
	int framePosX;
	int framePosY;
	int showPosX;
	int showPosY;
	int frameWidth;
	int rotation;
	GLuint yTexture, uTexture, vTexture;

}FrameBuff;

typedef struct _ImageBuff
{
	int width;		// real video width
	int height;		// real video height
	int nBuffSize;	// video frame buffer size
	char* pBuff;	// video frame buffer
	int drawWidth;
	int drawHeight;
	int framePosX;
	int framePosY;
	int showPosX;
	int showPosY;
	int frameWidth;
	GLuint texture;
}ImageBuff;

#define OPEN_LIMIT					100

#ifdef __cplusplus
extern "C" {
#endif

#include <android/log.h>

	void	InitShader();
	void	ExitShader();

	void CreateFrameTexture(int nWidth, int nHeight);
	void OnFragmentUnifom(FrameBuff* frame, int nWidth, int nHeight);
	void OnBlurFragmentUnifom(FrameBuff* frame, int nWidth, int nHeight);
	void SetBlurParams(int blurRadius, char *vertexShader, char*fragmentShader);

	void	OnFragmentDelete();

	GLuint	LoadShader(GLenum type, const char *shaderSrc);
	GLuint	CreateProgram(GLuint vertexShader, GLuint fragmentShader);
	void GetGLColorBuffer(char* pGLBuffer, int nGLScreenWidth, int nGLScreenHeight);
	void renderMaskImage(unsigned char *pImageBuffer, int width, int height);

	void	OnGLESRender(FrameBuff** pVideoBuff, ImageBuff** pImageBuff);
	int _getGLScreenBuffer(char* pGLBuffer, int nGLScreenWidth, int nGLScreenHeight);
    int GetGLBpp();


#ifdef __cplusplus
};
#endif
