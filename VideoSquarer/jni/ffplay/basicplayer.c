#include "basicplayer.h"
#include "ffplayer.h"
#include "opt.h"
#include "queue.h"

#define AV_FORMAT_IO_BUFFER_SIZE		32768
#define AVCODEC_MAX_AUDIO_FRAME_SIZE	192000 // 1 second of 48khz 32bit audio
#define SWR_CH_MAX 	32

/**************************************************************/
int		g_nExportTic = 0;
int		g_nExportColorFormat = 0;
int		g_nExportColorExtension= 0;

int		g_nRenderWorking = 0;

char*	g_pExportBuff = NULL;
char*	g_pGLTextureBuff = NULL;

int		m_nChannel		= 2;
int		m_nFrameRate	= 25;

int		m_nVBitRate = 4000 * 1000; //4000KHz
int		m_nABitRate = 128 * 1000;

AVFormatContext*	m_pAVFormatCtx = NULL;
AVIOContext *		m_io_context = NULL;
unsigned char*		m_io_buffer = NULL;

int64_t		m_nPrvVideoPts;
int64_t		m_nPrvAudioPts;

/**************************************************************/
/* audio output */
AudioQueue	m_queueAudio;
struct	SwrContext*		m_audio_swr = NULL;

char*		m_audio_sample_buffer;
int			m_audio_sample_buffer_size;

enum AVSampleFormat	m_audio_intput_sample_fmt;
/**************************************************************/
/* video output */

/**************************************************************/
FILE*		m_hFile;
int			m_b_write_header;

pthread_mutex_t*	m_hMutexFile;
pthread_mutex_t*	m_hMutexExport;

/**************************************************************/

//////////////////////////////////////////////////////////////////////////
static void *alloc_priv_context(int size, const AVClass *class)
{
	void *p = av_mallocz(size);
	if (p) {
		*(const AVClass **)p = class;
		av_opt_set_defaults(p);
	}
	return p;
}

int IORead( void *opaque, uint8_t *buf, int buf_size )
{
	return 0;
}


int IOWrite( void *opaque, uint8_t *buf, int buf_size )
{
	RepostWhizLog("IOWrite buf_size=%d", buf_size);

	if (m_hFile != NULL){

		fwrite(buf, 1, buf_size, m_hFile);
	}

	RepostWhizLog("IOWrite 2");

	return 0;
}

int64_t IOSeek( void *opaque, int64_t offset, int whence )
{
	RepostWhizLog("IOSeek offset=%d", (int)offset);

	if (m_hFile != NULL){
		//pthread_mutex_lock(m_hMutexFile);
		fseek(m_hFile, (long)offset, SEEK_SET);
		//pthread_mutex_unlock(m_hMutexFile);
	}

	RepostWhizLog("IOSeek 2");
	return 0;
}

void setup_array(uint8_t *out[SWR_CH_MAX], uint8_t *in, enum AVSampleFormat fmt, int samples){
	if(av_sample_fmt_is_planar(fmt)){
		int i;
		int plane_size= av_get_bytes_per_sample(fmt)*samples;
		for(i=0; i<SWR_CH_MAX; i++){
			out[i]= in + i*plane_size;
		}
	}else{
		out[0]= in;
	}
}

//////////////////////////////////////////////////////////////////////////
/*
 * add an audio output stream
 */
int add_audio_stream()
{
	RepostWhizDbg("add_audio_stream s");
	
	int i;
	enum AVCodecID codec = AV_CODEC_ID_AAC;

	AVStream*	stream = NULL;

	if (m_pAVFormatCtx == NULL || m_pAVFormatCtx->oformat == NULL){
		RepostWhizErr("add_audio_stream error, m_pAVFormatCtx or m_pAVFormatCtx->oformat is NULL");
		return -1;
	}

	// check already
	for (i=0; i<m_pAVFormatCtx->nb_streams; i++){
		if (m_pAVFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO){
			RepostWhizErr("add_audio_stream already exists (stream=%d)", i);
			return -1;
		}
	}

	m_pAVFormatCtx->oformat->audio_codec = codec;
	m_audio_intput_sample_fmt = AV_SAMPLE_FMT_S16;

	AVCodec *p_codec = avcodec_find_encoder(m_pAVFormatCtx->oformat->audio_codec);
	if( !p_codec )
	{
		RepostWhizErr("add_audio_stream avcodec_find_encoder error (codec=%d)", (int)m_pAVFormatCtx->oformat->audio_codec);
		return -1;
	}

	stream = av_new_stream( m_pAVFormatCtx, p_codec);
    if (!stream) {
    	RepostWhizLog("Could not alloc stream");
        return -1;
    }

	stream->codec->codec_id		= codec;
    stream->codec->codec_type	= AVMEDIA_TYPE_AUDIO;
    stream->codec->bit_rate		= m_nABitRate;
    stream->codec->sample_rate	= 44100;
    stream->codec->channels		= m_nChannel;
    stream->codec->sample_fmt 	= AV_SAMPLE_FMT_S16;
    stream->codec->channel_layout = av_get_default_channel_layout(m_nChannel);

    RepostWhizDbg("add_audio_stream 1, Audio BitRate=%d, m_nChannel=%d", stream->codec->bit_rate, m_nChannel);

    if (stream->codec->codec_id == AV_CODEC_ID_AAC && (p_codec->capabilities & CODEC_CAP_EXPERIMENTAL) != 0){
		stream->codec->sample_fmt = AV_SAMPLE_FMT_FLTP;
		stream->codec->bits_per_raw_sample = 32;
	}else{
		stream->codec->sample_fmt = AV_SAMPLE_FMT_S16;
		stream->codec->bits_per_raw_sample = 16;
	}

    AVRational time_base;
	time_base.num = 1;
	time_base.den = stream->codec->sample_rate;
	stream->codec->time_base = time_base;

    // some formats want stream headers to be separate
    if(m_pAVFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
        stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;

    if ((p_codec->capabilities & CODEC_CAP_EXPERIMENTAL) != 0){
    	RepostWhizDbg("add_audio_stream 3");
    	stream->codec->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
    }

    /* open it */
    int nErr = 0;
    if ((nErr = (int)avcodec_open2(stream->codec, p_codec, NULL)) < 0) {
    	char error[1024];
    	av_strerror(nErr, error, 1024);
    	RepostWhizErr("audio could not open codec, %s", error);
        return -1;
    }

    m_queueAudio.nArraySize = 0;
	m_queueAudio.pArrayFrames = (AudioFrame*)malloc(sizeof(AudioFrame) * AUDIO_QSIZE);

	int frame_size = stream->codec->frame_size;

	for(i=0; i<AUDIO_QSIZE; i++){
		m_queueAudio.pArrayFrames[i].time = 0.0;
		m_queueAudio.pArrayFrames[i].pBuff= NULL;
		m_queueAudio.pArrayFrames[i].size = 0;
	}

	int bufferSize = av_samples_get_buffer_size(NULL, stream->codec->channels, stream->codec->frame_size, stream->codec->sample_fmt, 1);

	m_audio_sample_buffer = (char*)malloc(bufferSize);
	m_audio_sample_buffer_size = bufferSize;

	m_audio_swr = swr_alloc_set_opts(NULL,
		stream->codec->channel_layout, stream->codec->sample_fmt, stream->codec->sample_rate,
		stream->codec->channel_layout, m_audio_intput_sample_fmt, stream->codec->sample_rate, 0, NULL);

	if (m_audio_swr == NULL)
	{
		RepostWhizErr("add_audio_stream::AddAudioStream swr_alloc_set_opts error");
		return -1;
	}

	if (swr_init(m_audio_swr) < 0)
	{
		RepostWhizErr("add_audio_stream::AddAudioStream swr_init error");
		return -1;
	}

	RepostWhizDbg("add_audio_stream e");

    return frame_size;
}

int encode_write_audio(int8_t* pbData, int cbData, double now_time)
{

	// get audio codec
	AVCodecContext* codec = NULL;
	AVStream*		stream	= NULL;

	int i;
	int stream_index = 0;

	for (i=0; i<m_pAVFormatCtx->nb_streams; i++)
	{
		if (m_pAVFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
		{
			stream = m_pAVFormatCtx->streams[i];
			codec = m_pAVFormatCtx->streams[i]->codec;
			stream_index = i;
			break;
		}
	}

	if (codec == NULL)
		return -1;

	RepostWhizLog("encode_write_audio s");

	AVFrame *frame = avcodec_alloc_frame();
	int got_packet, ret;

	AVPacket pkt = { 0 };
	av_init_packet(&pkt);

	uint8_t* outbuf[SWR_CH_MAX];
	const uint8_t* inbuf[SWR_CH_MAX];

	setup_array(outbuf, (uint8_t *)m_audio_sample_buffer, stream->codec->sample_fmt, codec->frame_size);
	setup_array((uint8_t**)inbuf, (uint8_t *)pbData, m_audio_intput_sample_fmt, codec->frame_size);

	if (swr_convert(m_audio_swr, outbuf, codec->frame_size, inbuf, codec->frame_size) < 0)
	{
		RepostWhizErr("swr_convert err");
		return -1;
	}

	// frame
	frame->nb_samples = codec->frame_size;
	avcodec_fill_audio_frame(frame, codec->channels, codec->sample_fmt,
				(const uint8_t*)m_audio_sample_buffer, m_audio_sample_buffer_size, 1);

	if (avcodec_encode_audio2( codec, &pkt, frame, &got_packet) < 0 || got_packet == 0)
	{
		RepostWhizErr("avcodec_encode_audio2 ret=-1");

		frame->pts += codec->frame_size;
		return -1;
	}
	frame->pts += codec->frame_size;

	if ( pkt.pts != AV_NOPTS_VALUE )
		pkt.pts = av_rescale_q(pkt.pts, codec->time_base, stream->time_base);
	if ( pkt.dts != AV_NOPTS_VALUE )
		pkt.dts = av_rescale_q(pkt.dts, codec->time_base, stream->time_base);

	pkt.stream_index = stream_index;
	pkt.flags |= AV_PKT_FLAG_KEY;

	/* Write the compressed frame to the media file. */
	pthread_mutex_lock(m_hMutexFile);
	ret = av_interleaved_write_frame(m_pAVFormatCtx, &pkt);
	pthread_mutex_unlock(m_hMutexFile);

	if (ret != 0) {
		RepostWhizErr("Error while writing audio frame, error=%d", ret);
		return -1;
	}

	avcodec_free_frame(&frame);

	RepostWhizLog("write_audio_frame e, pkt.size=%d", pkt.size);

	return 1;
}

int write_audio_frame(char* buff, int len, double now_time)
{
	RepostWhizLog("write_audio_frame s");

	if (m_pAVFormatCtx == NULL)
		return -1;

	if (m_b_write_header==0){

		if (m_queueAudio.nArraySize >= AUDIO_QSIZE){
			return -1;
		}

		char* pBuff = (char*)malloc(len);
		memcpy(pBuff, buff, len);

		int idx = m_queueAudio.nArraySize;
		m_queueAudio.pArrayFrames[idx].pBuff = pBuff;
		m_queueAudio.pArrayFrames[idx].size = len;
		m_queueAudio.pArrayFrames[idx].time = now_time;

		m_queueAudio.nArraySize ++;

		//RepostWhizErr("write_audio_frame audio frame pushed");
		return 1;
	}

	if (m_queueAudio.nArraySize > 0)
	{
		RepostWhizDbg("write_audio_frame 1, m_queueAudio.nArraySize=%d", m_queueAudio.nArraySize);

		int i;
		for(i=0; i<m_queueAudio.nArraySize; i++)
		{
			char* pBuff = m_queueAudio.pArrayFrames[i].pBuff;
			int size = m_queueAudio.pArrayFrames[i].size;
			double tm = m_queueAudio.pArrayFrames[i].time;

			encode_write_audio((int8_t*)pBuff,size, tm);

			SAFE_FREE(pBuff);
			m_queueAudio.pArrayFrames[i].time = 0.0;
			m_queueAudio.pArrayFrames[i].size = 0;
		}

		SAFE_FREE(m_queueAudio.pArrayFrames);
		m_queueAudio.nArraySize = 0;
	}

	encode_write_audio((int8_t*)buff, len, now_time);
	RepostWhizLog("write_audio_frame e");

	return 1;
}

//////////////////////////////////////////////////////////////////////////
// Video
void writeFileEx(char* name, char* buff, int len)
{
	FILE* fp = fopen(name, "at");

	if (fp){
		fwrite(buff, 1, len, fp);
		fclose(fp);
	}
}

int write_encoded_header(char* buff, int size)
{
	RepostWhizDbg("write_encoded_header s");

	int ret = -1;
	int i;

	if (m_pAVFormatCtx == NULL)
		return -1;

	AVCodecContext* codecCxt = NULL;
	AVStream*		stream = NULL;
	int stream_index = 0;

	for (i=0; i<m_pAVFormatCtx->nb_streams; i++)
	{
		if (m_pAVFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			stream = m_pAVFormatCtx->streams[i];
			codecCxt = m_pAVFormatCtx->streams[i]->codec;
			stream_index = i;
			break;
		}
	}

	if (codecCxt == NULL)
		return -1;

	RepostWhizDbg("write_encoded_header 1, size=%d, extradata_size=%d", size, codecCxt->extradata_size);

	memset(codecCxt->extradata, 0 , codecCxt->extradata_size);
	codecCxt->extradata_size = size;
	memcpy(codecCxt->extradata, buff, size);

	// write header
	if (m_b_write_header == 0)
	{
		RepostWhizDbg("avformat_write_header ");

		if( (avformat_write_header( m_pAVFormatCtx, NULL )) < 0 )
		{
			RepostWhizErr("avformat_write_header -1");
			return -1;
		}
		m_b_write_header = 1;
	}

	RepostWhizDbg("write_encoded_header e");

	return 1;
}

int	write_encoded_buffer(char* buff, int size, double now_time, int is_key_frame)
{
	RepostWhizLog("write_encoded_buffer s, size=%d ", size);

	if (m_pAVFormatCtx == NULL)
		return -1;

	int ret = -1;
	int i;

	AVCodecContext* codecCxt = NULL;
	AVStream*		stream = NULL;
	int stream_index = 0;

	for (i=0; i<m_pAVFormatCtx->nb_streams; i++)
	{
		if (m_pAVFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			stream = m_pAVFormatCtx->streams[i];
			codecCxt = m_pAVFormatCtx->streams[i]->codec;
			stream_index = i;
			break;
		}
	}

	if (codecCxt == NULL)
		return -1;

	/* if zero size, it means the image was buffered */
	AVPacket pkt;
	memset(&pkt, 0 , sizeof(AVPacket));
	av_init_packet(&pkt);

	int64_t pts = av_rescale_q((int64_t)(now_time * codecCxt->time_base.den / codecCxt->time_base.num), stream->codec->time_base, stream->time_base);

	if (pts <= m_nPrvVideoPts){
		pts = m_nPrvVideoPts + 100;
	}

	m_nPrvVideoPts = pts;

	pkt.pts = pts;
	pkt.dts = pts;

	if (is_key_frame > 0)
		pkt.flags |= AV_PKT_FLAG_KEY;

	pkt.stream_index= stream_index;
	pkt.data= buff;
	pkt.size= size;

	codecCxt->frame_number ++;

	/* write the compressed frame in the media file */
	pthread_mutex_lock(m_hMutexFile);
	ret = av_write_frame(m_pAVFormatCtx, &pkt);
	pthread_mutex_unlock(m_hMutexFile);

	RepostWhizLog("write_encoded_buffer ret=%d, pkt.pts=%d, pkt.size=%d, size=%d, now_time=%f, is_key_frame=%d, frame_number=%d", ret, (int)pkt.pts, pkt.size, size, now_time, is_key_frame, codecCxt->frame_number);

	return ret;
}

//////////////////////////////////////////////////////////////////////////
int add_video_stream()
{	
	RepostWhizDbg("add_video_stream s");

	int i;
	enum AVCodecID codec = AV_CODEC_ID_H264;

	// check already
	for (i=0; i<m_pAVFormatCtx->nb_streams; i++)
	{
		if (m_pAVFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			RepostWhizErr("add_video_stream already exists (stream=%d)", i);
			return -1;
		}
	}

	RepostWhizErr("Video codec %d", (int)m_pAVFormatCtx->oformat->video_codec);
	m_pAVFormatCtx->oformat->video_codec = codec;
	AVCodec *p_codec = avcodec_find_encoder( m_pAVFormatCtx->oformat->video_codec );

	if( !p_codec )
	{
		RepostWhizErr("AddVideoStream avcodec_find_encoder error");

		AVCodec * ccc = NULL;
		while((ccc = av_codec_next(ccc)) != NULL){
			RepostWhizErr(" enum codec (name=%s, type=%d, id=%d, encode=%d, decode=%d)", ccc->name, (int)ccc->type, (int)ccc->id, (int)(ccc->encode2 != NULL), (int)(ccc->decode != NULL) );
			//RepostWhizErr(" enum codec ");
		}

		return -1;
	}

	AVStream* stream = av_new_stream(m_pAVFormatCtx, p_codec);
	if (! stream) {
		RepostWhizErr("add_video_stream Could not alloc stream");
		return -1;
	}

	stream->codec->codec_id		= m_pAVFormatCtx->oformat->video_codec;
	stream->codec->codec_type	= AVMEDIA_TYPE_VIDEO;
	stream->codec->bit_rate		= m_nVBitRate;
	stream->codec->width		= g_nTextureWidth;
	stream->codec->height		= g_nTextureHeight;
	stream->codec->time_base.den = m_nFrameRate;
	stream->codec->time_base.num = 1;
	stream->codec->max_b_frames = 1;
	stream->codec->gop_size		= 12;

	stream->nb_frames = 0;


	if (stream->codec->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
		/* just for testing, we also add B frames */
		stream->codec->max_b_frames = 2;
	}
	if (stream->codec->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
		/* Needed to avoid using macroblocks in which some coeffs overflow.
		 * This does not happen with normal video, it just happens here as
		 * the motion of the chroma plane does not match the luma plane. */
		stream->codec->mb_decision = 2;
	}

	stream->codec->pix_fmt = AV_PIX_FMT_YUV420P;

	if (stream->codec->codec_id == AV_CODEC_ID_H264)
	{
		stream->codec->priv_data = alloc_priv_context(p_codec->priv_data_size, p_codec->priv_class);

		int flags = AV_OPT_FLAG_VIDEO_PARAM | AV_OPT_FLAG_ENCODING_PARAM;
		if (av_find_opt(stream->codec->priv_data, "profile", NULL, flags, flags)) {

			av_set_string3(stream->codec->priv_data, "profile", "baseline", 1, NULL);
			av_set_string3(stream->codec->priv_data, "preset", "medium", 1, NULL);
		}

		stream->codec->coder_type = FF_CODER_TYPE_AC;
		stream->codec->level = 13;
		stream->codec->rc_max_rate = stream->codec->bit_rate;
		stream->codec->rc_initial_buffer_occupancy = stream->codec->bit_rate;
		stream->codec->rc_buffer_size = stream->codec->bit_rate;
		stream->codec->bit_rate_tolerance = stream->codec->bit_rate;

		stream->codec->me_range = 32;
		stream->codec->me_subpel_quality = 10;
		stream->codec->trellis = 2;
		stream->codec->qcompress = 0.5f;
		stream->codec->scenechange_threshold = -1;

		stream->codec->me_method = ME_UMH;
		stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER + CODEC_FLAG_LOOP_FILTER;

		stream->codec->chromaoffset = 0;
		stream->codec->noise_reduction = 0;

		stream->codec->qmin = 4;
		stream->codec->qmax = 12;
		stream->codec->max_qdiff= 4;
	}    

	// some formats want stream headers to be separate
	if(m_pAVFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
		stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;

	RepostWhizDbg("avcodec_find_encoder prev codec_id= %d, m_nVBitRate=%d", stream->codec->codec_id, m_nVBitRate);

	if (avcodec_open2(stream->codec, p_codec, NULL) < 0) {
		RepostWhizErr("could not open codec");
		return -1;
	}

	RepostWhizDbg("add_video_stream e");

	return stream;
}

/////////////////////////////////////////////////////////////////////////
int create_movie_file( const char* filename, const char *fileext, int nVideoHeight, int nVideoWidth, int nVBitRate, int nABitRate )
{
	RepostWhizDbg("create_movie_file FileName='%s'; FileExt='%s'; nVideoHeight='%d', nVideoWidth='%d', VideoBitRate='%d', AudioBitRate='%d'", filename, fileext, nVideoHeight, nVideoWidth, nVBitRate, nABitRate);

	if (!filename)
		return -1;

	int i;
	int ret;

	//------------------------------------------------------------------------------//
	g_nRenderWorking = 0;
	g_nExportTic = 0;
	//SetPlayerScreenMode(2, nVideoWidth, nVideoHeight);

	SetGLScreenSize(nVideoWidth, nVideoHeight);
	SAFE_FREE(g_pExportBuff);
	SAFE_FREE(g_pGLTextureBuff);

	int size = (int)((g_nTextureWidth * g_nTextureHeight * 3) >> 1);
	g_pExportBuff = (char*)malloc(size);

	size = g_nTextureWidth * g_nTextureHeight * GetGLBpp();
	g_pGLTextureBuff = (char*)malloc(size);

	//------------------------------------------------------------------------------//
	m_io_context = NULL;
	m_b_write_header = 0;

	m_hFile = NULL;
	m_hFile = fopen(filename, "wb");

	m_hMutexFile = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(m_hMutexFile, NULL);

	m_hMutexExport = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(m_hMutexExport, NULL);

	m_nVBitRate = nVBitRate;
	m_nABitRate = nABitRate;

	m_pAVFormatCtx = avformat_alloc_context();
	m_pAVFormatCtx->oformat = av_guess_format("mp4", NULL, NULL );

	if (m_pAVFormatCtx->oformat == NULL)
	{
		RepostWhizErr("create_movie_file StartEncoding av_guess_format error");

		close_movie_file();
		return -1;
	}

	RepostWhizDbg("codec, filename=%s, video_codec=%d, audio_codec=%d",
					filename, m_pAVFormatCtx->oformat->video_codec, m_pAVFormatCtx->oformat->audio_codec);

	m_io_buffer = (unsigned char*)malloc(AV_FORMAT_IO_BUFFER_SIZE);
	memset(m_io_buffer, 0 , AV_FORMAT_IO_BUFFER_SIZE);

	m_io_context = avio_alloc_context((unsigned char *)m_io_buffer, AV_FORMAT_IO_BUFFER_SIZE, 1, NULL, IORead, IOWrite, IOSeek);

	if (m_io_context == NULL)
	{
		RepostWhizErr("create_movie_file::StartEncoding avio_alloc_context error");

		close_movie_file();
		return -1;
	}

	//------------------------------------------------------------------------------//
	m_pAVFormatCtx->pb = m_io_context;
	m_pAVFormatCtx->nb_streams = 0;

	RepostWhizDbg("create_movie_file 1");

	if (add_video_stream() == -1){

		RepostWhizErr("create_movie_file::add_video_stream error!");
		close_movie_file();
		return -1;
	}

	RepostWhizDbg("create_movie_file 2");

	int nABuffSize = 0;
	if ((nABuffSize = add_audio_stream()) < 0){
		RepostWhizErr("create_movie_file::add_audio_stream error!");
		close_movie_file();
		return -1;
	}

	m_nPrvVideoPts = 0;
	m_nPrvAudioPts = 0;

	RepostWhizDbg("create_movie_file end");

	return nABuffSize;
}
 
void close_movie_file()
{ 
	RepostWhizDbg("close_movie_file s");

	//SetPlayerScreenMode(g_nOldPlayerScreenMode, g_nOldTextureWidth, g_nOldTextureHeight);

	g_nExportTic = 0;

	usleep(100000);		//100ms

	int i;
	RepostWhizLog("close_movie_file 1");

	if (m_pAVFormatCtx){
		if (m_b_write_header>0){
			RepostWhizDbg("av_write_trailer  ");
			av_write_trailer(m_pAVFormatCtx);
		}

		for( i = 0 ; i < m_pAVFormatCtx->nb_streams; i++ )
		{
			//avcodec_close(m_pAVFormatCtx->streams[i]->codec);
			if( m_pAVFormatCtx->streams[i]->codec->extradata )
				av_free( m_pAVFormatCtx->streams[i]->codec->extradata );

			av_free( m_pAVFormatCtx->streams[i]->codec );
			av_free( m_pAVFormatCtx->streams[i]);
		}

		//avformat_free_context(m_pAVFormatCtx);
		av_free(m_pAVFormatCtx);
		m_pAVFormatCtx = NULL;
	}

	RepostWhizLog("close_movie_file 2");

	if (m_hFile != NULL)
	{
		fclose(m_hFile);
		m_hFile = NULL;
	}

	if (m_queueAudio.nArraySize > 0)
	{
		int i;
		for(i=0; i<m_queueAudio.nArraySize; i++){
			SAFE_FREE(m_queueAudio.pArrayFrames[i].pBuff);
		}

		SAFE_FREE(m_queueAudio.pArrayFrames);
		m_queueAudio.nArraySize = 0;
	}

	SAFE_FREE(m_audio_sample_buffer);

	pthread_mutex_destroy(m_hMutexFile);
	SAFE_FREE(m_hMutexFile);

	pthread_mutex_destroy(m_hMutexExport);
	SAFE_FREE(m_hMutexExport);

	SAFE_AV_FREE(m_io_context);
	SAFE_FREE(m_io_buffer);

	SAFE_FREE(g_pExportBuff);
	SAFE_FREE(g_pGLTextureBuff);

	RepostWhizDbg("close_movie_file e");
}

//////////////////////////////////////////////////////////////////////////
// set option.
void set_frame_rate(int frame_rate)
{
	m_nFrameRate = frame_rate;
}

void set_device_color_format(int color_format, int color_extension)
{
	g_nExportColorFormat = color_format;
	g_nExportColorExtension = color_extension;
}
