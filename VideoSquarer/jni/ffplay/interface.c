#ifndef _WINDOWS

#include "ffplayer.h"
#include "basicplayer.h"

//////////////////////////////////////////////////////////////////////////

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
	return JNI_VERSION_1_4;
}

jobject BitmapLock( JNIEnv* env, jobject thiz, jobject pBitmap, void** pBitmapRefPixelBuffer )
{
	jobject pBitmapRef = (*env)->NewGlobalRef(env, pBitmap); //lock the bitmap preventing the garbage collector from destructing it

	if (pBitmapRef == NULL)
	{
		*pBitmapRefPixelBuffer = NULL;
		return NULL;
	}

	int result = AndroidBitmap_lockPixels(env, pBitmapRef, pBitmapRefPixelBuffer);
	if (result != 0)
	{
		*pBitmapRefPixelBuffer = NULL;
		return NULL;
	}

	return pBitmapRef;
}

void BitmapUnlock( JNIEnv* env, jobject thiz, jobject pBitmapRef, void* pBitmapRefPixelBuffer )
{
	if (pBitmapRef)
	{
		if (pBitmapRefPixelBuffer)
		{
			AndroidBitmap_unlockPixels(env, pBitmapRef);
			pBitmapRefPixelBuffer = NULL;
		}
		(*env)->DeleteGlobalRef(env, pBitmapRef);
		pBitmapRef = NULL;
	}
}

//////////////////////////////////////////////////////////////////////////
// functions for ffmpeg
JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetBlurEnable(JNIEnv* env, jobject thiz, int enable)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetBlurEnable 1");
	set_blur_enable(enable);
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetBlurEnable 2");
}

JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetBlurValues(JNIEnv* env, jobject thiz, jint blurRadius,
		jstring vertexString, jstring fragmentString)
{
	   const char *nativeVertexString = (*env)->GetStringUTFChars(env, vertexString, 0);
	   const char *nativeFragmentString = (*env)->GetStringUTFChars(env, fragmentString, 0);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetBlurValues 1");
	set_blur_values(blurRadius, nativeVertexString, nativeFragmentString);
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetBlurValues 2");
}

JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeInitFFMpeg(JNIEnv* env, jobject thiz)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeInitFFMpeg 1");
	init_ffmpeg_lib();
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeInitFFMpeg 2");
}

JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeFinitFFMpeg(JNIEnv* env, jobject thiz)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeFinitFFMpeg 1");
	uninit_ffmpeg_lib();
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeFinitFFMpeg 2");
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeOpenVideo(JNIEnv* env, jobject thiz, jstring mediafile)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeOpenVideo 1");

	const char* mfileName = (*env)->GetStringUTFChars(env, mediafile, 0);
	int open_index;

	open_index = load_media(mfileName, CODEC_TYPE_VIDEO);

	(*env)->ReleaseStringUTFChars(env, mediafile, mfileName); //always release the java string reference

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeOpenVideo 2");

	return open_index;
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeOpenAudio(JNIEnv* env, jobject thiz, jstring mediafile)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeOpenAudio 1");

	const char* mfileName = (*env)->GetStringUTFChars(env, mediafile, 0);
	int open_index;

	open_index = load_media(mfileName, CODEC_TYPE_AUDIO);
	(*env)->ReleaseStringUTFChars(env, mediafile, mfileName); //always release the java string reference

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeOpenAudio 2");

	return open_index;
}

JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeClose(JNIEnv* env, jobject thiz, int open_index)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeClose 1, open_index=%d", open_index);

	if ( open_index < 0 || open_index >= OPEN_LIMIT ){
		RepostWhizErr("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeClose Error!");
		return;
	}

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeClose 1, step1 ");

	unload_media(open_index);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeClose 2");
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeThreadStart(JNIEnv* env, jobject thiz, int open_index)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeThreadStart");

	return start_media_thread(open_index);
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeUpdateVideo(JNIEnv* env, jobject thiz, int open_index, jdouble sec)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeUpdateBitmap 1");

	int res = catch_video_frame(open_index, sec);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeUpdateBitmap 2");

	return res;
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeUpdateAudio(JNIEnv* env, jobject thiz, int open_index, jdouble sec, int buf_size, jobject buffer)
{
	//return -1;
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeUpdateAudio 1");

	int res = 0;

	if ( buffer )
	{
		int length;
		jbyte * bytearray;

		bytearray = (*env)->GetByteArrayElements(env, (jbyteArray) buffer, NULL);
		res = get_audio_frame(open_index, bytearray, buf_size, sec);
		(*env)->ReleaseByteArrayElements(env, (jbyteArray) buffer, bytearray, 0);
	}

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeUpdateAudio 2");

	return res;
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeGetFrameRate(JNIEnv* env, jobject thiz, int open_index)
{
	return get_frame_rate(open_index);
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSeek(JNIEnv* env, jobject thiz, int open_index, jdouble sec)
{
	int res = 0;
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSeek 1");

	res = seek_media(open_index, sec);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSeek 2");

	return res;
}

JNIEXPORT jdouble JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeGetTotalTime(JNIEnv* env, jobject thiz, int open_index)
{
	double ret = 0.0;
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeGetTotalTime 1");

	ret = get_total_time(open_index);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeGetTotalTime 2");
	return ret;
}

//////////////////////////////////////////////////////////////////////////
// render
void Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderInit(JNIEnv *env, jobject thiz)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderInit 1");
	InitRenderFrame();
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderInit 2");
}

void Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderFinit(JNIEnv * env, jobject thiz)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderFrame 1");
	FiniteRenderFrame();
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderFrame 2");
}

void Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderResize(JNIEnv * env, jobject thiz)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderResize");
	SetGLScreenSize();
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderResize 2");
}

void Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderFrame(JNIEnv * env, jobject thiz)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderFrame 1");
	OnRenderFrame(NULL);
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_GLPlayerRenderer_NativeRenderFrame 2");
}

/* ------------------------------------------------------- Basic Player ----------------------------------------------------- */
jint Java_com_videosquarer_squareimage_gui_DisplayingActivity_initBasicPlayer(JNIEnv *env, jobject thiz)
{
	if (android_getCpuFamily() == ANDROID_CPU_FAMILY_ARM/* && (android_getCpuFeatures() & ANDROID_CPU_ARM_FEATURE_NEON) != 0*/)
	{
		av_register_all();
		return 0;
	}
	else
		return -1;
}

void Java_com_videosquarer_squareimage_gui_DisplayingActivity_setStreamFrameRate(JNIEnv *env, jobject thiz, jint val)
{
	set_frame_rate(val);
}

jint Java_com_videosquarer_squareimage_gui_DisplayingActivity_createMovieFile(JNIEnv *env, jobject thiz, jstring filePath, jstring fileFormat, int nVideoHeight, int nVideoWidth, int nVBitRate, int nABitRate)
{
	int result;
	const char *filename;
	const char *fileext;

	filename = (*env)->GetStringUTFChars(env, filePath, NULL);
	fileext = (*env)->GetStringUTFChars(env, fileFormat, NULL);

	RepostWhizLog("InterFace ExportVideoInfo FileName = '%s'; FileExt = '%s'; nVideoHeight = '%d', nVideoWidth = '%d', VideoBitRate = '%d', AudioBitRate = '%d'",
			filename, fileext, nVideoHeight, nVideoWidth, nVBitRate, nABitRate);

	result = create_movie_file(filename, fileext, nVideoHeight, nVideoWidth, nVBitRate, nABitRate);

	(*env)->ReleaseStringUTFChars(env, filePath, filename);
	(*env)->ReleaseStringUTFChars(env, fileFormat, fileext);
	return result;
}

void Java_com_videosquarer_squareimage_gui_DisplayingActivity_closeMovieFile(JNIEnv *env, jobject thiz)
{
	close_movie_file();
}

jint Java_com_videosquarer_squareimage_gui_DisplayingActivity_writeAudioSample(JNIEnv *env, jobject thiz, jobject pshData, int len, jdouble now_time)
{
	int ret;

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_writeAudioSample 1, len=%d", len);

	jbyte* shArr = (jbyte*) (*env)->GetPrimitiveArrayCritical(env, pshData, 0);

	ret = write_audio_frame(shArr, len, now_time);

	(*env)->ReleasePrimitiveArrayCritical(env, pshData, shArr, 0);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_DisplayingActivity_writeAudioSample 2");

	return ret;
}

int Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeGetExportTic(JNIEnv *env, jobject thiz)
{
	return g_nExportTic;
}

void Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_getScreenBuff(JNIEnv *env, jobject thiz, jbyteArray buff, jint size)
{
	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_getScreenBuff 1");

	jbyte* pBuff = (jbyte*) (*env)->GetPrimitiveArrayCritical(env, buff, 0);

	CopyScreenBuff(pBuff, size);

	(*env)->ReleasePrimitiveArrayCritical(env, buff, pBuff, 0);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_getScreenBuff 2");
}

jint Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_writeEncodedVideoHeader(JNIEnv *env, jobject thiz, jbyteArray buff, jint size)
{

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_writeEncodedVideoHeader 1");

	jbyte* pBuff = (jbyte*) (*env)->GetPrimitiveArrayCritical(env, buff, 0);

	write_encoded_header(pBuff, size);

	(*env)->ReleasePrimitiveArrayCritical(env, buff, pBuff, 0);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_writeEncodedVideoHeader 2");

	return 1;
}


jint Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_writeEncodedVideoBuff(JNIEnv *env, jobject thiz, jbyteArray buff, jint size, jdouble now_time, int is_key_frame)
{

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_writeEncodedVideoBuff 1");

	jbyte* pBuff = (jbyte*) (*env)->GetPrimitiveArrayCritical(env, buff, 0);

	write_encoded_buffer(pBuff, size, now_time, is_key_frame);

	(*env)->ReleasePrimitiveArrayCritical(env, buff, pBuff, 0);

	RepostWhizLog("Java_com_videosquarer_squareimage_gui_highencoder_EncodeThread_writeEncodedVideoBuff 2");

	return 1;
}

JNIEXPORT jint JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetMaskImage(JNIEnv* env, jobject thiz, jobject pBitmap, jint width, jint height)
{
	if ( pBitmap )
	{
		void* pBitmapRefPixelBuffer;

		jobject pBitmapRef = BitmapLock(env, thiz, pBitmap, &pBitmapRefPixelBuffer);

		if (pBitmapRef == NULL)
		{
			return -1;
		}


		SetMaskImage((unsigned char*)pBitmapRefPixelBuffer, width, height);

		BitmapUnlock(env, thiz, pBitmapRef, pBitmapRefPixelBuffer);
	}

	return 1;
}

JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeLoadImage(JNIEnv* env, jobject thiz, jobject pBitmap, jint width, jint height,
		jint drawWidth, jint drawHeight, jint framePosX, jint framePosY, jint showPosX, jint showPosY, jint frameWidth)
{
	if ( pBitmap )
	{
		void* pBitmapRefPixelBuffer;

		jobject pBitmapRef = BitmapLock(env, thiz, pBitmap, &pBitmapRefPixelBuffer);

		if (pBitmapRef == NULL)
		{
			return ;
		}


		load_image((unsigned char*)pBitmapRefPixelBuffer, width, height, drawWidth, drawHeight,
				framePosX, framePosY, showPosX, showPosY, frameWidth);

		BitmapUnlock(env, thiz, pBitmapRef, pBitmapRefPixelBuffer);
	}

	return;

}

JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetVideoInfo(JNIEnv* env, jobject thiz, jint open_index, jint width, jint height,
		jint drawWidth, jint drawHeight, jint framePosX, jint framePosY, jint showPosX, jint showPosY, jint frameWidth)
{
	set_video_transinfo(open_index, width, height, drawWidth, drawHeight, framePosX, framePosY, showPosX, showPosY, frameWidth);


	return;

}

JNIEXPORT void JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeSetVideoRotation(JNIEnv* env, jobject thiz, jint open_index, jint rotation)
{
	set_video_rotation(open_index, rotation);


	return;

}

#endif //_WINDOWS
