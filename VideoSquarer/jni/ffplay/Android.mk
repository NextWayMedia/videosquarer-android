LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libffplayer

EFFECT_PATH := ./effect
FFMPEG_PATH := $(LOCAL_PATH)/../ffmpeg/include
h_libffmpeg := $(LOCAL_PATH)/../ffmpeg/lib
h_libx264 := $(LOCAL_PATH)/../x264

LOCAL_C_INCLUDES := $(FFMPEG_PATH)
LOCAL_C_INCLUDES += $(h_libx264)
LOCAL_C_INCLUDES += $(FFMPEG_PATH)/libavcodec
LOCAL_C_INCLUDES += $(FFMPEG_PATH)/libavformat
LOCAL_C_INCLUDES += $(FFMPEG_PATH)/libswscale
LOCAL_C_INCLUDES += $(FFMPEG_PATH)/libavutil
LOCAL_C_INCLUDES += $(FFMPEG_PATH)/libswresample
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../ffmpeg
LOCAL_C_INCLUDES += $(LOCAL_PATH)/$(EFFECT_PATH)
LOCAL_C_INCLUDES += $(LOCAL_PATH)


LOCAL_SRC_FILES := ffplayer.c \
		basicplayer.c \
		interface.c \
		queue.c \
		common.c \
		ffopengles2.cpp \
		OtherProcess.c \
		blur.c

LOCAL_STATIC_LIBRARIES := libavformat libavcodec libswscale libavutil cpufeatures libx264 

LOCAL_LDLIBS := -lz -llog -ljnigraphics -lGLESv1_CM -lGLESv2

LOCAL_LDLIBS += $(h_libffmpeg)/libavformat.a
LOCAL_LDLIBS += $(h_libffmpeg)/libavcodec.a
LOCAL_LDLIBS += $(h_libffmpeg)/libavutil.a
LOCAL_LDLIBS += $(h_libffmpeg)/libswresample.a
LOCAL_LDLIBS += $(h_libffmpeg)/libswscale.a
LOCAL_LDLIBS += $(h_libx264)/libx264.a


LOCAL_ARM_MODE := arm

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/cpufeatures)
