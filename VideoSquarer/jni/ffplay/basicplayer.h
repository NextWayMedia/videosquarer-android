#ifndef BASICPLAYER_H__INCED__110326
#define BASICPLAYER_H__INCED__110326

	#include "ffplay_common.h"

	// ----- movie file create & close -----------------------------
	int		create_movie_file(const char* filename, const char *fileext, int nVideoHeight, int nVideoWidth, int nVBitRate, int nABitRate);
	void	close_movie_file();

	// ----- video  ------------------------------------
	int		add_video_stream();
	int		write_encoded_header(char* buff, int size);
	int		write_encoded_buffer(char* buff, int size, double now_time, int is_key_frame);

	// ----- audio  ------------------------------------
	int		add_audio_stream();
	int		write_audio_frame(char* pbData, int len, double now_time);
	
	int 	encode_write_audio(int8_t* pbData, int cbData, double now_time);

	void	set_frame_rate(int frame_rate);
	void	set_device_color_format(int color_format, int color_extension);

#endif
