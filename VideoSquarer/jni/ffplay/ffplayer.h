#ifndef __FFPLAYER_H__
#define __FFPLAYER_H__

	#include "ffplay_common.h"
	#include "ffopengles2.h"

int		g_nTextureWidth;
int		g_nTextureHeight;
int 	g_isBlur;
int		g_nExportColorFormat;
int		g_nExportColorExtension;
int		g_nRenderWorking;

char*	g_pExportBuff;
char*	g_pGLTextureBuff;

pthread_mutex_t*	m_hMutexExport;

int		g_nExportTic;

	//----------------------------------------------------------------------------------------//
	// load & unload media
	int		init_ffmpeg_lib();
	int		uninit_ffmpeg_lib();

	//////////////////////////////////////////////////////////////////////////
	int		load_media(const char * file_name, int media_type);
	void	unload_media(int open_index);
	void	unload_mediacontext(MediaContext * mediaCtx);

	void load_image(char *pBuff, int width, int height, int drawWidth, int drawHeight,
			int framePosX, int framePosY, int showPosX, int showPosY, int frameWidth);
	void set_video_transinfo(int open_index, int width, int height, int drawWidth, int drawHeight,
			int framePosX, int framePosY, int showPosX, int showPosY, int frameWidth);
	void set_video_rotation(int open_index, int rotation);

	void set_blur_enable(int enable);
	void set_blur_values(int blurRadius, char *vertexShader, char *fragmentShader);

	int		find_open_index();
	int 	find_open_image_index();

	int		start_media_thread(int open_index);
	void*	media_read_thread(void* arg);


	//----------------------------------------------------------------------------------------//
	// FFMPEG
	// decode video packet to frame
	int		decode_video_frame(MediaContext * mediaCtx, AVPacket* pkt_in, AVFrame** frame_out);
	int		decode_audio_frame(MediaContext * mediaCtx, AVPacket* pkt_in);
	int		_get_video_frame(MediaContext * mediaCtx, double sec, AVFrame** frame_out, int* video_must_free);
	int		_get_video_frame_by_order(MediaContext * mediaCtx, AVFrame** frame_out);

	// catch video frame
	int		catch_video_frame(int open_index, double sec);
	int		get_audio_frame(int open_index, void * buffer, int buf_size, double sec);

	// seek media
	int		seek_media(int open_index, double sec);
	int		clear_frame(int frame_index);
	int		clear_video_screen(int index);
	int		get_frame_rate(int open_index);


	// get media total time
	double	get_total_time(int open_index);
	int64_t	get_time_from_byte(MediaContext * mediaCtx, int64_t bytes);
	int64_t	get_byte_from_time(MediaContext * mediaCtx, int64_t pts);

	//----------------------------------------------------------------------------------------//
	//	Render & Shader
	void	SetGLScreenSize();
	void SetMaskImage(unsigned char *pixelBuffer, int width, int height);

	void	InitRenderFrame();
	void 	OnRenderFrame(char* pGLBuff);

#endif
