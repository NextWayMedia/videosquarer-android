#include "ffplayer.h"
#include "queue.h"
#include <GLES/gl.h>
#include "basicplayer.h"

// delta time, for get nearest position after seek , second
#define SEEK_DELTA_SEC	0.15 	// 100ms

// delta time, for get frame from decoded queue.
#define DELTA 			0.3		// 100ms

//--------------------------------------------//
// FFmpeg

MediaContext *	g_mediaCtx[OPEN_LIMIT];
pthread_mutex_t	g_mutexMedia;

FrameBuff*		g_pVideoFrameBuff[OPEN_LIMIT];
ImageBuff* 	    g_pImageBuff[OPEN_LIMIT];
AVFrame*		m_frame_scale = NULL;

////------------------------------------------------------------------------------------------------//
//// FFMpeg Video and Audio
//

int init_ffmpeg_lib()
{
	int i;

	RepostWhizDbg("Init FFMpeg library. %d", 123);

	av_log_set_flags(AV_LOG_SKIP_REPEATED);
	av_register_all();
	avcodec_register_all();

	for (i = 0; i < OPEN_LIMIT; i ++){
		g_mediaCtx[i] = NULL;
	}

	pthread_mutex_init(&g_mutexMedia, NULL);

	m_frame_scale = avcodec_alloc_frame();
	m_frame_scale->pts = 0;

	memset(g_pVideoFrameBuff, 0, OPEN_LIMIT);
	memset(g_pImageBuff, 0, OPEN_LIMIT);

	RepostWhizDbg("Init FFMpeg library Succeded.");

	return ERR_SUCCESS;
}

int uninit_ffmpeg_lib()
{
	int i = 0;
	RepostWhizDbg("Free Video Buffers");

	for (i = 0; i < OPEN_LIMIT; i ++)
	{
		if(g_pVideoFrameBuff[i])
		{
			SAFE_FREE(g_pVideoFrameBuff[i]->pBuff);
			if(g_pVideoFrameBuff[i]->yTexture != 0)
				glDeleteTextures(1, &g_pVideoFrameBuff[i]->yTexture);
			if(g_pVideoFrameBuff[i]->uTexture != 0)
				glDeleteTextures(1, &g_pVideoFrameBuff[i]->uTexture);
			if(g_pVideoFrameBuff[i]->vTexture != 0)
				glDeleteTextures(1, &g_pVideoFrameBuff[i]->vTexture);

			SAFE_FREE(g_pVideoFrameBuff[i]);
		}
	}

	RepostWhizDbg("Free Image Buffers");

	for(i = 0; i < OPEN_LIMIT; i++)
	{
		if(g_pImageBuff[i])
		{
			SAFE_FREE(g_pImageBuff[i]->pBuff);
			if(g_pImageBuff[i]->texture != 0)
				glDeleteTextures(1, &g_pImageBuff[i]->texture);

			SAFE_FREE(g_pImageBuff[i]);
		}
	}

	RepostWhizDbg("Uninit End");

	return 0;
}

void set_blur_enable(int enable)
{
	g_isBlur = enable;
}

void set_blur_values(int blurRadius, char *vertexShader, char *fragmentShader)
{
	SetBlurParams(blurRadius, vertexShader, fragmentShader);
}

void load_image(char *pBuff, int width, int height, int drawWidth, int drawHeight,
		int framePosX, int framePosY, int showPosX, int showPosY, int frameWidth)
{
	if(!pBuff)
		return;

	int open_index = find_open_image_index();
	if ( open_index < 0 )
	{
		RepostWhizLog("There is no empty media context");
		return;
	}
	//------------------------------------------------------------------------//

	int buff_size = width * height * 4;
	g_pImageBuff[open_index] = (ImageBuff*)malloc(sizeof(ImageBuff));
	memset(g_pImageBuff[open_index], 0 , sizeof(ImageBuff));

	g_pImageBuff[open_index]->pBuff = (char*)malloc(buff_size);
	memcpy(g_pImageBuff[open_index]->pBuff, pBuff, buff_size);
	g_pImageBuff[open_index]->nBuffSize = buff_size;
	g_pImageBuff[open_index]->width = width;
	g_pImageBuff[open_index]->height = height;
	g_pImageBuff[open_index]->drawWidth = drawWidth;
	g_pImageBuff[open_index]->drawHeight = drawHeight;
	g_pImageBuff[open_index]->framePosX = framePosX;
	g_pImageBuff[open_index]->framePosY = framePosY;
	g_pImageBuff[open_index]->showPosX = showPosX;
	g_pImageBuff[open_index]->showPosY = showPosY;
	g_pImageBuff[open_index]->frameWidth = frameWidth;
	g_pImageBuff[open_index]->texture = 0;

	//------------------------------------------------------------------------//
}

void set_video_transinfo(int open_index, int width, int height, int drawWidth, int drawHeight,
		int framePosX, int framePosY, int showPosX, int showPosY, int frameWidth)
{
	if(!g_pVideoFrameBuff[open_index])
		return;

	//------------------------------------------------------------------------//

	g_pVideoFrameBuff[open_index]->thumbWidth = width;
	g_pVideoFrameBuff[open_index]->thumbHeight = height;
	g_pVideoFrameBuff[open_index]->drawWidth = drawWidth;
	g_pVideoFrameBuff[open_index]->drawHeight = drawHeight;
	g_pVideoFrameBuff[open_index]->framePosX = framePosX;
	g_pVideoFrameBuff[open_index]->framePosY = framePosY;
	g_pVideoFrameBuff[open_index]->showPosX = showPosX;
	g_pVideoFrameBuff[open_index]->showPosY = showPosY;
	g_pVideoFrameBuff[open_index]->frameWidth = frameWidth;

	//------------------------------------------------------------------------//
}

void set_video_rotation(int open_index, int rotation)
{
	if(!g_pVideoFrameBuff[open_index])
		return;

	//------------------------------------------------------------------------//

	g_pVideoFrameBuff[open_index]->rotation += rotation;
	if(g_pVideoFrameBuff[open_index]->rotation >= 360)
		g_pVideoFrameBuff[open_index]->rotation = 0;
	//------------------------------------------------------------------------//
}
int load_media( const char * file_name, int media_type)
{
	int result;
	int open_index;
	int i;
	int media_stream_idx;
	AVCodec * codec;
	MediaContext * mediaCtx;

	if ( file_name == NULL ){
		RepostWhizErr("Failed to retrieve Media File Name");
		return ERR_FILENAME;
	}

	RepostWhizDbg("Open Media File %s", file_name);

	//Find out empty open index;
	open_index = find_open_index();
	if ( open_index < 0 )
	{
		RepostWhizLog("There is no empty media context");
		return open_index;
	}

	RepostWhizDbg("Open Media step1");

	//Initialize MediaContext structure.
	mediaCtx = (MediaContext *) malloc(sizeof(MediaContext));
	mediaCtx->formatCtx = NULL;
	mediaCtx->codecCtx = NULL;
	mediaCtx->swsCtx = NULL;
	mediaCtx->ref_count = 1;
	mediaCtx->streamIdx = 0;
	mediaCtx->totalTime = 0;
	mediaCtx->abort_request = 0;

	mediaCtx->seek_request = 0;

	mediaCtx->pts_request = 0;
	mediaCtx->sample_fmt = AV_SAMPLE_FMT_S16;
	mediaCtx->sample_rate = 44100;
	mediaCtx->channels = 2;
	mediaCtx->audio_frame = NULL;
	mediaCtx->resample_ctx = NULL;
	mediaCtx->eof = 0;

	media_queue_init(&mediaCtx->mediaQueue);

	RepostWhizDbg("Open Media step2");

	pthread_mutex_init(&mediaCtx->mutex_seek, NULL);
	pthread_mutex_init(&mediaCtx->mutex_free, NULL);

	//Open Input File to open_index media context.
	//If failed to open input file, return ERR_OPEN_INPUT_FILE error code.
	//result = av_open_input_file(&mediaCtx->formatCtx, file_name, NULL, 0, NULL);
	result = avformat_open_input(&mediaCtx->formatCtx, file_name, NULL, NULL);
	if (result < 0)
	{
		RepostWhizErr("Failed to open input file by av_open_input_file function, result=%d", result);
		mediaCtx->ref_count = 0;
		unload_mediacontext(mediaCtx);
		RepostWhizErr("3");
		//return ERR_OPEN_INPUT_FILE;
		return -1;
	}

	RepostWhizDbg("Open Media step3");

	if (mediaCtx->formatCtx == NULL){
		RepostWhizDbg("Open Media step3-1");
	}

	//Find out stream info from AVFormatContext.
	//If failed to find out stream info, return ERR_FIND_STREAM_INFO error code.
	result = av_find_stream_info(mediaCtx->formatCtx);
	if (result < 0)
	{
		RepostWhizErr("Failed to find out stream info by av_find_stream_info function");
		mediaCtx->ref_count = 0;
		unload_mediacontext(mediaCtx);
		return ERR_FIND_STREAM_INFO;
	}

	RepostWhizDbg("Open Media step4");

	mediaCtx->mediaType = media_type;
	mediaCtx->pts_request = AV_NOPTS_VALUE;

	//Find out matched media type from format context's opened streams.
	media_stream_idx = -1;
	for ( i = 0; i < mediaCtx->formatCtx->nb_streams; i ++ ){
		if (mediaCtx->formatCtx->streams[i]->codec->codec_type == media_type){
			media_stream_idx = i;
			break;
		}
	}

	RepostWhizDbg("Open Media step5");

	//If there is no matched media type, return ERR_NO_MEDIA_TYPE error code.
	if (media_stream_idx == -1)
	{
		mediaCtx->ref_count = 0;

		RepostWhizLog("There is no matched media type");
		unload_mediacontext(mediaCtx);
		return ERR_NO_MEDIA_TYPE;
	}

	//Set codec context to codex context of opened index's media context.
	mediaCtx->codecCtx = mediaCtx->formatCtx->streams[media_stream_idx]->codec;

	RepostWhizDbg("Open Media step6");

	//Find out decoder codec
	codec = avcodec_find_decoder(mediaCtx->codecCtx->codec_id);
	if (!codec)
	{
		mediaCtx->ref_count = 0;

		RepostWhizErr("Failed to find out decoder codec");
		unload_mediacontext(mediaCtx);
		return ERR_NO_DECODER_CODEC;
	}

	int rotation = 0;

	if(media_type == AVMEDIA_TYPE_VIDEO){

		// get video rotation.
		AVStream* vstream = mediaCtx->formatCtx->streams[media_stream_idx];

		if(vstream)
		{
			AVDictionary* dic = vstream->metadata;
			if(dic && !(av_dict_count(dic) == 1 && av_dict_get(dic, "language", NULL, 0))){

				AVDictionaryEntry *tag=NULL;

				while((tag=av_dict_get(dic, "", tag, AV_DICT_IGNORE_SUFFIX))) {

					if(strcmp("language", tag->key)){
						if (!strcmp(tag->key, "rotate"))
							rotation = atoi(tag->value);
						break;
					}
				}
			}

			// set rotation
			if (rotation != 90 &&
					rotation != 180 &&
					rotation != 270){
				rotation = 0;
			}
		}
	}

	// decoding option. for smooth play
	av_opt_set_int(mediaCtx->codecCtx, "refcounted_frames", 1, 0);

	//Open decoder codec
#if LIBAVCODEC_VERSION_INT >= ((53<<16)+(8<<8)+0)
	result = avcodec_open2( mediaCtx->codecCtx, codec, NULL );
#else
	result = avcodec_open(mediaCtx->codecCtx, codec);
#endif

	if (result != 0)
	{
		mediaCtx->ref_count = 0;

		RepostWhizErr("Failed to open decoder codec");
		unload_mediacontext(mediaCtx);
		return ERR_OPEN_DECODER_CODEC;
	}

	RepostWhizDbg("Open Media step7");

	//all good, set index so that nativeProcess() can now recognize the media stream
	mediaCtx->streamIdx = media_stream_idx;
	mediaCtx->totalTime = av_q2d(mediaCtx->formatCtx->streams[media_stream_idx]->time_base) * mediaCtx->formatCtx->streams[media_stream_idx]->duration;
	mediaCtx->time_base = (double)av_q2d(mediaCtx->formatCtx->streams[mediaCtx->streamIdx]->time_base);
	mediaCtx->time_base_rev = (double)(1.0 / av_q2d(mediaCtx->formatCtx->streams[mediaCtx->streamIdx]->time_base));

	int den = mediaCtx->formatCtx->streams[media_stream_idx]->avg_frame_rate.den;
	int num = mediaCtx->formatCtx->streams[media_stream_idx]->avg_frame_rate.num;

	if (den > 0)
		mediaCtx->frame_rate = (int)((int)num / (int)den);
	else
		mediaCtx->frame_rate = -1;

	RepostWhizErr("Succeded opening media. frame_rate=%d, num=%d, den=%d", mediaCtx->frame_rate, num, den);
//	RepostWhizLog("width = %d, height = %d, stride = %d", mediaCtx->codecCtx->width, mediaCtx->codecCtx->height, mediaCtx->stride);
//	RepostWhizLog("Media time base is %d", av_q2d(mediaCtx->formatCtx->streams[media_stream_idx]->time_base));
//	RepostWhizLog("Media duration is %d", mediaCtx->formatCtx->streams[media_stream_idx]->duration);
//	RepostWhizLog("Media M=%d, n=%d", mediaCtx->formatCtx->streams[mediaCtx->streamIdx]->time_base.den, mediaCtx->formatCtx->streams[mediaCtx->streamIdx]->time_base.num);

	pthread_mutex_lock(&g_mutexMedia);
	g_mediaCtx[open_index] = mediaCtx;
	pthread_mutex_unlock(&g_mutexMedia);

	// create video buffer,
	if(media_type == AVMEDIA_TYPE_VIDEO){

		//------------------------------------------------------------------------//
		int width = mediaCtx->codecCtx->width;
		int height = mediaCtx->codecCtx->height;

		if(width % 4)
		{
			int delta = width % 4;
			height += (4 - delta) * width / height;
			width += 4 - delta;
		}
		double yuvRate = 1.5;
		int buff_size = width * height * yuvRate;

		g_pVideoFrameBuff[open_index] = (FrameBuff*)malloc(sizeof(FrameBuff));
		memset(g_pVideoFrameBuff[open_index], 0 , sizeof(FrameBuff));

		g_pVideoFrameBuff[open_index]->pBuff = (char*)malloc(buff_size);
		g_pVideoFrameBuff[open_index]->nBuffSize = buff_size;
		g_pVideoFrameBuff[open_index]->width = width;
		g_pVideoFrameBuff[open_index]->height = height;
		g_pVideoFrameBuff[open_index]->rotation = rotation;

		g_pVideoFrameBuff[open_index]->thumbWidth = width;
		g_pVideoFrameBuff[open_index]->thumbHeight = height;
		g_pVideoFrameBuff[open_index]->drawWidth = width;
		g_pVideoFrameBuff[open_index]->drawHeight = height;
		g_pVideoFrameBuff[open_index]->framePosX = 0;
		g_pVideoFrameBuff[open_index]->framePosY = 0;
		g_pVideoFrameBuff[open_index]->showPosX = 0;
		g_pVideoFrameBuff[open_index]->showPosY = 0;
		g_pVideoFrameBuff[open_index]->frameWidth = width;

		//if(mediaCtx->codecCtx->codec_id == AV_CODEC_ID_H264)
		mediaCtx->isH264 = 0;

		if(width >= 1900 || height >= 1900)
			mediaCtx->isH264 = 0;
		//------------------------------------------------------------------------//
	}
	RepostWhizDbg("Load Media Ended.");

	return open_index;

}

void unload_mediacontext( MediaContext * mediaCtx )
{
	if ( mediaCtx == NULL )
		return;

	int cnt = 0;
	while (mediaCtx->ref_count > 0)
	{
		RepostWhizErr("unload_mediacontext while, mediaCtx->ref_count=%d", mediaCtx->ref_count);

		if (cnt++ > 200)
			break;

		usleep(2000);
	}

	if ( mediaCtx->codecCtx )
		avcodec_close(mediaCtx->codecCtx);

	if ( mediaCtx->formatCtx )
	{
		av_close_input_file(mediaCtx->formatCtx);
	}

	if ( mediaCtx->resample_ctx )
	{
		swr_free(&mediaCtx->resample_ctx);
		mediaCtx->resample_ctx = NULL;
	}

	if(mediaCtx->audio_frame)
	{
		avcodec_free_frame(&mediaCtx->audio_frame);
		mediaCtx->audio_frame = NULL;
	}

	media_queue_uninit(&mediaCtx->mediaQueue);

	pthread_mutex_destroy(&mediaCtx->mutex_seek);
	pthread_mutex_destroy(&mediaCtx->mutex_free);

	free(mediaCtx);
	mediaCtx = NULL;
}

void unload_media( int open_index )
{
	RepostWhizDbg("unload_media %d", open_index);

	MediaContext * mediaCtx;

	if ( open_index < 0 || open_index >= OPEN_LIMIT )
		return;

	pthread_mutex_lock(&g_mutexMedia);
	mediaCtx = g_mediaCtx[open_index];
	g_mediaCtx[open_index] = NULL;
	pthread_mutex_unlock(&g_mutexMedia);

	if ( mediaCtx == NULL )
		return;

	mediaCtx->abort_request = 1;
	mediaCtx->ref_count --;

	pthread_join(mediaCtx->read_thread, NULL);

	unload_mediacontext(mediaCtx);
	RepostWhizDbg("unload_media end");
}

int find_open_index()
{
	int open_index, i;

	open_index = ERR_NO_OPENINDEX;

	for ( i = 0; i < OPEN_LIMIT; i ++ )
	{
		if ( g_mediaCtx[i] == NULL )
		{
			open_index = i;
			break;
		}
	}

	return open_index;
}

int find_open_image_index()
{
	int open_index, i;

	open_index = ERR_NO_OPENINDEX;

	for ( i = 0; i < OPEN_LIMIT; i ++ )
	{
		if ( g_pImageBuff[i] == NULL )
		{
			open_index = i;
			break;
		}
	}

	return open_index;
}

void* media_read_thread(void* arg)
{
	int			ret;
	int			sleep_time;

	MediaContext * mediaCtx;
	AVPacket	pkt;
	AVFrame*	frame;

	mediaCtx = (MediaContext *) arg;
	if ( mediaCtx == NULL )
	{
		RepostWhizErr("media_read_thread null detected");
		return NULL;
	}

	RepostWhizLog("media_read_thread started");
	mediaCtx->ref_count ++;

	while(1)
	{
		sleep_time = 1000;

		RepostWhizLog("media_read_thread 1 --->");

		//If thread abort request flag is set, break from loop
		if ( mediaCtx->abort_request )
			break;

		//If media seek request flag is set, seek.
		if ( mediaCtx->seek_request )
		{
			pthread_mutex_lock(&mediaCtx->mutex_seek);

			int64_t seek_target = mediaCtx->seek_pos;
			int64_t seek_min = mediaCtx->seek_rel > 0 ? seek_target - mediaCtx->seek_rel + 2 : INT64_MIN;
			int64_t seek_max = mediaCtx->seek_rel < 0 ? seek_target - mediaCtx->seek_rel - 2 : INT64_MAX;

			mediaCtx->seek_request = 0;
			pthread_mutex_unlock(&mediaCtx->mutex_seek);

			RepostWhizLog("seek media to pos %d", seek_target);

			//Seek the media stream.
			//If succeded in seeking, clear packet queue.
			ret = avformat_seek_file(mediaCtx->formatCtx, mediaCtx->streamIdx, seek_min, seek_target, seek_max, mediaCtx->seek_flags);
			if (ret < 0)
			{
				RepostWhizErr("Failed to seek file");
			}
			else
			{
				media_queue_flush(&mediaCtx->mediaQueue);
			}

			//avcodec_flush_buffers(mediaCtx->codecCtx);
			mediaCtx->eof = 0;
		}

		//Find out the empty packet queue index.
		//If frame queue is full, continue the loop.
		if (is_media_queue_full(&mediaCtx->mediaQueue) > 0)
		{
			RepostWhizLog("media_read_thread <------ is_media_queue_full ");

			if ( mediaCtx->skip_request)
			{
				media_queue_flush(&mediaCtx->mediaQueue);
				usleep(50);
			}
			else
			{
				usleep(100);
			}

			continue;
		}

		//Read the media frame from file.
		ret = av_read_frame(mediaCtx->formatCtx, &pkt);

		if (ret != 0)
		{
			if ( ret == AVERROR_EOF )
			{
				//RepostWhizLog("media read thread. end of file has reached");
			}

			//RepostWhizErr("media_read_thread <------ av_read_frame fail ret=%d", ret);

			mediaCtx->eof = 1;

			//break;
			usleep(1000);
			continue;
		}

		//If media stream is not matched with media stream, continue the loop.
		if ( pkt.stream_index != mediaCtx->streamIdx )
		{
			RepostWhizLog("media_read_thread <------ stream_index no same, pkt.stream_index=%d, mediaCtx.stream_index=%d, pkt.dts=%d", pkt.stream_index, mediaCtx->streamIdx, pkt.dts);

			av_free_packet(&pkt);
			usleep(20);

			continue;
		}

		mediaCtx->eof = 0;

		if ( mediaCtx->skip_request )
		{
			RepostWhizLog("media_read_thread skip_request --> pkt.dts = %d, mediaCtx->pts_request = %d", (int) pkt.dts, (int) mediaCtx->pts_request);

			if ( pkt.dts < mediaCtx->pts_request )
			{
				av_free_packet(&pkt);
				usleep(20);
				continue;
			}

			//if ( !(pkt.flags & AV_PKT_FLAG_KEY) )
			//{
			//	av_free_packet(&pkt);
			//	usleep(100);
			//	continue;
			//}
		}

		if ( mediaCtx->mediaType == CODEC_TYPE_VIDEO )
		{
			RepostWhizLog("media_read_thread 4-1");

			if (decode_video_frame(mediaCtx, &pkt, &frame) > 0)
			{
				mediaCtx->skip_request = 0;
				media_queue_push(&mediaCtx->mediaQueue, frame);
			}
			else
			{
				if (mediaCtx->skip_request)
				{
					sleep_time = 100;
				}
			}

			av_free_packet(&pkt);

			RepostWhizLog("media_read_thread 4-2");
		}
		else // CODEC_TYPE_AUDIO
		{
			AVPacket bef_pkt = pkt;

			RepostWhizLog("media_read_thread dts=%d", (int) pkt.dts);

			decode_audio_frame(mediaCtx, &pkt);
			av_free_packet(&bef_pkt);
		}

		usleep(sleep_time);

		RepostWhizLog("media_read_thread packet_queue_pushed <------ ");
	}

	mediaCtx->ref_count --;

	RepostWhizDbg("media_read_thread end");

	return NULL;
}

// decode video packet to frame
int decode_video_frame(MediaContext * mediaCtx, AVPacket* pkt_in, AVFrame** frame_out)
{
	AVFrame * frame;
	int got_picture;

	RepostWhizLog("decode_video_frame 9-1");

	frame = avcodec_alloc_frame();
	avcodec_decode_video2(mediaCtx->codecCtx, frame, &got_picture, pkt_in);

	av_free_packet(pkt_in);

	if (got_picture) {
		*frame_out = frame;
		(*frame_out)->pts = pkt_in->dts;

		RepostWhizLog("decode_video_frame 9-2");
		return 1;
	}

	RepostWhizLog("decode_video_frame fail 9-3");

	SAFE_AVVIDEO_FREE(frame);
	return 0;
}

void* audio_read_thread(void* arg)
{
	int			ret;

	MediaContext * mediaCtx;
	AVPacket	pkt;
	AVFrame*	frame;

	mediaCtx = (MediaContext *) arg;
	if ( mediaCtx == NULL )
	{
		RepostWhizErr("audio_read_thread null detected");
		return NULL;
	}

	RepostWhizLog("audio_read_thread started");
	mediaCtx->ref_count ++;

	while(1)
	{
		RepostWhizLog("audio_read_thread 1 --->");

		//If thread abort request flag is set, break from loop
		if ( mediaCtx->abort_request )
			break;

		//If media seek request flag is set, seek.
		if ( mediaCtx->seek_request )
		{
			pthread_mutex_lock(&mediaCtx->mutex_seek);

			int64_t seek_target = mediaCtx->seek_pos;

			mediaCtx->seek_request = 0;

			pthread_mutex_unlock(&mediaCtx->mutex_seek);

			//Seek the media stream.
			//If succeded in seeking, clear packet queue.
			ret = avformat_seek_file(mediaCtx->formatCtx, mediaCtx->streamIdx,
					0, seek_target, seek_target, AVSEEK_FLAG_FRAME);


			if (ret < 0){
				RepostWhizErr("Failed to seek file");
			}else{
				media_queue_flush(&mediaCtx->mediaQueue);
			}

			//avcodec_flush_buffers(mediaCtx->codecCtx);
			mediaCtx->eof = 0;
		}

		//Find out the empty packet queue index.
		//If frame queue is full, continue the loop.
		if (is_media_queue_full(&mediaCtx->mediaQueue) > 0)
		{
			RepostWhizLog("audio_read_thread <------ is_media_queue_full ");
			usleep(10000);	//10ms
			continue;
		}

		//Read the media frame from file.
		ret = av_read_frame(mediaCtx->formatCtx, &pkt);

		if (ret != 0)
		{
			RepostWhizLog("audio_read_thread <------ av_read_frame fail ret=%d", ret);
			mediaCtx->eof = 1;

			//break;
			usleep(100000); //100ms
			continue;
		}

		//If media stream is not matched with media stream, continue the loop.
		if ( pkt.stream_index != mediaCtx->streamIdx )
		{
			av_free_packet(&pkt);
			usleep(1000); // 1ms

			continue;
		}

		mediaCtx->eof = 0;

		AVPacket bef_pkt = pkt;

		RepostWhizLog("audio_read_thread dts=%d", (int) pkt.dts);

		decode_audio_frame(mediaCtx, &pkt);
		RepostWhizLog("decode_audio_frame success");

		av_free_packet(&bef_pkt);
		RepostWhizLog("av_free_packet success");

		usleep(1000); //1ms

	}

	mediaCtx->ref_count --;

	RepostWhizLog("audio_read_thread end");

	return NULL;
}

AVFrame* get_seeked_frame(MediaContext * mediaCtx)
{
	AVPacket pkt;

	AVFrame* frame = NULL;
	AVFrame* frame_prv = NULL;

	pthread_mutex_lock(&mediaCtx->mutex_seek);
	mediaCtx->seek_request = 0;
	int64_t seek_target = mediaCtx->seek_pos;
	pthread_mutex_unlock(&mediaCtx->mutex_seek);

	//Seek the media stream.
	//If succeded in seeking, clear packet queue.

	if (avformat_seek_file(mediaCtx->formatCtx, mediaCtx->streamIdx,
			0, seek_target, seek_target, AVSEEK_FLAG_FRAME) < 0){
		RepostWhizErr("Failed to seek file");
		return NULL;
	}

	int64_t seek_delta = (int64_t)((double)SEEK_DELTA_SEC / mediaCtx->time_base);

	RepostWhizLog("seek_video_frame to pos %lld, seek_delta=%lld", seek_target, seek_delta);

	//flush avcodec buffer
	avcodec_flush_buffers(mediaCtx->codecCtx);
	mediaCtx->eof = 0;

	while(1)
	{
		//Read the media frame from file.
		if (av_read_frame(mediaCtx->formatCtx, &pkt) != 0){

			RepostWhizErr("video_read_thread <------ av_read_frame fail");
			mediaCtx->eof = 1;
			break;
		}

		if ( pkt.stream_index != mediaCtx->streamIdx ){
			av_free_packet(&pkt);
			continue;
		}

		if (decode_video_frame(mediaCtx, &pkt, &frame) > 0)
		{
			if (seek_target - frame->pts < seek_delta){

				SAFE_AVVIDEO_FREE(frame_prv);
				break;
			}

			// delete prev frame.
			SAFE_AVVIDEO_FREE(frame_prv);

			// backup prev frame.
			frame_prv = frame;
		}

		// little sleep.
		usleep(1);
	}

	RepostWhizLog("seek_video_frame finish, pts=%lld", frame->pts);

	return frame;
}

void* video_read_thread(void* arg)
{
	int			ret;

	MediaContext * mediaCtx;
	AVPacket	pkt;
	AVFrame*	frame;

	mediaCtx = (MediaContext *) arg;
	if ( mediaCtx == NULL )
	{
		RepostWhizErr("video_read_thread null detected");
		return NULL;
	}

	RepostWhizLog("video_read_thread started");
	mediaCtx->ref_count ++;

	while(1)
	{
		RepostWhizLog("video_read_thread 1 --->");

		//If thread abort request flag is set, break from loop
		if ( mediaCtx->abort_request )
			break;

		//If media seek request flag is set, seek.
		if ( mediaCtx->seek_request )
		{
			// flush buffer.
			video_queue_flush(&mediaCtx->mediaQueue);

			frame = get_seeked_frame(mediaCtx);
			if (frame != NULL){

				//push seeked frame.
				media_queue_push(&mediaCtx->mediaQueue, frame);
			}
		}

		//Find out the empty packet queue index.
		//If frame queue is full, continue the loop.
		if (is_media_queue_full(&mediaCtx->mediaQueue) > 0)
		{
			RepostWhizLog("video_read_thread <------ is_media_queue_full ");
			usleep(10000);	//10ms
			continue;
		}

		//Read the media frame from file.
		ret = av_read_frame(mediaCtx->formatCtx, &pkt);

		if (ret != 0)
		{
			RepostWhizErr("video_read_thread <------ av_read_frame fail ret=%d", ret);

			mediaCtx->eof = 1;

			//break;
			usleep(10000); //100ms
			continue;
		}

		//If media stream is not matched with media stream, continue the loop.
		if ( pkt.stream_index != mediaCtx->streamIdx )
		{
			av_free_packet(&pkt);
			usleep(1000); // 1ms

			continue;
		}

		mediaCtx->eof = 0;

		RepostWhizLog("decode_video_frame prev pkt.pts=%lld, dts=%lld ", pkt.pts, pkt.dts);

		if (decode_video_frame(mediaCtx, &pkt, &frame) > 0)
		{
			RepostWhizLog("media_queue_push pkt.pts=%lld, pkt.dts=%lld, frame->pkt_dts=%lld, frame->pkt_pts=%lld,",
					pkt.pts, pkt.dts, frame->pkt_dts, frame->pkt_pts);

			media_queue_push(&mediaCtx->mediaQueue, frame);
		}

		usleep(1000); //1ms

		RepostWhizLog("video_read_thread packet_queue_pushed <------ ");
	}

	mediaCtx->ref_count --;

	RepostWhizLog("video_read_thread end");

	return NULL;
}

#define AVCODEC_MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio
int decode_audio_frame(MediaContext * mediaCtx, AVPacket* pkt_in)
{
	//RepostWhizDbg("decode_audio_frame s ---->");

	AVFrame * frame;
	int len1, len2, got_frame;
	int got_picture;

	uint8_t dec_buffer2[AVCODEC_MAX_AUDIO_FRAME_SIZE + FF_INPUT_BUFFER_PADDING_SIZE];
	int16_t * temp_buffer1, * temp_buffer2;
	int frame_size;
	int pts_sum;

	int wanted_nb_samples, resampled_data_size, decoded_data_size;
	int64_t dec_channel_layout;

	pts_sum = 0;

	while (pkt_in->size > 0)
	{
		if ( mediaCtx->abort_request )
		{
			RepostWhizErr("queue audio frame abort1");

			pkt_in->size = 0;
			break;
		}

		//RepostWhizDbg("decode_audio_frame 1-1 ---->");

		if(!mediaCtx->audio_frame){
			if(!(mediaCtx->audio_frame = avcodec_alloc_frame())){
				//RepostWhizDbg("decode_audio_frame 1-2 ---->");

				return AVERROR(ENOMEM);
			}
		} else{
			//RepostWhizDbg("decode_audio_frame 1-3 ---->");

			avcodec_get_frame_defaults(mediaCtx->audio_frame);
		}

		frame_size = AVCODEC_MAX_AUDIO_FRAME_SIZE;
		//RepostWhizDbg("decode_audio_frame 1-4 ---->");

		len1 = avcodec_decode_audio4(mediaCtx->codecCtx, mediaCtx->audio_frame, &got_frame, pkt_in);

		//RepostWhizDbg("decode_audio_frame 2, pkt_in->size=%d, len=%d", pkt_in->size, len1);

		if (len1 < 0)
		{
			RepostWhizErr("queue audio frame abort2");

			pkt_in->size = 0;
			continue;
		}

		pkt_in->data += len1;
		pkt_in->size -= len1;

		if (got_frame <= 0)
		{
			RepostWhizErr("queue audio frame abort3");
			//pkt.size = 0;
			continue;
		}

		decoded_data_size = av_samples_get_buffer_size(NULL, mediaCtx->audio_frame->channels, mediaCtx->audio_frame->nb_samples, mediaCtx->audio_frame->format, 1);
		frame_size = decoded_data_size;

		if (mediaCtx->audio_frame->channel_layout && mediaCtx->audio_frame->channels == av_get_channel_layout_nb_channels(mediaCtx->audio_frame->channel_layout)){

			RepostWhizLog("dec_channel_layout setting 1");
			dec_channel_layout = mediaCtx->audio_frame->channel_layout;
		}else{

			RepostWhizLog("dec_channel_layout setting 2");
			dec_channel_layout = av_get_default_channel_layout(mediaCtx->audio_frame->channels);
		}

		//dec_channel_layout = (mediaCtx->audio_frame->channel_layout && mediaCtx->audio_frame->channels == av_get_channel_layout_nb_channels(mediaCtx->audio_frame->channel_layout)) ? mediaCtx->audio_frame->channel_layout : av_get_default_channel_layout(mediaCtx->audio_frame->channels);
		wanted_nb_samples =  mediaCtx->audio_frame->nb_samples;

		if (mediaCtx->codecCtx->sample_fmt != mediaCtx->sample_fmt || mediaCtx->codecCtx->channels != mediaCtx->channels || mediaCtx->codecCtx->sample_rate != mediaCtx->sample_rate)
		{
			RepostWhizLog("decode_audio_frame convert, mediaCtx->codecCtx->sample_fmt=%d, mediaCtx->sample_fmt=%d, channel1=%d, channel2=%d, codecCtx->sample_rate=%d, mediaCtx->sample_rate=%d",
					mediaCtx->codecCtx->sample_fmt, mediaCtx->sample_fmt, mediaCtx->codecCtx->channels, mediaCtx->channels, mediaCtx->codecCtx->sample_rate, mediaCtx->sample_rate);

			if (mediaCtx->resample_ctx == NULL)
			{
				RepostWhizLog("Create audio resample context. %d, %d, %d, %d, %d, %d, %d, %d",
						(int) mediaCtx->channels, (int) mediaCtx->codecCtx->channels, (int)av_get_default_channel_layout(mediaCtx->channels),
						(int)av_get_default_channel_layout(mediaCtx->codecCtx->channels),(int) mediaCtx->sample_rate,
						(int) mediaCtx->codecCtx->sample_rate, (int) mediaCtx->sample_fmt, (int) mediaCtx->codecCtx->sample_fmt);

				mediaCtx->resample_ctx = swr_alloc_set_opts(NULL, av_get_default_channel_layout(mediaCtx->channels), mediaCtx->sample_fmt, mediaCtx->sample_rate,
						dec_channel_layout, mediaCtx->audio_frame->format, mediaCtx->audio_frame->sample_rate, 0, NULL);

				if(mediaCtx->resample_ctx == NULL){
					RepostWhizErr("swr_alloc_set_opts failed..");
					break;
				}else if(swr_init(mediaCtx->resample_ctx) < 0){
					RepostWhizErr("swr_init failed..");
					swr_free(&mediaCtx->resample_ctx);
					mediaCtx->resample_ctx = NULL;
					break;
				}
			}

			if (mediaCtx->resample_ctx == NULL)
			{
				RepostWhizErr("Failed to create audio resample context ");
				pkt_in->size = 0;
				break;
			}

			//RepostWhizErr("swr_convert s <----");

			const uint8_t **ibuf = (const uint8_t **)mediaCtx->audio_frame->extended_data;
			void *obuf[]= {dec_buffer2};
			len2 = swr_convert(mediaCtx->resample_ctx, obuf, (AVCODEC_MAX_AUDIO_FRAME_SIZE + FF_INPUT_BUFFER_PADDING_SIZE) / mediaCtx->channels / 2, ibuf, mediaCtx->audio_frame->nb_samples);
			frame_size = len2 * mediaCtx->channels * av_get_bytes_per_sample(mediaCtx->sample_fmt);

			//RepostWhizErr("swr_convert e <----");
			temp_buffer2 = dec_buffer2;
		}
		else
		{
			RepostWhizDbg("decode_audio_frame no convert");
			temp_buffer2 = mediaCtx->audio_frame->data[0];
		}

		RepostWhizLog("decode_audio_frame 5, pkt_in->size=%d", pkt_in->size);

		frame =(AVFrame*) av_malloc(frame_size + sizeof(AVFrame) + FF_INPUT_BUFFER_PADDING_SIZE);
		frame->data[0] = (uint8_t *) frame + sizeof(AVFrame);
		frame->linesize[0] = frame_size;
		frame->linesize[1] = 0;
		frame->linesize[2] = frame_size;
		frame->pkt_dts = (int64_t)(av_q2d(mediaCtx->formatCtx->streams[mediaCtx->streamIdx]->time_base) * (double) pkt_in->dts * 1000.0) + pts_sum;
		pts_sum += get_time_from_byte(mediaCtx, frame_size);
		memcpy(frame->data[0], temp_buffer2, frame_size);

		media_queue_push(&mediaCtx->mediaQueue, frame);
	}

	//RepostWhizDbg("decode_audio_frame e <----");

	return 1;
}

int get_audio_frame(int open_index, void * buffer, int buf_size, double sec)
{
	RepostWhizDbg("------> get audio frame startedx. sec=%f, buf_size=%d", sec, buf_size);

	MediaContext * mediaCtx;
	AVFrame * audio;
	int res, bytes_request;
	int64_t pts_start, pts_end, pts_request;
	int8_t * buffer_request;
	int retSize = 0;

	if ( open_index < 0 || open_index >= OPEN_LIMIT ){
		RepostWhizErr("get_audio_frame open_index  is null open_index=%d", open_index);
		return -1;
	}

	pthread_mutex_lock(&g_mutexMedia);
	mediaCtx = g_mediaCtx[open_index];
	if ( mediaCtx == NULL )
	{
		RepostWhizErr("get_audio_frame mediaCtx is null.");

		pthread_mutex_unlock(&g_mutexMedia);
		return -1;
	}
	pthread_mutex_unlock(&g_mutexMedia);

	mediaCtx->ref_count ++;

	pts_request = (int64_t) (sec * 1000);
	bytes_request = buf_size;//nb_samples * mediaCtx->channels * 2;
	buffer_request = (int8_t *) buffer;

	RepostWhizDbg("get audio frame started. sec=%f, buf_size=%d, pts_request=%d", sec, buf_size, (int)pts_request);

	while ( bytes_request > 0 )
	{
		if ( mediaCtx->abort_request || mediaCtx->seek_request)
		{
			RepostWhizErr("get_audio_frame mediaCtx->abort_request is true, %d", mediaCtx->abort_request);

			mediaCtx->ref_count --;
			return -1;
		}

		res = media_queue_get(&mediaCtx->mediaQueue, &audio, 0);
		if ( res < 0 )
		{
			if ( mediaCtx->eof )
			{
				RepostWhizErr("get_audio_frame mediaCtx->eof is null.");

				mediaCtx->ref_count --;
				return -2;
			}

			RepostWhizDbg("media_queue_get is blank, usleep 5ms");
			usleep(1000); //5ms
			continue;
		}

		pts_start = audio->pkt_dts + get_time_from_byte(mediaCtx, audio->linesize[1]);
		pts_end = pts_start + get_time_from_byte(mediaCtx, audio->linesize[2]);

		RepostWhizDbg("get audio frame while bytes_request=%d, pts_start=%d, pts_end=%d, audio->linesize[1]=%d, audio->linesize[2]=%d, queue_size=%d",
						(int)bytes_request, (int)pts_start, (int)pts_end, audio->linesize[1], audio->linesize[2], mediaCtx->mediaQueue.nArraySize);

		int64_t	SEC_DELTA_BIG = 3000;

		if ((pts_request - pts_end) > SEC_DELTA_BIG || (pts_start - pts_request) > SEC_DELTA_BIG)
		{
			mediaCtx->ref_count --;

			RepostWhizDbg("get_audio_frame audio must seek., pts_start=%d, pts_end=%d, pts_request=%d", (int)pts_start, (int)pts_end, (int)pts_request);
			// return -2 for seek in out side.
			return -2;
		}

		if ( pts_end <= pts_request )
		{
			RepostWhizDbg("Skip frame %d, %d, %d", (int) pts_start, (int) pts_end, (int) pts_request);
			pthread_mutex_lock(&mediaCtx->mutex_free);
			res = media_queue_pop(&mediaCtx->mediaQueue, &audio);
			if(res > 0)
				SAFE_AV_FREE(audio);
			pthread_mutex_unlock(&mediaCtx->mutex_free);
			continue;
		}

		if ( pts_start < pts_request - 1 )
		{
			int64_t temp = get_byte_from_time(mediaCtx, pts_request - pts_start);

			if ( temp < audio->linesize[2] )
			{
				audio->linesize[1] += temp;
				audio->linesize[2] -= temp;
			}
			else
			{
				RepostWhizDbg("Skip frame %d, %d, %d", (int) pts_start, (int) pts_end, (int) pts_request);
				pthread_mutex_lock(&mediaCtx->mutex_free);
				res = media_queue_pop(&mediaCtx->mediaQueue, &audio);
				if(res > 0)
					SAFE_AV_FREE(audio);
				pthread_mutex_unlock(&mediaCtx->mutex_free);
				continue;
			}
		}

		RepostWhizDbg("audio->linesize[2]=%d, bytes_request=%d", audio->linesize[2], bytes_request);
		if ( audio->linesize[2] <= bytes_request )
		{
			memcpy(buffer_request, audio->data[0] + audio->linesize[1], audio->linesize[2]);
			retSize += audio->linesize[2];
			bytes_request -= audio->linesize[2];
			buffer_request += audio->linesize[2];
			pts_request += get_time_from_byte(mediaCtx, audio->linesize[2]);

			RepostWhizDbg("audio frame duration1");
			pthread_mutex_lock(&mediaCtx->mutex_free);
			res = media_queue_pop(&mediaCtx->mediaQueue, &audio);
			if(res > 0)
				SAFE_AV_FREE(audio);
			pthread_mutex_unlock(&mediaCtx->mutex_free);
		}
		else
		{
			RepostWhizLog("copy audio buffer linesize1 = %d, linesize2 = %d", audio->linesize[1], audio->linesize[2]);
			memcpy(buffer_request, audio->data[0] + audio->linesize[1], bytes_request);

			//RepostWhizDbg("audio frame duration2");

			retSize += bytes_request;
			audio->linesize[1] += bytes_request;
			audio->linesize[2] -= bytes_request;
			bytes_request = 0;
		}
	}

	RepostWhizDbg("<------- get audio frame successed.");

	mediaCtx->ref_count --;
	return retSize;
}


int start_media_thread( int open_index )
{
	MediaContext * mediaCtx;
	int res;

	if ( open_index < 0 || open_index >= OPEN_LIMIT )
		return -1;

	pthread_mutex_lock(&g_mutexMedia);
	mediaCtx = g_mediaCtx[open_index];	
	pthread_mutex_unlock(&g_mutexMedia);
	if ( mediaCtx == NULL )
		return -1;

	RepostWhizDbg("Start Media Read Thread");

	if ( mediaCtx->mediaType == CODEC_TYPE_AUDIO )
		res = pthread_create(&mediaCtx->read_thread, NULL, audio_read_thread, (void *) mediaCtx);
	else
		res = pthread_create(&mediaCtx->read_thread, NULL, video_read_thread, (void *) mediaCtx);

	if ( res != 0 ){
		RepostWhizDbg("Failed to start media read thread");
	}

	RepostWhizDbg("start media read thread success.");

	return 1;
}

int  _get_video_frame(MediaContext* mediaCtx, double sec, AVFrame** frame_out, int* video_must_free)
{
	if (mediaCtx == NULL)
		return -1;

	AVFrame* video = NULL;

	int res;
	int has_skipped = 0;

	double	SEC_DELTA_BIG = 3.0;
	double	SEC_DELTA_DELAY = 0.5;
	double	SEC_DELTA_PRIOR = 0.1;

	double	pkt_sec;
	double	delta;

	int index;
	int sleep_time;

	mediaCtx->ref_count ++;
	mediaCtx->pts_request = (int64_t) (sec * mediaCtx->time_base_rev);

	if (video_must_free)
		*video_must_free = 1;

	RepostWhizLog("<<<============ _get_video_frame begin, pts_request=%d, sec=%f, time_base_rev=%f", (int) mediaCtx->pts_request, (float) sec, mediaCtx->time_base_rev);

	while ( 1 )
	{
		sleep_time = 1000;

		if ( mediaCtx->abort_request )
		{
			RepostWhizErr("catch_video_frame mediaCtx->abort_reques=1");

			media_queue_flush(&mediaCtx->mediaQueue);
			mediaCtx->ref_count --;
			return -1;
		}

		video = NULL;

		res = media_queue_get(&mediaCtx->mediaQueue, &video, 0);

		if (res >= 0)
		{
			pkt_sec = video->pkt_dts * mediaCtx->time_base;
			delta = pkt_sec - sec;

			if ( delta > SEC_DELTA_PRIOR)
			{
				RepostWhizLog("media_queue_get 0 video->pkt_dts=%d, mediaCtx->pts_request=%d, delta=%f, sec=%f, pkt_sec=%f, queue_size=%d", (int)video->pkt_dts, (int)mediaCtx->pts_request, delta, sec, pkt_sec, mediaCtx->mediaQueue.nArraySize);
				*video_must_free = 0;
				res = 1;
				break;
			}
		}

		res = media_queue_pop(&mediaCtx->mediaQueue, &video);
		if (res >= 0)
		{
			pkt_sec = video->pkt_dts * mediaCtx->time_base;
			delta = sec - pkt_sec;

			RepostWhizLog("media_queue_pop 0 video->pkt_dts=%d, mediaCtx->pts_request=%d, delta=%f, sec=%f, pkt_sec=%f, queue_size=%d", (int)video->pkt_dts, (int)mediaCtx->pts_request, delta, sec, pkt_sec, mediaCtx->mediaQueue.nArraySize);

			// video->pkt_dts is bigger than mediaCtx->pts_request
			if ( delta <= 0.0 ){
				res = 1;
				break;
			}

			sleep_time = 100; // be quick

			if (delta > SEC_DELTA_DELAY)
			{
				//RepostWhizDbg("mediaCtx->skip_request = 1,  video->pkt_dts=%d, mediaCtx->pts_request=%d", (int)video->pkt_dts, (int)mediaCtx->pts_request);

				if ( has_skipped == 0 )
				{
					media_queue_flush(&mediaCtx->mediaQueue);

					has_skipped = 1;
					mediaCtx->skip_request = 1;
				}
				sleep_time = 300;
			}

			SAFE_AV_FREE(video);

		}

		if ( mediaCtx->eof )
		{
			RepostWhizErr("_get_video_frame mediaCtx->eof=1 ");

			media_queue_flush(&mediaCtx->mediaQueue);
			mediaCtx->ref_count --;
			return -1;
		}

		usleep(sleep_time);
	}

	RepostWhizLog("_get_video_frame succeded ===================>>>>");

	*frame_out = video;
	mediaCtx->ref_count --;

	return 1;
}

int  _get_video_frame_by_order(MediaContext* mediaCtx, AVFrame** frame_out)
{
	if (mediaCtx == NULL)
		return -1;

	AVFrame* video = NULL;

	int res;

	mediaCtx->ref_count ++;

	RepostWhizLog("<<<============ _get_video_frame begin, pts_request=%d, time_base_rev=%f", (int) mediaCtx->pts_request, mediaCtx->time_base_rev);
	int nWhile = 0;

	while ( 1 )
	{
		if ( mediaCtx->abort_request || nWhile ++ > 1000)
		{
			RepostWhizErr("queue audio frame abort1");

			media_queue_flush(&mediaCtx->mediaQueue);
			mediaCtx->ref_count --;

			return -1;
		}

		video = NULL;
		if (media_queue_pop(&mediaCtx->mediaQueue, &video) >= 0)
			break;

		if ( mediaCtx->eof )
		{
			RepostWhizErr("_get_video_frame mediaCtx->eof=1 ");

			media_queue_flush(&mediaCtx->mediaQueue);
			mediaCtx->ref_count --;
			return -1;
		}

		usleep(1000);	//1ms
	}

	RepostWhizLog("_get_video_frame succeded ===================>>>>");

	*frame_out = video;
	mediaCtx->ref_count --;

	return 1;
}

int	get_frame_rate(int open_index)
{
	if ( open_index < 0 || open_index >= OPEN_LIMIT )
		return -1;

	MediaContext*	mediaCtx;

	pthread_mutex_lock(&g_mutexMedia);
	mediaCtx = g_mediaCtx[open_index];
	if ( mediaCtx == NULL )
	{
		RepostWhizErr("get_frame_rate error!, mediaCtx is null.");

		pthread_mutex_unlock(&g_mutexMedia);
		return -1;
	}
	pthread_mutex_unlock(&g_mutexMedia);

	return mediaCtx->frame_rate;
}

// set seek flag, call this function for seek
int wait_untill_seek(MediaContext* mediaCtx, double seek_sec)
{
	if (mediaCtx == NULL)
		return -1;

	pthread_mutex_lock(&mediaCtx->mutex_seek);
	mediaCtx->seek_request = 1;
	mediaCtx->seek_pos = seek_sec * mediaCtx->time_base_rev;
	pthread_mutex_unlock(&mediaCtx->mutex_seek);

	video_queue_flush(&mediaCtx->mediaQueue);

	int cnt = 100;
	while(mediaCtx->seek_request){

		usleep(1000); //1ms
		if (cnt -- < 0)
			break;
	}

	return 1;
}

/*
 *		--------------------|-----------|--------------------------------|---------------------------
 *		------ R1 ----------| delta R2  |	 frame queue     R3          |-------- R4 --------------
 *		--------------------|-----------|--------------------------------|--------------------------
 *		                              secMin                          secMax
 * */
/*
 * PARAMS
 * 	must_free: create frame in the function, must free outside
 * */
int get_video_frame2(MediaContext* mediaCtx, double sec, AVFrame** frame_out, int* must_free)
{
	if (mediaCtx == NULL)
		return -1;

	AVFrame* video = NULL;

	int res;
	int has_skipped = 0;

	double	pkt_sec;	// package sec
	int		inner_seeked = 0;

	// sleep time
	int sleep_time;

	RepostWhizLog(" ");
	RepostWhizLog("get_video_frame2 s ");

	mediaCtx->ref_count ++;
	*must_free = 1;

	while(1){

		if ( mediaCtx->abort_request )
		{
			RepostWhizErr("catch_video_frame mediaCtx->abort_reques=1");

			video_queue_flush(&mediaCtx->mediaQueue);
			mediaCtx->ref_count --;
			return -1;
		}

		video = NULL;

		int64_t ptsmin = 0;
		int64_t ptsmax = 0;

		// get first video frame from queue.
		res = media_queue_get_with_range(&mediaCtx->mediaQueue, &video, 0, &ptsmin, &ptsmax);

		if (res >= 0)
		{
			RepostWhizLog("get_video_frame2 1 res=%d, ptsmin=%lld, ptsmax=%lld, size=%lld, video.pts=%lld, pkt_pts=%lld, pkt_dts=%lld",
					res, ptsmin, ptsmax, mediaCtx->mediaQueue.nArraySize, video->pts, video->pkt_pts, video->pkt_dts);

			// get first frame, but no pop it.
			double secMin = ptsmin * mediaCtx->time_base;
			double secMax = ptsmax * mediaCtx->time_base;

			// it is seek ones.
			if (inner_seeked > 0){
				RepostWhizDbg("get_video_frame2 2 sec=%.1f, secMin=%.1f, secMax=%.1f", sec*1000, secMin*1000, secMax*1000);

				*must_free = 0;
				break;
			}

			//  case R1 , seek
			if (sec <=  secMin - DELTA){

				RepostWhizDbg("get_video_frame2 case 1 pts_seek=%lld, ptsmin=%lld, ptsmax=%lld, sec=%.1f, secMin=%.1f, secMax=%.1f",
						(int64_t)(sec * mediaCtx->time_base_rev), ptsmin, ptsmax, sec*1000, secMin*1000, secMax*1000);

				inner_seeked = 1;
				wait_untill_seek(mediaCtx, sec);
			}
			// case R2
			else if ((sec > secMin - DELTA) && (sec <= secMin)){

				RepostWhizDbg("get_video_frame2 case 2");

				*must_free = 0;
				break;
			}
			// case R3
			else if ((sec > secMin) && (sec <= secMax + DELTA)){

				RepostWhizDbg("get_video_frame2 case4");

				// pop frame from queue.
				AVFrame* nextframe = NULL;
				double next_sec = 0;
				while(media_queue_get_with_range(&mediaCtx->mediaQueue, &video, 0, &ptsmin, &ptsmax) > 0)
				{
					pkt_sec = video->pkt_dts * mediaCtx->time_base;
					if(media_queue_get_with_range(&mediaCtx->mediaQueue, &nextframe, 1, &ptsmin, &ptsmax) > 0)
					{
						next_sec = nextframe->pkt_dts * mediaCtx->time_base;

						if ((next_sec > sec)){
							*must_free = 0;
							break;
						}
					}
					else
					{
						// return frame , when frame exist in queue only one
						*must_free = 0;
						break;
					}

					//RepostWhizDbg("get_video_frame2 SAFE_AVVIDEO_FREE, video->pts=%lld", video->pts);

					media_queue_pop(&mediaCtx->mediaQueue, &video);

					SAFE_AVVIDEO_FREE(video);
				}

				break;
			}
			// case R4, seek
			else{

				RepostWhizDbg("get_video_frame2 case 4 pts_seek=%lld, ptsmin=%lld, ptsmax=%lld, sec=%.1f, secMin=%.1f, secMax=%.1f",
						(int64_t)(sec * mediaCtx->time_base_rev), ptsmin, ptsmax, sec*1000, secMin*1000, secMax*1000);

				inner_seeked = 1;
				wait_untill_seek(mediaCtx, sec);
			}
		}

		if ( mediaCtx->eof )
		{
			RepostWhizErr("get_video_frame2 mediaCtx->eof=1 ");

			video_queue_flush(&mediaCtx->mediaQueue);
			mediaCtx->ref_count --;
			return -1;
		}

		usleep(10);
	}

	RepostWhizLog("get_video_frame2 e");

	*frame_out = video;
	mediaCtx->ref_count --;

	return 1;
}

// catch video frame
int catch_video_frame( int open_index, double sec)
{

	MediaContext*	mediaCtx;
	AVFrame*		video = NULL;
	FrameBuff*		pVideoFrameBuff = NULL;

	int res;
	int index;
	int video_must_free;
	RepostWhizLog("catch_video_frame begin ===================>>>>");

	if ( open_index < 0 || open_index >= OPEN_LIMIT )
		return -1;

	RepostWhizLog("catch_video_frame step 1");

	pthread_mutex_lock(&g_mutexMedia);
	RepostWhizLog("catch_video_frame step 2");

	mediaCtx = g_mediaCtx[open_index];
	RepostWhizLog("catch_video_frame step 3");

	if ( mediaCtx == NULL )
	{
		RepostWhizErr("catch_video_frame error!, mediaCtx is null.");

		pthread_mutex_unlock(&g_mutexMedia);
		return -1;
	}

	pthread_mutex_unlock(&g_mutexMedia);

	RepostWhizLog("catch_video_frame step 4");
	if(mediaCtx->isH264 == 0)
	{

		// get video frame from Media queue.
		video_must_free = 1;
#if 0
		res = _get_video_frame_by_order(mediaCtx, &video);
#else
		res = _get_video_frame(mediaCtx, sec, &video, &video_must_free);
#endif

		if (res < 0){
			RepostWhizErr("_get_video_frame failed.");
			return -1;
		}

		RepostWhizLog("catch_video_frame pop framed video->pkt_dts=%d, mediaCtx->pts_request=%d", (int)video->pkt_dts, (int)mediaCtx->pts_request);

		pthread_mutex_lock(&g_mutexMedia);

		if ( avpicture_fill((AVPicture*)m_frame_scale, (uint8_t*)g_pVideoFrameBuff[open_index]->pBuff, AV_PIX_FMT_YUV420P,
				g_pVideoFrameBuff[open_index]->width, g_pVideoFrameBuff[open_index]->height) < 0){
			RepostWhizErr("catch_video_frame avpicture_fill 2 error");

			pthread_mutex_unlock(&g_mutexMedia);
			return 0;
		}

		RepostWhizLog("catch_video_frame sws_scale 1 linesize=%d", m_frame_scale->linesize);

		if(mediaCtx->codecCtx->width == g_pVideoFrameBuff[open_index]->width)
			mediaCtx->swsCtx = sws_getCachedContext(mediaCtx->swsCtx, mediaCtx->codecCtx->width, mediaCtx->codecCtx->height, mediaCtx->codecCtx->pix_fmt,
					g_pVideoFrameBuff[open_index]->width, g_pVideoFrameBuff[open_index]->height, AV_PIX_FMT_YUV420P, SWS_X , NULL, NULL, NULL);
		else
		mediaCtx->swsCtx = sws_getCachedContext(mediaCtx->swsCtx, mediaCtx->codecCtx->width, mediaCtx->codecCtx->height, mediaCtx->codecCtx->pix_fmt,
					g_pVideoFrameBuff[open_index]->width, g_pVideoFrameBuff[open_index]->height, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR , NULL, NULL, NULL);

		RepostWhizLog("catch_video_frame sws_scale 2, src(video_width=%d, video_height=%d), dst(video_width=%d, video_height=%d)",
				mediaCtx->codecCtx->width, mediaCtx->codecCtx->height, g_pVideoFrameBuff[open_index]->width, g_pVideoFrameBuff[open_index]->height);

		if ( sws_scale(mediaCtx->swsCtx, video->data, video->linesize, 0, g_pVideoFrameBuff[open_index]->height,
				m_frame_scale->data, m_frame_scale->linesize) < 0){
			RepostWhizErr("catch_video_frame sws_scale  error");

			if (video_must_free > 0)
				SAFE_AV_FREE(video);

			pthread_mutex_unlock(&g_mutexMedia);
			return 0;
		}

		pthread_mutex_unlock(&g_mutexMedia);

		if (video_must_free > 0)
			SAFE_AV_FREE(video);

		RepostWhizLog("catch_video_frame succeded ===================>>>>");

		return 1;
	}
	else
	{
		//	RepostWhizLog("catch_video_frame 1");
		//
		//	if ( open_index < 0 || open_index >= OPEN_LIMIT ){
		//		RepostWhizErr("catch_video_frame open_index  is null open_index=%d", open_index);
		//		return -1;
		//	}
		//
		//	MediaContext * mediaCtx;
		//	AVFrame* video = NULL;
		//	int res;
		//
		//	// get mediaContext
		//	pthread_mutex_lock(&g_mutexMedia);
		//	mediaCtx = g_mediaCtx[open_index];
		//	if ( mediaCtx == NULL )
		//	{
		//		RepostWhizErr("get_audio_frame mediaCtx is null.");
		//
		//		pthread_mutex_unlock(&g_mutexMedia);
		//		return -1;
		//	}
		//	pthread_mutex_unlock(&g_mutexMedia);

		// get video frame from Media queue.
		int video_must_free = 1;

		res = get_video_frame2(mediaCtx, sec, &video, &video_must_free);

		if (res < 0){
			RepostWhizErr("get_video_frame failed.");
			return -1;
		}

		pthread_mutex_lock(&g_mutexMedia);

		if ( avpicture_fill((AVPicture*)m_frame_scale, (uint8_t*)g_pVideoFrameBuff[open_index]->pBuff, AV_PIX_FMT_YUV420P,	mediaCtx->codecCtx->width, mediaCtx->codecCtx->height) < 0){
			RepostWhizErr("catch_video_frame avpicture_fill 2 error");

			pthread_mutex_unlock(&g_mutexMedia);
			return 0;
		}

		RepostWhizLog("catch_video_frame sws_scale 1 linesize=%d", m_frame_scale->linesize);

		mediaCtx->swsCtx = sws_getCachedContext(mediaCtx->swsCtx, mediaCtx->codecCtx->width, mediaCtx->codecCtx->height, mediaCtx->codecCtx->pix_fmt,
				mediaCtx->codecCtx->width, mediaCtx->codecCtx->height, AV_PIX_FMT_YUV420P, SWS_X , NULL, NULL, NULL);

		RepostWhizLog("catch_video_frame sws_scale 2, video_width=%d, video_height=%d", mediaCtx->codecCtx->width, mediaCtx->codecCtx->height);

		if ( sws_scale(mediaCtx->swsCtx, video->data, video->linesize, 0, mediaCtx->codecCtx->height, m_frame_scale->data, m_frame_scale->linesize) < 0){
			RepostWhizErr("catch_video_frame sws_scale  error");

			if (video_must_free > 0){
				SAFE_AVVIDEO_FREE(video);
			}

			pthread_mutex_unlock(&g_mutexMedia);
			return 0;
		}

		pthread_mutex_unlock(&g_mutexMedia);

		if (video_must_free > 0){
			SAFE_AVVIDEO_FREE(video);
		}

		RepostWhizLog("catch_video_frame succeded ");

		return 1;
	}
}

int seek_media( int open_index, double sec )
{
	MediaContext * mediaCtx;

	if ( open_index < 0 || open_index >= OPEN_LIMIT )
		return -1;

	pthread_mutex_lock(&g_mutexMedia);
	mediaCtx = g_mediaCtx[open_index];
	if ( mediaCtx == NULL )
	{
		pthread_mutex_unlock(&g_mutexMedia);
		return -1;
	}

	mediaCtx->ref_count ++;
	pthread_mutex_unlock(&g_mutexMedia);

	pthread_mutex_lock(&mediaCtx->mutex_seek);
	mediaCtx->seek_request = 1;

	mediaCtx->seek_pos = (int64_t) (sec * mediaCtx->time_base_rev);
	pthread_mutex_unlock(&mediaCtx->mutex_seek);

	RepostWhizLog("seek_media 1 seek_pos = %d", (int) mediaCtx->seek_pos);

	mediaCtx->ref_count --;

	return 0;
}

double get_total_time(int open_index)
{
	MediaContext * mediaCtx;

	if ( open_index < 0 || open_index >= OPEN_LIMIT )
		return -1;

	pthread_mutex_lock(&g_mutexMedia);
	mediaCtx = g_mediaCtx[open_index];
	if ( mediaCtx == NULL )
	{
		pthread_mutex_unlock(&g_mutexMedia);
		return -1;
	}

	pthread_mutex_unlock(&g_mutexMedia);

	RepostWhizLog("Get Total Time %f", mediaCtx->totalTime);

	return mediaCtx->totalTime;
}

int64_t get_time_from_byte(MediaContext * mediaCtx, int64_t bytes)
{
	return (int64_t) ((double) bytes * 1000.0 / (double) (2 * mediaCtx->channels * mediaCtx->sample_rate));	
}

int64_t get_byte_from_time( MediaContext * mediaCtx, int64_t pts )
{
	return 2 * mediaCtx->channels * (int64_t) ((double) pts * (double) mediaCtx->sample_rate / 1000.0);
}

////////////////////////////////////////////////////////////////////////////
//// Draw video frame
//
//    
//
////////////////////////////////////////////////////////////////////////////

void OnRenderFrame(char* pGLBuff)
{
	RepostWhizLog("NativeRenderFrame");

	if(!g_pVideoFrameBuff)
		return;

	pthread_mutex_lock(&g_mutexMedia);

	OnGLESRender(g_pVideoFrameBuff, g_pImageBuff);

	pthread_mutex_unlock(&g_mutexMedia);

	RepostWhizLog("NativeRenderFrame buff=%p, width=%d, height=%d", g_pGLTextureBuff, g_nTextureWidth, g_nTextureHeight);

	if(!g_pGLTextureBuff)
		return;

	int bpp = GetGLBpp();
	GetGLColorBuffer(g_pGLTextureBuff, g_nTextureWidth, g_nTextureHeight);

	RepostWhizLog("NativeRenderFrame, step 6");

	pthread_mutex_lock(m_hMutexExport);

	if (g_pExportBuff)
	{
		RepostWhizLog("NativeRenderFrame, step 7 Copy Export Frame");

		if (bpp == 2) {
			switch (g_nExportColorFormat) {
			case COLOR_FormatYUV420Planar:
				bgr565_to_yuv420(g_pGLTextureBuff, g_pExportBuff, g_nTextureWidth, g_nTextureHeight);
				break;
			case COLOR_FormatYUV420SemiPlanar:
				if ((g_nExportColorExtension == YUV420SemiPlanar_BGR) || (g_nExportColorExtension == YUV420SemiPlanar_HTC))
					bgr565_to_yuv420sp(g_pGLTextureBuff, g_pExportBuff, g_nTextureWidth, g_nTextureHeight);
				else
					rgb565_to_yuv420sp(g_pGLTextureBuff, g_pExportBuff, g_nTextureWidth, g_nTextureHeight);
				break;
			default:
				rgb565_to_yuv420sp(g_pGLTextureBuff, g_pExportBuff, g_nTextureWidth, g_nTextureHeight);
				break;
			}
		} else {
			if (g_nExportColorFormat == COLOR_FormatYUV420Planar)
				rgb8888_to_yuv420(g_pGLTextureBuff, g_pExportBuff, g_nTextureWidth, g_nTextureHeight);
			else if (g_nExportColorFormat == COLOR_TI_FormatYUV420PackedSemiPlanar)
				rgb8888_to_yuv420sp(g_pGLTextureBuff, g_pExportBuff, g_nTextureWidth, g_nTextureHeight);
			else
				bgr8888_to_yuv420sp(g_pGLTextureBuff, g_pExportBuff, g_nTextureWidth, g_nTextureHeight);
		}
	}

	pthread_mutex_unlock(m_hMutexExport);

	RepostWhizLog("NativeRenderFrame, step 6.4, g_nExportColorFormat=%d", g_nExportColorFormat);

	g_nExportTic ++;

	RepostWhizLog("NativeRenderFrame End");
}

// get screen buffer //use only java encode
void CopyScreenBuff(char* pBuff, int size)
{
	if (g_pExportBuff){

		pthread_mutex_lock(m_hMutexExport);
		memcpy(pBuff, g_pExportBuff, size);
		pthread_mutex_unlock(m_hMutexExport);
	}
}

void SetMaskImage(unsigned char *pixelBuffer, int width, int height)
{
	renderMaskImage(pixelBuffer, width, height);
	//__android_log_print(ANDROID_LOG_DEBUG, "test", "SetMaskImage width=%d, height=%d", width, height);
}

void SetGLScreenSize()
{
	if ((g_nTextureWidth == 0) || (g_nTextureHeight == 0))
	{
		g_nTextureWidth = 640;
		g_nTextureHeight = 640;
		glViewport(0, 0, g_nTextureWidth, g_nTextureHeight);

		CreateFrameTexture(g_nTextureWidth, g_nTextureHeight);
	}
}

void InitRenderFrame()
{
	g_nTextureWidth = g_nTextureHeight = 0;
	InitShader();
}

void FiniteRenderFrame()
{
	ExitShader();
}
