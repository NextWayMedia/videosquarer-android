#include <jni.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <android/log.h>
#include <android/bitmap.h>

#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

typedef struct {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
	uint8_t alpha;
} rgba;

void gen_lookup_table(float* cmatrix, int cmatrix_length, float* lookup_table)
{
	int lookup_table_n = 0;
	int cmatrix_n = 0;
	int i, j;

	for (i=0; i<cmatrix_length; i++)
	{
		for (j=0; j<256; j++)
		{
			lookup_table[lookup_table_n] = cmatrix[cmatrix_n] * (float)j;
			lookup_table_n ++;
		}
		cmatrix_n ++;
	}
}

void blur_line( float* ctable, float* cmatrix, int cmatrix_length, uint8_t* cur_col, uint8_t* dest_col, int y, int bytes )
{
	float scale;
	float sum;
	int i=0, j=0;
	int row;
	int cmatrix_middle = cmatrix_length/2;
	int dest_col_n;
	int cur_col_n;
	int cur_col_n1;
	int ctable_n;
	/* this first block is the same as the non-optimized version --
	 * it is only used for very small pictures, so speed isn't a
	 * big concern.
	 */

	if (cmatrix_length > y)
	{
		for (row = 0; row < y ; row++)
		{
			scale=0;
			/* find the scale factor */
			for (j = 0; j < y ; j++)
			{
				/* if the index is in bounds, add it to the scale counter */
				if ((j + cmatrix_middle - row >= 0) &&
						(j + cmatrix_middle - row < cmatrix_length))
					scale += cmatrix[j + cmatrix_middle - row];
			}
			for (i = 0; i<bytes; i++)
			{
				sum = 0;
				for (j = 0; j < y; j++)
				{
					if ((j >= row - cmatrix_middle) &&
							(j <= row + cmatrix_middle))
						sum += ((char)cur_col[j*bytes + i] & 0xFF) * cmatrix[j];
				}
				dest_col[row*bytes + i] = (uint8_t)(0.5f + sum / scale);
			}
		}
	}
	else
	{
		/* for the edge condition, we only use available info and scale to one */
		for (row = 0; row < cmatrix_middle; row++)
		{
			/* find scale factor */
			scale=0;
			for (j = cmatrix_middle - row; j<cmatrix_length; j++)
				scale += cmatrix[j];
			for (i = 0; i<bytes; i++)
			{
				sum = 0;
				for (j = cmatrix_middle - row; j<cmatrix_length; j++)
				{
					sum += ((char)cur_col[(row + j-cmatrix_middle)*bytes + i] & 0xFF) * cmatrix[j];
				}
				dest_col[row*bytes + i] = (uint8_t)(0.5f + sum / scale);
			}
		}
		/* go through each pixel in each col */

		dest_col_n = row * bytes;

		for (; row < y - cmatrix_middle; row++)
		{
			cur_col_n = (row - cmatrix_middle) * bytes;
			for (i = 0; i < bytes; i++)
			{
				sum = 0;
				cur_col_n1 = cur_col_n;
				ctable_n = 0;
				for (j = cmatrix_length; j>0; j--)
				{
					sum += ctable[ctable_n + ((char)cur_col[cur_col_n1] & 0xFF)];

					cur_col_n1 += bytes;
					ctable_n += 256;
				}
				cur_col_n ++;
				dest_col[dest_col_n] = (uint8_t)(0.5f + sum);
				dest_col_n ++;
			}
		}

		/* for the edge condition , we only use available info, and scale to one */
		for (; row < y; row++)
		{
			/* find scale factor */
			scale=0;
			for (j = 0; j< y-row + cmatrix_middle; j++)
				scale += cmatrix[j];
			for (i = 0; i<bytes; i++)
			{
				sum = 0;
				for (j = 0; j<y-row + cmatrix_middle; j++)
				{
					sum += ((char)cur_col[(row + j-cmatrix_middle)*bytes + i] & 0xFF) * cmatrix[j];
				}
				dest_col[row*bytes + i] = (uint8_t) (0.5f + sum / scale);
			}
		}
	}
}

int min(int a, int b) {
	return a > b ? b : a;
}

int max(int a, int b) {
	return a > b ? a : b;
}

JNIEXPORT int JNICALL Java_com_videosquarer_squareimage_gui_DisplayingActivity_nativeFastBlur( JNIEnv* env, jobject thiz,
		jobject pbBmpDest, int w, int h, int radius)
{
	void* pBuff;

	jobject pBmpDestRef = BitmapLock(env, thiz, pbBmpDest, &pBuff);

	if (pBmpDestRef == NULL)
		return -1;

	int* pix = (int*)pBuff;

	int wm = w - 1;
	int hm = h - 1;
	int wh = w * h;
	int div = radius + radius + 1;

	int ms = wh * sizeof(int);
	int* r = (int*)malloc(ms);
	int* g = (int*)malloc(ms);
	int* b = (int*)malloc(ms);

	int rsum, gsum, bsum, x, y, i, j, p, yp, yi, yw;
	ms = max(w, h) * sizeof(int);

	int* vmin = (int*)malloc(ms);

	int divsum = (div + 1) >> 1;
	divsum *= divsum;

	//ms = divsum  256  sizeof(int);
	ms = divsum << 10;

	int* dv = (int*)malloc(ms);

	for (i = 0; i < 256 * divsum; i++) {
		dv[i] = (i / divsum);
	}

	yw = yi = 0;

	// step1.
	int **stack = (int**)malloc(sizeof(int) * div);
	for(i=0; i<div; i++){

		stack[i] = (int*)malloc(sizeof(int) * 3);
	}

	int stackpointer;
	int stackstart;
	int* sir;
	int rbs;
	int r1 = radius + 1;
	int routsum, goutsum, boutsum;
	int rinsum, ginsum, binsum;

	// step2.
	for (y = 0; y < h; y++)
	{
		rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;

		for (i = -radius; i <= radius; i++)
		{
			p = pix[yi + min(wm, max(i, 0))];
			sir = stack[i + radius];
			sir[0] = (p & 0xff0000) >> 16;
			sir[1] = (p & 0x00ff00) >> 8;
			sir[2] = (p & 0x0000ff);
			rbs = r1 - abs(i);
			rsum += sir[0] * rbs;
			gsum += sir[1] * rbs;
			bsum += sir[2] * rbs;

			if (i > 0) {
				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];
			} else {
				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];
			}
		}

		stackpointer = radius;

		for (x = 0; x < w; x++)
		{
			r[yi] = dv[rsum];
			g[yi] = dv[gsum];
			b[yi] = dv[bsum];

			rsum -= routsum;
			gsum -= goutsum;
			bsum -= boutsum;

			stackstart = stackpointer - radius + div;
			sir = stack[stackstart % div];

			routsum -= sir[0];
			goutsum -= sir[1];
			boutsum -= sir[2];

			if (y == 0) {
				vmin[x] = min(x + radius + 1, wm);
			}
			p = pix[yw + vmin[x]];

			sir[0] = (p & 0xff0000) >> 16;
			sir[1] = (p & 0x00ff00) >> 8;
			sir[2] = (p & 0x0000ff);

			rinsum += sir[0];
			ginsum += sir[1];
			binsum += sir[2];

			rsum += rinsum;
			gsum += ginsum;
			bsum += binsum;

			stackpointer = (stackpointer + 1) % div;
			sir = stack[(stackpointer) % div];

			routsum += sir[0];
			goutsum += sir[1];
			boutsum += sir[2];

			rinsum -= sir[0];
			ginsum -= sir[1];
			binsum -= sir[2];

			yi++;
		}
		yw += w;
	}

	// step3.
	for (x = 0; x < w; x++)
	{
		rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
		yp = -radius * w;

		for (i = -radius; i <= radius; i++)
		{
			yi = max(0, yp) + x;

			sir = stack[i + radius];

			sir[0] = r[yi];
			sir[1] = g[yi];
			sir[2] = b[yi];

			rbs = r1 - abs(i);

			rsum += r[yi] * rbs;
			gsum += g[yi] * rbs;
			bsum += b[yi] * rbs;

			if (i > 0) {
				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];
			} else {
				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];
			}

			if (i < hm) {
				yp += w;
			}
		}

		yi = x;
		stackpointer = radius;

		for (y = 0; y < h; y++)
		{
			pix[yi] = 0xff000000 | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

			rsum -= routsum;
			gsum -= goutsum;
			bsum -= boutsum;

			stackstart = stackpointer - radius + div;
			sir = stack[stackstart % div];

			routsum -= sir[0];
			goutsum -= sir[1];
			boutsum -= sir[2];

			if (x == 0) {
				vmin[y] = min(y + r1, hm) * w;
			}
			p = x + vmin[y];

			sir[0] = r[p];
			sir[1] = g[p];
			sir[2] = b[p];

			rinsum += sir[0];
			ginsum += sir[1];
			binsum += sir[2];

			rsum += rinsum;
			gsum += ginsum;
			bsum += binsum;

			stackpointer = (stackpointer + 1) % div;
			sir = stack[stackpointer];

			routsum += sir[0];
			goutsum += sir[1];
			boutsum += sir[2];

			rinsum -= sir[0];
			ginsum -= sir[1];
			binsum -= sir[2];

			yi += w;
		}
	}

	for(i=0; i<div; i++){
		free(stack[i]);
	}
	free(stack);

	free(dv);
	free(vmin);

	free(r);
	free(g);
	//free(beer);

	BitmapUnlock(env, thiz, pBmpDestRef, pBuff);

	return 0;
}

int gen_convolve_matrix( float radius, float* cmatrix, int matrix_length )
{
	int i,j;
	float std_dev;
	float sum;

	/* we want to generate a matrix that goes out a certain radius
	 * from the center, so we have to go out ceil(rad-0.5) pixels,
	 * inlcuding the center pixel.  Of course, that's only in one direction,
	 * so we have to go the same amount in the other direction, but not count
	 * the center pixel again.  So we double the previous result and subtract
	 * one.
	 * The radius parameter that is passed to this function is used as
	 * the standard deviation, and the radius of effect is the
	 * standard deviation * 2.  It's a little confusing.
	 * <DP> modified scaling, so that matrix_lenght = 1+2*radius parameter
	 */

	radius = (float)(0.5*(radius > 0 ? radius : -radius)) + 0.25f;

	std_dev = radius;
	radius = std_dev * 2;

	/*  Now we fill the matrix by doing a numeric integration approximation
	 * from -2*std_dev to 2*std_dev, sampling 50 points per pixel.
	 * We do the bottom half, mirror it to the top half, then compute the
	 * center point.  Otherwise asymmetric quantization errors will occur.
	 *  The formula to integrate is e^-(x^2/2s^2).
	 */

	/* first we do the top (right) half of matrix */
	for (i = matrix_length/2 + 1; i < matrix_length; i++)
	{
		float base_x = i - (float)floor((float)(matrix_length/2)) - 0.5f;
		sum = 0;
		for (j = 1; j <= 50; j++)
		{
			if ( base_x+0.02*j <= radius )
				sum += (float)exp (-(base_x+0.02*j)*(base_x+0.02*j) /
						(2*std_dev*std_dev));
		}
		cmatrix[i] = sum/50;
	}

	/* mirror the thing to the bottom half */
	for (i=0; i<=matrix_length/2; i++) {
		cmatrix[i] = cmatrix[matrix_length-1-i];
	}

	/* find center val -- calculate an odd number of quanta to make it symmetric,
	 * even if the center point is weighted slightly higher than others. */
	sum = 0;
	for (j=0; j<=50; j++)
	{
		sum += (float)exp (-(0.5+0.02*j)*(0.5+0.02*j) /
				(2*std_dev*std_dev));
	}
	cmatrix[matrix_length/2] = sum/51;

	/* normalize the distribution by scaling the total sum to one */
	sum=0;
	for (i=0; i<matrix_length; i++) sum += cmatrix[i];
	for (i=0; i<matrix_length; i++) cmatrix[i] = cmatrix[i] / sum;

	return matrix_length;
}
