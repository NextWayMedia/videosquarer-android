package com.videosquarer.ffmpeg;

import java.util.ArrayList;
import java.util.List;

public class FrameJob {
    public static ProcessBuilder getFrameTask(String ffmpeg, String inFile, String outFile) {
        List<String> cmd = new ArrayList<String>();
        cmd.add(ffmpeg);
        cmd.add("-i");
        cmd.add(inFile);
        cmd.add("-vf");
        cmd.add("thumbnail=25, scale=iw/4:ih/4");
        cmd.add("-frames:v");
        cmd.add("1");
        cmd.add("-y");
        cmd.add(outFile);
        return new ProcessBuilder(cmd);
    }
}
