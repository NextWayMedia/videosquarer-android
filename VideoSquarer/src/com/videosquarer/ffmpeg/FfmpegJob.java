package com.videosquarer.ffmpeg;
import java.util.LinkedList;
import java.util.List;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;

@SuppressLint("NewApi")
public class FfmpegJob 
{
	public String originalVideoFilePath;
	public String processedVideoFilePath;
	public int squareSize;
	public String borderColor;
	public int nTranspose;
	public int nBorderSize;
	
	public String ffmpegPath;
	public String watermarkFilePath;
	
	public FfmpegJob(String ffmpegPath, String watermarkFilePath) 
	{
		this.ffmpegPath = ffmpegPath;
		this.watermarkFilePath = watermarkFilePath;
		
		nTranspose = 0;
		nBorderSize = 100;
	}

    public ProcessRunnable PreLoadVideo()
	{
		List<String> cmd = new LinkedList<String>();
		
		/*		
		String filterComplexOption = "color=c=black,scale=" + 
				Integer.toString(squareSize) + 
				":" + 
				Integer.toString(squareSize) + 
				"[bg];[0:v]scale=" + 
				Integer.toString(squareSize) +
				":-1 [ov]; [bg][ov]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2";
 		*/		 
				
		/*
		ffmpeg -y -i in.mp4 -acodec copy -qscale 10 -flags +global_header -strict experimental -shortest 
		-vf "color=c=red,scale=450:450[bg];[0:v]scale=450:-1 [ov]; [bg][ov]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2" -map 0 /sdcard/ipictorial/ooo.mp4
					
		ffmpeg -y -i in.mp4 -acodec copy -q:v 250 -strict experimental -shortest 
		-vf "[0:v]scale=450:-1,pad=max(iw\,ih):ow:(ow-iw)/2:(oh-ih)/2:red" 
		-map 0 -b:v 100k -g 0 -global_quality:v "0*QP2LAMBDA" -r 2 -threads 4 out.mp4
		*/
		
		String filterComplexOption = "[0:v]scale=w='if(gte(iw,ih)," + 
				Integer.toString(squareSize) + ",-1)':h='if(gte(iw,ih),-1," + 
				Integer.toString(squareSize) + ")'" + 
				",pad=max(iw\\,ih):ow:(ow-iw)/2:(oh-ih)/2:black";
		
		cmd.add(ffmpegPath);
		cmd.add("-loglevel");
		cmd.add("quiet");
		cmd.add("-y");
		cmd.add("-i");
		cmd.add(originalVideoFilePath);
		cmd.add("-acodec");
		cmd.add("copy");
		cmd.add("-q:v");
		cmd.add("250");
		cmd.add("-strict");
		cmd.add("experimental");
		cmd.add("-shortest");
		cmd.add("-vf");
		cmd.add(filterComplexOption);
		cmd.add("-map");
		cmd.add("0");
		cmd.add("-b:v");
		cmd.add("100k");
		cmd.add("-g");
		cmd.add("0");
		cmd.add("-global_quality:v");
		cmd.add("0*QP2LAMBDA");
		cmd.add("-r");
		cmd.add("2");
		cmd.add("-threads");
		cmd.add("4");
		cmd.add(processedVideoFilePath);
		
		final ProcessBuilder pb = new ProcessBuilder(cmd);
		return new ProcessRunnable(pb);					
	}
	
	
	public ProcessRunnable setBordersColor()
	{
		/*		
		ffmpeg -y -i in.mp4 -acodec copy -q:v 250 -strict experimental -shortest 
		-vf "[0:v]scale=450:-1,pad=max(iw\,ih):ow:(ow-iw)/2:(oh-ih)/2:0xSelected Color" 
		-map 0 -b:v 100k -g 0 -global_quality:v "0*QP2LAMBDA" -r 2 -threads 4 out.mp4
		 */
		
		List<String> cmd = new LinkedList<String>();
		int nSquareSize = squareSize - 100 + nBorderSize;
		String filterComplexOption = "[0:v]scale=w='if(gte(iw,ih)," + 
				Integer.toString(nSquareSize) + ",-1)':h='if(gte(iw,ih),-1," + 
				Integer.toString(nSquareSize) + ")'" + 
				",pad=" + Integer.toString(squareSize) + ":" + Integer.toString(squareSize) + ":(ow-iw)/2:(oh-ih)/2:0x" + borderColor;
		
		int nTransCount;
		String strTrans = "";
		for(nTransCount = 0 ; nTransCount < nTranspose ; nTransCount ++)
		{
			strTrans = strTrans + ",transpose=1";
		}
		
		filterComplexOption = filterComplexOption + strTrans;
		
		cmd.add(ffmpegPath);
		cmd.add("-loglevel");
		cmd.add("quiet");
		cmd.add("-y");
		cmd.add("-threads");
		cmd.add("4");
		cmd.add("-i");
		cmd.add(originalVideoFilePath);
		cmd.add("-acodec");
		cmd.add("copy");
		cmd.add("-q:v");
		cmd.add("250");
		cmd.add("-strict");
		cmd.add("experimental");
		cmd.add("-shortest");
		cmd.add("-vf");
		cmd.add(filterComplexOption);
		cmd.add("-map");
		cmd.add("0");
		cmd.add("-b:v");
		cmd.add("100k");
		cmd.add("-g");
		cmd.add("0");
		cmd.add("-global_quality:v");
		cmd.add("0*QP2LAMBDA");
		cmd.add("-r");
		cmd.add("2");
		cmd.add(processedVideoFilePath);
		
		final ProcessBuilder pb = new ProcessBuilder(cmd);
		return new ProcessRunnable(pb);					
	}
	
	public ProcessRunnable rotateVideo()
	{
		List<String> cmd = new LinkedList<String>();
		int nSquareSize = squareSize - 100 + nBorderSize;
		String filterComplexOption = "[0:v]scale=w='if(gte(iw,ih)," + 
				Integer.toString(nSquareSize) + ",-1)':h='if(gte(iw,ih),-1," + 
				Integer.toString(nSquareSize) + ")'" + 
				",pad=" + Integer.toString(squareSize) + ":" + Integer.toString(squareSize) + ":(ow-iw)/2:(oh-ih)/2:0x" + borderColor;
		int nTransCount;
		String strTrans = "";
		for(nTransCount = 0 ; nTransCount < nTranspose ; nTransCount ++)
		{
			strTrans = strTrans + ",transpose=1";
		}
		
		filterComplexOption = filterComplexOption + strTrans;
		
		cmd.add(ffmpegPath);
		cmd.add("-loglevel");
		cmd.add("quiet");
		cmd.add("-y");
		cmd.add("-threads");
		cmd.add("4");
		cmd.add("-i");
		cmd.add(originalVideoFilePath);
		cmd.add("-acodec");
		cmd.add("copy");
		cmd.add("-q:v");
		cmd.add("250");
		cmd.add("-strict");
		cmd.add("experimental");
		cmd.add("-shortest");
		cmd.add("-vf");
		cmd.add(filterComplexOption);
		cmd.add("-map");
		cmd.add("0");
		cmd.add("-b:v");
		cmd.add("100k");
		cmd.add("-g");
		cmd.add("0");
		cmd.add("-global_quality:v");
		cmd.add("0*QP2LAMBDA");
		cmd.add("-r");
		cmd.add("2");
		cmd.add(processedVideoFilePath);
		
		final ProcessBuilder pb = new ProcessBuilder(cmd);
		return new ProcessRunnable(pb);						
	}


	
	public ProcessBuilder RenderingVideo()
	{
		List<String> cmd = new LinkedList<String>();
//		int nSquareSize = squareSize - 100 + nBorderSize;
		int nSquareSize = nBorderSize;
		String filterComplexOption = "[0:v]scale=w='if(gte(iw,ih)," +
				Integer.toString(nSquareSize) + ",-1)':h='if(gte(iw,ih),-1," + 
				Integer.toString(nSquareSize) + ")'" + 
				",pad=" + Integer.toString(squareSize) + ":" + Integer.toString(squareSize) + ":(ow-iw)/2:(oh-ih)/2:0x" + borderColor;
		
		// watermark

		
		int nTransCount;
		String strTrans = "";
		for(nTransCount = 0 ; nTransCount < nTranspose ; nTransCount++)
		{
			strTrans = strTrans + ",transpose=1";
		}
		
		filterComplexOption = filterComplexOption + strTrans;

		cmd.add(ffmpegPath);
//		cmd.add("-loglevel");
//		cmd.add("quiet");
		cmd.add("-y");
//		cmd.add("-threads");
//		cmd.add("4");
		cmd.add("-i");
		cmd.add(originalVideoFilePath);
		cmd.add("-acodec");
		cmd.add("copy");
		cmd.add("-q:v");
		cmd.add("4");
		cmd.add("-strict");
		cmd.add("experimental");
		cmd.add("-shortest");
		cmd.add("-vf");
		cmd.add(filterComplexOption);
//		cmd.add("-vf");
//		cmd.add(watermarkOption);
		cmd.add("-map");
		cmd.add("0");
		cmd.add(processedVideoFilePath);
//        Log.d(FfmpegJob.class.getSimpleName(), "cmd: " + TextUtils.join(" ", cmd));
		/*final*/ ProcessBuilder pb = new ProcessBuilder(cmd);		
//		return new ProcessRunnable(pb);
		return pb;
	}
}

