package com.videosquarer.ffmpeg;

import java.util.LinkedList;
import java.util.List;

public class WatermarkJob {
    private String inPath;
    private String outPath;
    private int watermarkX, watermarkY;
    private String watermarkPath;
    private String ffmpegPath;

    public void setFfmpegPath(String ffmpegPath) {
        this.ffmpegPath = ffmpegPath;
    }

    public void setWatermarkPath(String watermarkPath) {
        this.watermarkPath = watermarkPath;
    }

    public void setInPath(String inPath) {
        this.inPath = inPath;
    }

    public void setOutPath(String outPath) {
        this.outPath = outPath;
    }

    public void setWatermarkX(int watermarkX) {
        this.watermarkX = watermarkX;
    }

    public void setWatermarkY(int watermarkY) {
        this.watermarkY = watermarkY;
    }

    public ProcessBuilder getWatermarkTask() {
        List<String> cmd = new LinkedList<String>();
        String watermarkOption =  "movie=" + watermarkPath + " [watermark];[in] [watermark] overlay=" + watermarkY + ":" + watermarkX + " [out]";
        cmd.add(ffmpegPath);
        cmd.add("-y");
        cmd.add("-i");
        cmd.add(inPath);
        cmd.add("-acodec");
        cmd.add("copy");
        cmd.add("-q:v");
        cmd.add("4");
        cmd.add("-strict");
        cmd.add("experimental");
        cmd.add("-shortest");
        cmd.add("-vf");
        cmd.add(watermarkOption);
        cmd.add(outPath);
        return new ProcessBuilder(cmd);
    }

    public int getWatermarkX() {
        return watermarkX;
    }

    public int getWatermarkY() {
        return watermarkY;
    }

    public String getWatermarkPath() {
        return watermarkPath;
    }
}
