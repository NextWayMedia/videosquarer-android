package com.videosquarer.squareimage.util;

public class MovieWriterParam {

	public String strFileFormat;
	public String	strFileName;
	public long nFileFormat;
	
	public AudioParamStruct AudioParam = new AudioParamStruct();
	public VideoParamStruct VideoParam = new VideoParamStruct();

	public class AudioParamStruct
	{
		public WAVEFORMATEX		wfx;
		public int				bitrate;
	};

	public class VideoParamStruct  
	{
		public VIDEOINFOHEADER	vih;
		public int				keyframe;
	};
}
