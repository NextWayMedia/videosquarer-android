package com.videosquarer.squareimage.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class BITMAPFILEHEADER {

	public	short    bfType;
	public	int		 bfSize;
	public	short    bfReserved1;
	public	short    bfReserved2;
	public	int	     bfOffBits;

	public static int size()
    {
    	return 2 + 4 + 2 + 2 + 4;
    }
	
	public byte[] getBytes()
	{
		ByteBuffer buf = ByteBuffer.allocate(size());
		buf.order(ByteOrder.LITTLE_ENDIAN);
		
		return buf.putShort(bfType).putInt(bfSize).putShort(bfReserved1).putShort(bfReserved1).putInt(bfOffBits).array();
	}
}
