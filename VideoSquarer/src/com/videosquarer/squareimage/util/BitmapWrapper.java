package com.videosquarer.squareimage.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

public class BitmapWrapper {
	public final static int PHOTO_FRAME_MAX_SIZE = 1280; 

	public static Bitmap createBitmap(Bitmap src) {
		return checkNew(Bitmap.createBitmap(src), src);
	}

	public static Bitmap createBitmap(Bitmap source, int x, int y, int width, int height) {
		return checkNew(Bitmap.createBitmap(source, x, y, width, height), source);
	}

	public static Bitmap createBitmap(Bitmap source, int x, int y, int width, int height, Matrix m, boolean filter) {
		return checkNew(Bitmap.createBitmap(source, x, y, width, height, m, filter), source);
	}

	public static Bitmap createScaledBitmap(Bitmap src, int dstWidth, int dstHeight, boolean filter) {
		return checkNew(Bitmap.createScaledBitmap(src, dstWidth, dstHeight, filter), src);
	}

	private static Bitmap checkNew(Bitmap bitmap, Bitmap src) {
		return (bitmap != src) ? bitmap : src.copy(src.getConfig(), true);
	}

	public static Bitmap createBitmap(int width, int height, Config config) {
		return Bitmap.createBitmap(width, height, config);
	}

	public static Bitmap createBitmap(int colors[], int width, int height, Config config) {
		return Bitmap.createBitmap(colors, width, height, config);
	}

	public static Bitmap decodeResource(Resources res, int id) {
		return BitmapFactory.decodeResource(res, id);
	}

	public static void recycleBitmap(Bitmap bmp) {
		if (bmp != null) {
			if (!bmp.isRecycled())
				bmp.recycle();
		}
		bmp = null;
	}

	public static Bitmap bitmapFromBuffer(byte[] pBuffer, int dwBufSize, int nSampleSize) {
		Bitmap pResult = null;

		if (pBuffer == null)
			return pResult;

		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inSampleSize = nSampleSize;

		pResult = BitmapFactory.decodeByteArray(pBuffer, 0, dwBufSize);
		opt = null;

		return pResult;
	}

	public static Bitmap BitmapFromSize(int nWidth, int nHeight, Config config) {
		return createBitmap(nWidth, nHeight, Bitmap.Config.ARGB_8888);
	}

	public static Bitmap BitmapFromFile(String strFileName) {
		return BitmapFromFile(strFileName, 1);
	}

	public static Bitmap BitmapFromFile(String strFileName, int nSampleSize) {
		Bitmap pResult = null;
		boolean blAsset = false;

		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = nSampleSize;
		opts.inPurgeable = true;

			pResult = BitmapFromFileEx(strFileName, PHOTO_FRAME_MAX_SIZE, 0);

		return pResult;
	}

	public static Bitmap BitmapFromFileEx(String strFileName, int width, int height) {

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(strFileName, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		int scaleFactor = 1;
		if(photoW > width || photoH > height)
		{
			if(width == 0 || height == 0)
			{
				width = Math.max(width, height);
				photoW = Math.max(photoW, photoH);
				if(photoW > width)
					scaleFactor = photoW / width;
			}
			else
			{
				// Determine how much to scale down the image
				scaleFactor = Math.min(photoW/width, photoH/height);
			}
		}
		scaleFactor = ((scaleFactor >> 1) << 1); //make even value
		if(scaleFactor < 1)
			scaleFactor = 1;

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;
		bmOptions.inPreferQualityOverSpeed = true;

		Bitmap bitmap = BitmapFactory.decodeFile(strFileName, bmOptions);

		return bitmap;
	}

	public static Bitmap LoadBigBitmap(String strFileName, int maxW) {
		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(strFileName, bmOptions);

		int w = bmOptions.outWidth;
		int h = bmOptions.outHeight;

		// if bitmap small than limit
		if (w < maxW && h < maxW){
			bmOptions.inJustDecodeBounds = false;
			bmOptions.inSampleSize = 1;
			bmOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;

			return BitmapFactory.decodeFile(strFileName, bmOptions);
		}

		float wscale = w / maxW;
		float hscale = h / maxW;
		int scale = (int)((wscale > hscale) ? wscale : hscale);

		if(scale < 1){
			scale = 1;
		}else{
			scale = (scale + 1) >> 1 << 1; // make power of 2.
		}

		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scale;
		bmOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;

		return BitmapFactory.decodeFile(strFileName, bmOptions);
	}

	public static void setBitmapData(Bitmap src, Bitmap dest) {
		Canvas canvas = new Canvas(dest);
		if ((src!=null) && (dest!=null))
			canvas.drawBitmap(src, new Rect(0, 0, src.getWidth(), src.getHeight()), new Rect(0, 0, dest.getWidth(), dest.getHeight()), null);
	}

	public static void saveBitmapToSdcard(Bitmap bitmap, String fileName) {
		try {
			File bitmapFile = new File(fileName);
			FileOutputStream bitmapWtriter = new FileOutputStream(bitmapFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bitmapWtriter);
		} catch (FileNotFoundException e) {
		}
	}

}
