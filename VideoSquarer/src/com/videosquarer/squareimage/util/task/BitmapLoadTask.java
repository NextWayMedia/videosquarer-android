package com.videosquarer.squareimage.util.task;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.util.Log;

import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.ImageUtil;

public class BitmapLoadTask extends AsyncTask<Void, Void, Bitmap> {

	private static final String TAG = "BitmapLoadTask";

	private DisplayingActivity activity;
	private int widthPixel;
	private Dialog progressDialog;

	public BitmapLoadTask(DisplayingActivity activity, int widthPixel) {
		this.activity = activity;
		this.widthPixel = widthPixel;
		progressDialog = AppUtil.getProgressDialog(activity,
				AppUtil.getStringRes(R.string.loading_please_wait));
	}
		
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog.show();
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		Bitmap image = BitmapFactory.decodeFile(
				activity.getOriginalImageFilePath(), options);

		options.inSampleSize = AppUtil.calculateInSampleSize(
				options.outWidth, options.outHeight, widthPixel, widthPixel);
		activity.setImageInSampleSize(options.inSampleSize);

		options.inJustDecodeBounds = false;
		image = BitmapFactory.decodeFile(activity.getOriginalImageFilePath(), options);
		if (image == null) {
			return null;
		}

		Matrix rotatingMatrix = ImageUtil.getRotatingMatrixFromExif(
				activity.getOriginalImageFilePath());
		Bitmap newImage = AppUtil.getBitmapWithoutMemoryError(image, rotatingMatrix);
		if (newImage != image) {
			image.recycle();
			System.gc();
		}

		return newImage;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		try {
			progressDialog.dismiss();
		} catch (Exception e) {
			Log.d(TAG, "Dialog exception");
		}

		if (result == null) {
			AppUtil.showLongToast(R.string.cant_load_image);
			activity.finish();
			return;
		}

		activity.getSquareImageView().setImageBitmap(result);
	}
}