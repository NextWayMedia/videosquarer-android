package com.videosquarer.squareimage.util.task;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.AsyncTask;

import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.gui.element.SquareImageView;
import com.videosquarer.squareimage.util.AppUtil;

public class RotatingTask extends AsyncTask<Void, Void, Bitmap> {

	private SquareImageView imageView;
	private Bitmap originalBitmap;

	public RotatingTask(SquareImageView imageView, Bitmap originalBitmap) {
		this.imageView = imageView;
		this.originalBitmap = originalBitmap;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		imageView.setIsRotatingInProgress(true);
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		Matrix matrix = new Matrix();
		matrix.postRotate(90);
		Bitmap resizedBitmap =
				AppUtil.getBitmapWithoutMemoryError(originalBitmap, matrix);

		if(DisplayingActivity.isBlur && (DisplayingActivity.blurBitmap != null))
			DisplayingActivity.blurBitmap = AppUtil.getBitmapWithoutMemoryError(DisplayingActivity.blurBitmap, matrix);
		
		return resizedBitmap;
	};

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);

		originalBitmap.recycle();
		System.gc();

		imageView.setImageBitmap(result);
		imageView.setIsRotatingInProgress(false);
	}
}