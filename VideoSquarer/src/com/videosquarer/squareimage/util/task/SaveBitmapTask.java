package com.videosquarer.squareimage.util.task;

import java.io.File;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.ImageUtil;

public class SaveBitmapTask extends AsyncTask<Bitmap, Void, Boolean> {

	private static final String TAG = "SaveBitmapTask";

	private DisplayingActivity activity;
	private Dialog progressDialog;
	private Intent intent;

	public SaveBitmapTask(DisplayingActivity activity, Intent intent) {
		this.activity = activity;
		this.intent = intent;
		progressDialog = AppUtil.getProgressDialog(activity,
				AppUtil.getStringRes(R.string.saving_please_wait));
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog.show();
	}

	@Override
	protected Boolean doInBackground(Bitmap... params) {
		Bitmap resultedBitmap = params[0];
		boolean isSaved = false;
		if (AppUtil.isMediaStorageMounted()) {
			// isSaved = AppUtil.savePicture(null, activity.getFileName(), resultedBitmap);
			isSaved = AppUtil.savePicture(activity.getProcessedFolderPath(), activity.getFileName(), resultedBitmap);
		}

		resultedBitmap.recycle();
		System.gc();

		return isSaved;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		try {
			progressDialog.dismiss();
		} catch (Exception e) {
			Log.d(TAG, "Dialog exception");
		}

		if (result) {
			if (intent != null) {
				activity.startActivity(intent);
			} else {
				AppUtil.showLongToast(AppUtil.getStringRes(R.string.saved_to) +
						activity.getProcessedImageFilePath());
			}
			ImageUtil.scanFile(activity, activity.getProcessedImageFilePath(), null);
		} else {
			AppUtil.showLongToast(R.string.cant_save_image);
		}

		String fileName = new File(activity.getOriginalImageFilePath()).getName();
		String fileExtension = fileName.substring(fileName.lastIndexOf('.'));
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));

//		activity.setProcessedImageFilePath(AppUtil
//				.getExternalPublicDirectory(Environment.DIRECTORY_PICTURES)
//				+ fileName + System.currentTimeMillis() + fileExtension);
		activity.setUpShareAction();
	}
}