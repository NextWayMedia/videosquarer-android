package com.videosquarer.squareimage.util.task;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.util.Log;

import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.ImageUtil;
import com.videosquarer.squareimage.util.constant.SharedPrefKeys;

public class CreateSquareFullSizeImageTask extends AsyncTask<Void, Void, Bitmap> {

	private static final String TAG = "CreateSquareFullSizeImageTask";

	private DisplayingActivity activity;
	private Dialog progressDialog;
	private Intent intent;
    private boolean isPro;

	public CreateSquareFullSizeImageTask(DisplayingActivity activity, Intent intent, boolean isPro) {
		this.activity = activity;
		this.intent = intent;
		progressDialog = AppUtil.getProgressDialog(activity,
				AppUtil.getStringRes(R.string.creating_image_please_wait));
        this.isPro = isPro;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog.show();
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		Bitmap image = null;
		int rotatePosition = activity.getSquareImageView().getRotatePosition();
		if (rotatePosition == 0) {
			image = AppUtil.getBitmapWithoutMemoryError(
					activity.getOriginalImageFilePath());
		} else {
			Matrix matrix = new Matrix();
			matrix.postRotate(90 * rotatePosition);
			image = AppUtil.getBitmapWithoutMemoryError(
					activity.getOriginalImageFilePath(), matrix);
		}

		Matrix rotatingMatrix = ImageUtil.getRotatingMatrixFromExif(
				activity.getOriginalImageFilePath());
		Bitmap newImage = AppUtil.getBitmapWithoutMemoryError(image, rotatingMatrix);
		if (newImage != image) {
			image.recycle();
			System.gc();
		}

		Bitmap result = ImageUtil.makeSquareBitmap(newImage,
				activity.getSquareImageView().getScaledSize(),
				AppUtil.getSharedPrefInteger(SharedPrefKeys.SHARED_PREF_PICKED_COLOR),
				activity, isPro);
		return result;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		try {
			progressDialog.dismiss();
		} catch (Exception e) {
			Log.d(TAG, "Dialog exception");
		}
		new SaveBitmapTask(activity, intent).execute(result);
	}
}