package com.videosquarer.squareimage.util;

public class AM_MEDIA_TYPE {

    public	GUID majortype;
    public	GUID subtype;
    public	boolean bFixedSizeSamples;
    public	boolean bTemporalCompression;
    public	long lSampleSize;
    public	GUID formattype;
    public	byte[] pUnk;
    public	long cbFormat;
    public	byte[] pbFormat;
}
