package com.videosquarer.squareimage.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.regex.Pattern;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.widget.Toast;

public class AppUtil extends Application {

	private static final String TAG = "AppUtil";

	private static AppUtil instance;
	private SharedPreferences defaultPreferences;
	private static final String folderName = "Video Squarer";

	public static final int	   VIDEO_NO_ERRO = 0;
	public static final int	   VIDEO_ERR_BIGSIZE 		= -1;
	public static final int	   VIDEO_ERR_SHORTTIME 		= -2;
	public static final int	   VIDEO_ERR_BADCONTENT 	= -3;
	public static final int	   VIDEO_ERR_BADVIDEOTYPE 	= -4;
	
	public static final int	   VIDEO_ERR_UNKNOWN 		= -99;

	private static final int YUV420SemiPlanar_BGR = 0x10001;
	private static final int YUV420SemiPlanar_RGB = 0x10002;
	private static final int YUV420SemiPlanar_HTC = 0x10003;
	public static String[] g_strVideoMIME = {"video/avc", "video/mp4v-es", "video/3gpp"};

	public final static int g_nCodecColorFormat = MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar;
	public final static int COLOR_TI_FormatYUV420PackedSemiPlanarInterlaced = 0x7f000001;	// xiaomi, lenovo decode codec
	public final static int COLOR_SONYXPERIA_DECODE_FORMAT = 0x7fa30c03; // 2141391875	// Sony Xperia decide codec
	public final static int HTC_DECODE_CODEC			= 0x7fa30c01; // 2141391873		// HTC decode codec
	public final static int SAMSUNG_GNOTE3_DECODE_CODEC		= 0x7fa30c04; // 2141391876		// Samsung galaxy Note3 decode codec
	public final static int HTC_DECODE_CODEC2				= 0x7fa30c00;  //2141391872
	
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		defaultPreferences = PreferenceManager.getDefaultSharedPreferences(this);

	}

	public static DisplayMetrics getDisplayMetrics() {
		return instance.getResources().getDisplayMetrics();
	}

	public static ProgressDialog getProgressDialog(Context context, String message) {
		ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_SEARCH) {
					return true;
				}
				return false;
			}
		});

		return progressDialog;
	}

	public static String getSharedPrefString(String key) {
		return instance.defaultPreferences.getString(key, "");
	}

	public static int getSharedPrefInteger(String key) {
		return instance.defaultPreferences.getInt(key, 0);
	}

	public static boolean getSharedPrefBool(String key) {
		return instance.defaultPreferences.getBoolean(key, false);
	}

	public static void putSharedPref(String key, String value) {
		instance.defaultPreferences.edit().putString(key, value).commit();
	}

	public static void putSharedPref(String key, boolean value) {
		instance.defaultPreferences.edit().putBoolean(key, value).commit();
	}

	public static void putSharedPref(String key, int value) {
		instance.defaultPreferences.edit().putInt(key, value).commit();
	}

	public static void removeSharedPref(String key) {
		instance.defaultPreferences.edit().remove(key).commit();
	}

	public static void showLongToast(String message) {
		Toast.makeText(instance, message, Toast.LENGTH_LONG).show();
	}

	public static void showNearFutureToast() {
		showLongToast("This functionality will be implemented in the near future");
	}

	public static void showLongToast(int resId) {
		showLongToast(instance.getString(resId));
	}

	public static int convertToPixels(int dp) {
		Resources res = instance.getResources();
		return Math.round(TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, dp,
				res.getDisplayMetrics()));
	}

	public static int convertToDip(int px) {
		DisplayMetrics displayMetrics = instance.getResources().getDisplayMetrics();
		int dp = (int) (px / displayMetrics.density + 0.5f);
		return dp;
	}

	public static int calculateInSampleSize(int width, int height, int reqWidth, int reqHeight) {
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		Log.v(TAG, "In sample size is: " + inSampleSize);
		return inSampleSize;
	}

	public static boolean isValidText(String text) {
		if (TextUtils.isEmpty(text)) {
			return false;
		}

		if (text.replace(" ", "").replace("\r", "").replace("\n", "").length() == 0) {
			return false;
		}

		return true;
	}

	public static boolean isValidEmail(String email) {
		if (!isValidText(email)) {
			return false;
		}

		Pattern emailPattern = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
				+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
				+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25}" + ")+");

		return emailPattern.matcher(email).matches();
	}

	public static String getStringRes(int resId) {
		return instance.getString(resId);
	}

	public static String getPackage() {
		return instance.getApplicationInfo().packageName;
	}

	public static boolean isMediaStorageMounted() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
	}

	public static String getAppExternalDataDirectory() {
		StringBuilder sb = new StringBuilder();
		sb.append(Environment.getExternalStorageDirectory())
				.append(File.separator)
				.append("Android")
				.append(File.separator)
				.append("data")
				.append(File.separator)
				.append(getPackage())
				.append(File.separator);

		return sb.toString();
	}

	public static String getExternalPublicDirectory(String directoryType) {
		StringBuilder sb = new StringBuilder();
		sb.append(Environment.getExternalStoragePublicDirectory(directoryType))
				.append(File.separator)
				.append(folderName)
				.append(File.separator);

		return sb.toString();
	}

	public static boolean savePicture(String path, String filename, Bitmap pic) {
		return savePicture(path, filename, pic, Bitmap.CompressFormat.JPEG, 85);
	}

	public static boolean savePicture(String path, String filename, Bitmap pic,
			CompressFormat format, int quality) {
		boolean saved = false;
		if (path == null) {
			path = getExternalPublicDirectory(Environment.DIRECTORY_PICTURES);
		}
		try {
			File picturePath = new File(path);
			picturePath.mkdirs();

			File pictureFile = new File(path + File.separator + filename);
			pictureFile.createNewFile();

			Uri uri = Uri.fromFile(pictureFile);
			ContentResolver cr = instance.getContentResolver();
			OutputStream thumbOut = cr.openOutputStream(uri);
			saved = pic.compress(format, quality, thumbOut);
			thumbOut.close();
		} catch (Exception e) {
			Log.e(TAG, "Unable to save picture " + e.toString());
		}

		if (!saved) {
			Log.i(TAG, "failed to save " + path);
		}

		return saved;
	}

	public static Bitmap getBitmapWithoutMemoryError(String filePath) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 1;
		Bitmap image = null;
		boolean isLoaded = false;
		while (!isLoaded) {
			try {
				Log.v(TAG, "Trying to load bitmap with inSampleSize = "
						+ options.inSampleSize);
				image = BitmapFactory.decodeFile(filePath, options);
				isLoaded = true;
			} catch (OutOfMemoryError e) {
				Log.w(TAG, "We can't handle that image size. Adjusting inSampleSize.");
				options.inSampleSize++;
			}
		}

		Log.v(TAG, "Loaded bitmap with inSampleSize = "
				+ options.inSampleSize);

		return image;
	}

	public static Bitmap getBitmapWithoutMemoryError(Bitmap originBitmap, Matrix matrix) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 1;
		Bitmap image = null;
		boolean isLoaded = false;
		while (!isLoaded) {
			try {
				Log.v(TAG, "Trying to load bitmap with inSampleSize = "
						+ options.inSampleSize);
				image = Bitmap.createBitmap(originBitmap, 0, 0,
						originBitmap.getWidth(), originBitmap.getHeight(),
						matrix, true);
				isLoaded = true;
			} catch (OutOfMemoryError e) {
				Log.w(TAG, "We can't handle that image size. Adjusting inSampleSize.");
				options.inSampleSize++;
			}
		}

		Log.v(TAG, "Loaded bitmap with inSampleSize = "
				+ options.inSampleSize);

		return image;
	}

	public static Bitmap getBitmapWithoutMemoryError(String filePath, Matrix matrix) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 1;
		Bitmap originBitmap = null;
		Bitmap resultImage = null;
		boolean isLoaded = false;
		while (!isLoaded) {
			try {
				Log.v(TAG, "Trying to load bitmap with inSampleSize = "
						+ options.inSampleSize);
				originBitmap = getBitmapWithoutMemoryError(filePath);
				resultImage = Bitmap.createBitmap(originBitmap, 0, 0,
						originBitmap.getWidth(), originBitmap.getHeight(),
						matrix, true);
				isLoaded = true;
			} catch (OutOfMemoryError e) {
				Log.w(TAG, "We can't handle that image size. Adjusting inSampleSize.");
				options.inSampleSize++;
			}
		}

		originBitmap.recycle();
		System.gc();

		Log.v(TAG, "Loaded bitmap with inSampleSize = "
				+ options.inSampleSize);

		return resultImage;
	}

	public static Bitmap getBitmapSquareWithoutMemoryError(int imageSize) {
		Bitmap resultedBitmap = null;
		boolean isLoaded = false;
		while (!isLoaded) {
			try {
				Log.v(TAG, "Trying to create bitmap with size = "
						+ imageSize + "x" + imageSize);
				resultedBitmap = Bitmap.createBitmap(imageSize, imageSize,
						Bitmap.Config.ARGB_8888);
				isLoaded = true;
			} catch (OutOfMemoryError e) {
				Log.w(TAG, "We can't handle that image size. Adjusting inSampleSize.");
				imageSize = imageSize - 50;
			}
		}

		return resultedBitmap;
	}

	public static Bitmap getBitmapWithoutMemoryError(Bitmap originBitmap, int imageSize) {
		Bitmap resultedBitmap = null;
		boolean isLoaded = false;
		while (!isLoaded) {
			try {
				Log.v(TAG, "Trying to create bitmap with size = "
						+ imageSize + "x" + imageSize);
				resultedBitmap = Bitmap.createBitmap(originBitmap, 0, 0, imageSize, imageSize);
				isLoaded = true;
			} catch (OutOfMemoryError e) {
				Log.w(TAG, "We can't handle that image size. Adjusting inSampleSize.");
				imageSize = imageSize - 50;
			}
		}

		return resultedBitmap;
	}

    public static boolean copyFile(File fileIn, File fileOut) {
        if (!fileIn.exists()) {
            return false;
        }
        boolean res = true;

        //checkMainDir();

        OutputStream ou = null;
        try {

            FileInputStream fis = new FileInputStream(fileIn);
            createFile(fileOut);
            ou = new FileOutputStream(fileOut);

            copyStream(fis, ou);

        } catch (Exception e) {
            e.printStackTrace();
            res = false;
        } finally {
            try {
                if (ou != null)
                    ou.close();
            } catch (IOException e) {
                e.printStackTrace();
                res = false;
            }

        }
        return res;
    }

    public static void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 8096;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static boolean createFile(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return true;
        }
        return false;
    }
    
    	public static String getDeviceColorFormat(Integer[] colorInfo)
	{
		int colorFormat = -1;
		String encodeCodecName = "video/avc";
		int colorExtension = YUV420SemiPlanar_RGB;
		int defWidth = 640, defHeight = 480;
		int defBitrate = 700000, defFps = 30, defInterval = 1;
		String codecName = "";

		for (int i = 0; i < MediaCodecList.getCodecCount(); i++) 
		{
			MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);
			if (!info.isEncoder()) {
				continue;
			}

			String[] types = info.getSupportedTypes();
			boolean found = false;
			for (int j = 0; j < types.length && !found; j++) {

				if (types[j].equals(g_strVideoMIME[0])){
					found = true;		                 
				}
			}

			if (!found)
				continue;

			ArrayList<Integer> lstFormat = new ArrayList<Integer>();
			MediaCodecInfo.CodecCapabilities capabilities = info.getCapabilitiesForType("video/avc");
			for (int j = 0; j < capabilities.colorFormats.length; j++) {
				lstFormat.add(capabilities.colorFormats[j]);
			}

			if (lstFormat.contains(MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar)){
				colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar;
			}else if (lstFormat.contains(MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar)){
				colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar;
			}else if (lstFormat.contains(MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar)){
				colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar;
			}else{
				colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar;
			}

			codecName = info.getName();

			MediaCodec mEncoder = MediaCodec.createByCodecName(codecName);
			MediaFormat mediaFormat = MediaFormat.createVideoFormat("video/avc", defWidth, defHeight);

			mediaFormat.setInteger(MediaFormat.KEY_WIDTH, defWidth);
			mediaFormat.setInteger(MediaFormat.KEY_HEIGHT, defHeight);
			mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, defBitrate);
			mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, defFps);
			mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
			mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, defInterval);

			try {
				mEncoder.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
				mEncoder.start();
			}
			catch (Exception e)
			{
				mEncoder = null;
				colorFormat = -1;
				continue;
			}

			if (mEncoder != null)
			{
				mEncoder.stop();
				mEncoder.release();
				mEncoder = null;

				encodeCodecName = codecName;

				// check HTC
				if (lstFormat.contains(HTC_DECODE_CODEC)){
					colorExtension = YUV420SemiPlanar_HTC;
				}

				// check samsung galaxy note n5110.
				if(lstFormat.contains(0x7F000001) &&
						lstFormat.contains(0x7F000002) &&
						lstFormat.contains(0x7F000003) &&
						lstFormat.contains(0x13))
				{
					colorExtension = YUV420SemiPlanar_BGR;
				}
				break;
			}
		}

		colorInfo[0] = colorFormat;
		colorInfo[1] = colorExtension;
		return encodeCodecName;
	}

	public static boolean isSystemHighVersion()
	{
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
			return true;
		else
			return false;
	}
	
	public static int getSystemVer(){

		return android.os.Build.VERSION.SDK_INT;
	}

}
