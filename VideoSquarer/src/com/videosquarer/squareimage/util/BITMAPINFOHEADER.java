package com.videosquarer.squareimage.util;

public class BITMAPINFOHEADER {

    public	int			biSize;
    public	int			biWidth;
    public	int			biHeight;
    public	short       biPlanes;
    public	short       biBitCount;
    public	int			biCompression;
    public	int			biSizeImage;
    public	int			biXPelsPerMeter;
    public	int			biYPelsPerMeter;
    public	int			biClrUsed;
    public	int			biClrImportant;
    
    public static int size()
    {
    	return 4 + 4 + 4 + 2 + 2 + 4 + 4 + 4 + 4 + 4 + 4;
    }
}
