package com.videosquarer.squareimage.util;

import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.util.Log;

import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.gui.element.SquareImageView;

public class ImageUtil {

	public static final String TAG = "ImageUtil";

	public static Bitmap makeSquareBitmap(Bitmap originBitmap, int previewSize,
			int color, DisplayingActivity activity, boolean isPro) {
		Log.v(TAG, "Make square bitmap");
		Log.v(TAG, "Color is " + color);

		color = Color.rgb(Color.red(color), Color.green(color), Color.blue(color));
		Log.v(TAG, "Parsed color is " + color);

		boolean isImageTurned = originBitmap.getWidth() > originBitmap.getHeight()
				? true : false;

		int largestImageSide = isImageTurned
				? originBitmap.getWidth()
				: originBitmap.getHeight();

		float ratio = (float) largestImageSide / (float) previewSize;
		Log.v(TAG, "Ratio between original and preview image is " + ratio);

		int smallestImageSide = isImageTurned
				? originBitmap.getHeight()
				: originBitmap.getWidth();

		int smallBorder = (int) (SquareImageView.smallestBorderSize * ratio);
		int imageSize = (largestImageSide + (smallBorder * 2));

		int originalImageSize = imageSize;
		Bitmap resultedBitmap = AppUtil.getBitmapSquareWithoutMemoryError(imageSize);
		imageSize = resultedBitmap.getWidth();
		float scaleRatio = (float) originalImageSize / (float) resultedBitmap.getWidth();

		smallBorder = (int) (smallBorder / scaleRatio);

		int largeBorder = (int) ((imageSize - (smallestImageSide / scaleRatio)) / 2);

		int leftPoint = isImageTurned
				? smallBorder
				: largeBorder;

		int topPoint = isImageTurned
				? largeBorder
				: smallBorder;

		int rightPoint = isImageTurned
				? imageSize - smallBorder
				: imageSize - leftPoint;

		int bottomPoint = isImageTurned
				? imageSize - topPoint
				: imageSize - smallBorder;

		Rect rect = new Rect(leftPoint, topPoint, rightPoint, bottomPoint);

		Log.d(TAG, "Created result bitmap with size = " + imageSize + "x" + imageSize);
		Log.d(TAG, "Canvas scale ratio is " + scaleRatio);

		Log.d(TAG, "Small border size is " + smallBorder);
		Log.d(TAG, "Small border size at preview is " + SquareImageView.smallestBorderSize);
		Log.d(TAG, "Large border size is " + largeBorder);

		Log.d(TAG, "Rectangle for image is " + rect.toString());

		Canvas canvas = new Canvas(resultedBitmap);
		Paint paint = new Paint();
		paint.setColor(color);
		if(DisplayingActivity.isBlur)
		{
		    Rect rect1 = new Rect();

		    rect1.left = 0;
		    rect1.top = 0;
		    rect1.bottom = canvas.getWidth();
		    rect1.right = canvas.getHeight();

			canvas.drawBitmap(DisplayingActivity.blurBitmap, null, rect1, null);
		}
		else
			canvas.drawColor(color);
		canvas.drawBitmap(originBitmap, null, rect, null);
        if (!isPro) {
            Bitmap watermark = BitmapFactory.decodeResource(activity.getResources(), R.drawable.watermark);
            if (ratio != 1) {
                Bitmap scaledWatermark = BitmapWrapper.createScaledBitmap(watermark, (int)(watermark.getWidth() * ratio), (int)(watermark.getHeight() * ratio), true);
                watermark.recycle();
        		System.gc();
        		watermark = scaledWatermark;
            }

//            if (!isImageTurned && watermark.getWidth() > watermark.getHeight()) {
//                watermark = rotateImage(watermark, 90);
//            }
//            if (isImageTurned && watermark.getWidth() > canvas.getWidth() / 3) {
//                int newWidth = canvas.getWidth() / 3;
//                int newHeight = (int)((float) newWidth / watermark.getWidth() * watermark.getHeight());
//                watermark = Bitmap.createScaledBitmap(watermark, newWidth, newHeight, true);
//            } else if (!isImageTurned && watermark.getHeight() > canvas.getHeight() / 3) {
//                int newHeight = canvas.getHeight() / 3;
//                int newWidth = (int)((float) newHeight / watermark.getHeight() * watermark.getWidth());
//                watermark = Bitmap.createScaledBitmap(watermark, newWidth, newHeight, true);
//            }
            int newWidth = canvas.getWidth() / 3;
            int newHeight = (int)((float) newWidth / watermark.getWidth() * watermark.getHeight());
            Bitmap resizeWatermark = Bitmap.createScaledBitmap(watermark, newWidth, newHeight, true);
            watermark.recycle();
            
            Paint translucentPaint = new Paint();
            translucentPaint.setAlpha(85);
            
            canvas.drawBitmap(resizeWatermark,
                    canvas.getWidth() - resizeWatermark.getWidth() - 10,
                    canvas.getHeight() - resizeWatermark.getHeight() - 10,
                    translucentPaint);
        }
		return resultedBitmap;
	}

	public static Bitmap makeSquareBackgroundBitmap(Bitmap originBitmap, int previewSize,
			int color, DisplayingActivity activity, boolean isPro) {
		Log.v(TAG, "Make square bitmap");
		Log.v(TAG, "Color is " + color);

		color = Color.rgb(Color.red(color), Color.green(color), Color.blue(color));
		Log.v(TAG, "Parsed color is " + color);

		boolean isImageTurned = originBitmap.getWidth() > originBitmap.getHeight()
				? true : false;

		int largestImageSide = isImageTurned
				? originBitmap.getWidth()
				: originBitmap.getHeight();

		float ratio = (float) largestImageSide / (float) previewSize;
		Log.v(TAG, "Ratio between original and preview image is " + ratio);

		int smallestImageSide = isImageTurned
				? originBitmap.getHeight()
				: originBitmap.getWidth();

		int smallBorder = (int) (SquareImageView.smallestBorderSize * ratio);
		int imageSize = (largestImageSide + (smallBorder * 2));

		int originalImageSize = imageSize;
		Bitmap resultedBitmap = AppUtil.getBitmapSquareWithoutMemoryError(imageSize);
		imageSize = resultedBitmap.getWidth();
		float scaleRatio = (float) originalImageSize / (float) resultedBitmap.getWidth();

		smallBorder = (int) (smallBorder / scaleRatio);

		int largeBorder = (int) ((imageSize - (smallestImageSide / scaleRatio)) / 2);

		int leftPoint = isImageTurned
				? smallBorder
				: largeBorder;

		int topPoint = isImageTurned
				? largeBorder
				: smallBorder;

		int rightPoint = isImageTurned
				? imageSize - smallBorder
				: imageSize - leftPoint;

		int bottomPoint = isImageTurned
				? imageSize - topPoint
				: imageSize - smallBorder;

		Rect rect = new Rect(leftPoint, topPoint, rightPoint, bottomPoint);

		Log.d(TAG, "Created result bitmap with size = " + imageSize + "x" + imageSize);
		Log.d(TAG, "Canvas scale ratio is " + scaleRatio);

		Log.d(TAG, "Small border size is " + smallBorder);
		Log.d(TAG, "Small border size at preview is " + SquareImageView.smallestBorderSize);
		Log.d(TAG, "Large border size is " + largeBorder);

		Log.d(TAG, "Rectangle for image is " + rect.toString());

		Canvas canvas = new Canvas(resultedBitmap);
		Paint paint = new Paint();
		paint.setColor(color);
		if(DisplayingActivity.isBlur)
		{
		    Rect rect1 = new Rect();

		    rect1.left = 0;
		    rect1.top = 0;
		    rect1.bottom = canvas.getWidth();
		    rect1.right = canvas.getHeight();

			canvas.drawBitmap(DisplayingActivity.blurBitmap, null, rect1, null);
		}
		else
			canvas.drawColor(color);
		//canvas.drawBitmap(originBitmap, null, rect, null);
        if (!isPro) {
            Bitmap watermark = BitmapFactory.decodeResource(activity.getResources(), R.drawable.watermark);
            if (ratio != 1) {
                Bitmap scaledWatermark = BitmapWrapper.createScaledBitmap(watermark, (int)(watermark.getWidth() * ratio), (int)(watermark.getHeight() * ratio), true);
                watermark.recycle();
        		System.gc();
        		watermark = scaledWatermark;
            }

            int newWidth = canvas.getWidth() / 3;
            int newHeight = (int)((float) newWidth / watermark.getWidth() * watermark.getHeight());
            Bitmap resizeWatermark = Bitmap.createScaledBitmap(watermark, newWidth, newHeight, true);
            watermark.recycle();
            
            Paint translucentPaint = new Paint();
            translucentPaint.setAlpha(85);
            
            canvas.drawBitmap(resizeWatermark,
                    canvas.getWidth() - resizeWatermark.getWidth() - 10,
                    canvas.getHeight() - resizeWatermark.getHeight() - 10,
                    translucentPaint);
        }
		return resultedBitmap;
	}

	public static Bitmap makeSquareMaskBitmap(Bitmap originBitmap, int previewSize,
			int color, DisplayingActivity activity, boolean isPro) {
		Log.v(TAG, "Make square bitmap");
		Log.v(TAG, "Color is " + color);

		color = Color.rgb(Color.red(color), Color.green(color), Color.blue(color));
		Log.v(TAG, "Parsed color is " + color);

		boolean isImageTurned = originBitmap.getWidth() > originBitmap.getHeight()
				? true : false;

		int largestImageSide = isImageTurned
				? originBitmap.getWidth()
				: originBitmap.getHeight();

		float ratio = (float) largestImageSide / (float) previewSize;
		Log.v(TAG, "Ratio between original and preview image is " + ratio);

		int smallestImageSide = isImageTurned
				? originBitmap.getHeight()
				: originBitmap.getWidth();

		int smallBorder = (int) (SquareImageView.smallestBorderSize * ratio);
		int imageSize = (largestImageSide + (smallBorder * 2));

		int originalImageSize = imageSize;
		Bitmap resultedBitmap = AppUtil.getBitmapSquareWithoutMemoryError(imageSize);
		imageSize = resultedBitmap.getWidth();
		float scaleRatio = (float) originalImageSize / (float) resultedBitmap.getWidth();

		smallBorder = (int) (smallBorder / scaleRatio);

		int largeBorder = (int) ((imageSize - (smallestImageSide / scaleRatio)) / 2);

		int leftPoint = isImageTurned
				? smallBorder
				: largeBorder;

		int topPoint = isImageTurned
				? largeBorder
				: smallBorder;

		int rightPoint = isImageTurned
				? imageSize - smallBorder
				: imageSize - leftPoint;

		int bottomPoint = isImageTurned
				? imageSize - topPoint
				: imageSize - smallBorder;

		Rect rect = new Rect(leftPoint, topPoint, rightPoint, bottomPoint);

		Log.d(TAG, "Created result bitmap with size = " + imageSize + "x" + imageSize);
		Log.d(TAG, "Canvas scale ratio is " + scaleRatio);

		Log.d(TAG, "Small border size is " + smallBorder);
		Log.d(TAG, "Small border size at preview is " + SquareImageView.smallestBorderSize);
		Log.d(TAG, "Large border size is " + largeBorder);

		Log.d(TAG, "Rectangle for image is " + rect.toString());

		Canvas canvas = new Canvas(resultedBitmap);
		canvas.drawColor(Color.TRANSPARENT);

        if (!isPro) {
            Bitmap watermark = BitmapFactory.decodeResource(activity.getResources(), R.drawable.watermark);
            if (ratio != 1) {
                Bitmap scaledWatermark = BitmapWrapper.createScaledBitmap(watermark, (int)(watermark.getWidth() * ratio), (int)(watermark.getHeight() * ratio), true);
                watermark.recycle();
        		System.gc();
        		watermark = scaledWatermark;
            }

            int newWidth = canvas.getWidth() / 3;
            int newHeight = (int)((float) newWidth / watermark.getWidth() * watermark.getHeight());
            Bitmap resizeWatermark = BitmapWrapper.createScaledBitmap(watermark, newWidth, newHeight, true);
            watermark.recycle();
            System.gc();
            
            Paint translucentPaint = new Paint();
            translucentPaint.setAlpha(85);
            
            canvas.drawBitmap(resizeWatermark,
                    canvas.getWidth() - resizeWatermark.getWidth() - 10,
                    canvas.getHeight() - resizeWatermark.getHeight() - 10,
                    translucentPaint);
        }
        
//		Bitmap output = Bitmap.createBitmap(resultedBitmap.getWidth(),
//				resultedBitmap.getHeight(), Config.ARGB_8888);
		//Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
		//return _bmp;
		return resultedBitmap;
//		resultedBitmap.recycle();
//		Bitmap cropBitmap = Bitmap.createScaledBitmap(output, previewSize, previewSize, false);
//		output.recycle();
//		return cropBitmap;

	}

    public static Bitmap rotateImage(Bitmap image, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
    }

	public static Matrix getRotatingMatrixFromExif(String filePath) {
		int exifOrientation = 0;
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(filePath);
			exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Matrix matrix = new Matrix();
		switch (exifOrientation) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			matrix.postRotate(90);

			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			matrix.postRotate(180);
			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			matrix.postRotate(270);
			break;
		}

		return matrix;
	}

	public static void scanFile(Context context, String path, String mimeType) {
		Client client = new Client(path, mimeType);
		MediaScannerConnection connection = new MediaScannerConnection(context, client);
		client.connection = connection;
		connection.connect();
	}

	private static final class Client implements MediaScannerConnectionClient {
		private final String path;
		private final String mimeType;
		MediaScannerConnection connection;

		public Client(String path, String mimeType) {
			this.path = path;
			this.mimeType = mimeType;
		}

		@Override
		public void onMediaScannerConnected() {
			connection.scanFile(path, mimeType);
		}

		@Override
		public void onScanCompleted(String path, Uri uri) {

			connection.disconnect();
		}
	}

}
