package com.videosquarer.squareimage.util;

public class VIDEOINFOHEADER {

    public	RECT            rcSource = new RECT();          // The bit we really want to use
    public	RECT            rcTarget = new RECT();          // Where the video should go
    public	int				dwBitErrorRate;    // Bit error rate for this stream
    //longlong  AvgTimePerFrame;   // Average time per frame (100ns units)
    public	long  AvgTimePerFrame;   // Average time per frame (100ns units)

    public	BITMAPINFOHEADER bmiHeader = new BITMAPINFOHEADER();
}
