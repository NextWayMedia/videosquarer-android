package com.videosquarer.squareimage.util;

import android.graphics.Bitmap;

public class MediaFrame {

	public	long			lStartTime;		// millisecond
	public	long			lEndTime;		// millisecond
	public	Bitmap			bmpData;
	public	byte[]			pbData;
	public	long			cbData;
	public	int				dwPos;
	public	int				dwWidth;
	public	int				dwHeight;
	
	public MediaFrame()
	{
		bmpData = null;
		pbData = null;
		cbData = 0;
	}
	
	public void MediaFrame_Free()
	{
		if (bmpData != null)
		{
			BitmapWrapper.recycleBitmap(bmpData);
			bmpData = null;
		}
		pbData = null;
		cbData = 0;
	}
}
