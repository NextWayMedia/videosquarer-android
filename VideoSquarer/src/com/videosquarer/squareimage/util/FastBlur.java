package com.videosquarer.squareimage.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RSIllegalArgumentException;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;

import com.videosquarer.squareimage.gui.DisplayingActivity;

public class FastBlur {

	private static final String TAG = "FastBlur";

	public static String blurVertexShaderString = null;
	public static String blurFragmentShaderString = null;

	@SuppressLint("NewApi")
	public static Bitmap fastblur(Context context, Bitmap sentBitmap, int radius) {

		//if (VERSION.SDK_INT > 16) {
		if(false){
			Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
			synchronized (context) {
				try
				{
					final RenderScript rs = RenderScript.create(context);
					final Allocation input = Allocation.createFromBitmap(rs, sentBitmap, Allocation.MipmapControl.MIPMAP_NONE,
							Allocation.USAGE_SCRIPT);
					final Allocation output = Allocation.createTyped(rs, input.getType());
					final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
					script.setRadius(radius /* e.g. 3.f */);
					script.setInput(input);
					script.forEach(output);
					output.copyTo(bitmap);
					return bitmap;
				}
				catch (RSIllegalArgumentException e)
				{
					Log.d(TAG, "radius = " + radius);
					return null;
				}
			}

		}
		else
		{
			Bitmap dstBitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
			DisplayingActivity.nativeFastBlur(dstBitmap, sentBitmap.getWidth(), sentBitmap.getHeight(), radius);
			return dstBitmap;
		}
		// Stack Blur v1.0 from
		// http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
		//
		// Java Author: Mario Klingemann <mario at quasimondo.com>
		// http://incubator.quasimondo.com
		// created Feburary 29, 2004
		// Android port : Yahel Bouaziz <yahel at kayenko.com>
		// http://www.kayenko.com
		// ported april 5th, 2012

		// This is a compromise between Gaussian Blur and Box blur
		// It creates much better looking blurs than Box Blur, but is
		// 7x faster than my Gaussian Blur implementation.
		//
		// I called it Stack Blur because this describes best how this
		// filter works internally: it creates a kind of moving stack
		// of colors whilst scanning through the image. Thereby it
		// just has to add one new block of color to the right side
		// of the stack and remove the leftmost color. The remaining
		// colors on the topmost layer of the stack are either added on
		// or reduced by one, depending on if they are on the right or
		// on the left side of the stack.
		//
		// If you are using this algorithm in your code please add
		// the following line:
		//
		// Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

		//		Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
		//
		//		if (radius < 1) {
		//			return (null);
		//		}
		//
		//		int w = bitmap.getWidth();
		//		int h = bitmap.getHeight();
		//
		//		int[] pix = new int[w * h];
		//		Log.e("pix", w + " " + h + " " + pix.length);
		//		bitmap.getPixels(pix, 0, w, 0, 0, w, h);
		//
		//		int wm = w - 1;
		//		int hm = h - 1;
		//		int wh = w * h;
		//		int div = radius + radius + 1;
		//
		//		int r[] = new int[wh];
		//		int g[] = new int[wh];
		//		int b[] = new int[wh];
		//		int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
		//		int vmin[] = new int[Math.max(w, h)];
		//
		//		int divsum = (div + 1) >> 1;
		//		divsum *= divsum;
		//		int dv[] = new int[256 * divsum];
		//		for (i = 0; i < 256 * divsum; i++) {
		//			dv[i] = (i / divsum);
		//		}
		//
		//		yw = yi = 0;
		//
		//		int[][] stack = new int[div][3];
		//		int stackpointer;
		//		int stackstart;
		//		int[] sir;
		//		int rbs;
		//		int r1 = radius + 1;
		//		int routsum, goutsum, boutsum;
		//		int rinsum, ginsum, binsum;
		//
		//		for (y = 0; y < h; y++) {
		//			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
		//			for (i = -radius; i <= radius; i++) {
		//				p = pix[yi + Math.min(wm, Math.max(i, 0))];
		//				sir = stack[i + radius];
		//				sir[0] = (p & 0xff0000) >> 16;
		//				sir[1] = (p & 0x00ff00) >> 8;
		//				sir[2] = (p & 0x0000ff);
		//				rbs = r1 - Math.abs(i);
		//				rsum += sir[0] * rbs;
		//				gsum += sir[1] * rbs;
		//				bsum += sir[2] * rbs;
		//				if (i > 0) {
		//					rinsum += sir[0];
		//					ginsum += sir[1];
		//					binsum += sir[2];
		//				} else {
		//					routsum += sir[0];
		//					goutsum += sir[1];
		//					boutsum += sir[2];
		//				}
		//			}
		//			stackpointer = radius;
		//
		//			for (x = 0; x < w; x++) {
		//
		//				r[yi] = dv[rsum];
		//				g[yi] = dv[gsum];
		//				b[yi] = dv[bsum];
		//
		//				rsum -= routsum;
		//				gsum -= goutsum;
		//				bsum -= boutsum;
		//
		//				stackstart = stackpointer - radius + div;
		//				sir = stack[stackstart % div];
		//
		//				routsum -= sir[0];
		//				goutsum -= sir[1];
		//				boutsum -= sir[2];
		//
		//				if (y == 0) {
		//					vmin[x] = Math.min(x + radius + 1, wm);
		//				}
		//				p = pix[yw + vmin[x]];
		//
		//				sir[0] = (p & 0xff0000) >> 16;
		//				sir[1] = (p & 0x00ff00) >> 8;
		//				sir[2] = (p & 0x0000ff);
		//
		//				rinsum += sir[0];
		//				ginsum += sir[1];
		//				binsum += sir[2];
		//
		//				rsum += rinsum;
		//				gsum += ginsum;
		//				bsum += binsum;
		//
		//				stackpointer = (stackpointer + 1) % div;
		//				sir = stack[(stackpointer) % div];
		//
		//				routsum += sir[0];
		//				goutsum += sir[1];
		//				boutsum += sir[2];
		//
		//				rinsum -= sir[0];
		//				ginsum -= sir[1];
		//				binsum -= sir[2];
		//
		//				yi++;
		//			}
		//			yw += w;
		//		}
		//		for (x = 0; x < w; x++) {
		//			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
		//			yp = -radius * w;
		//			for (i = -radius; i <= radius; i++) {
		//				yi = Math.max(0, yp) + x;
		//
		//				sir = stack[i + radius];
		//
		//				sir[0] = r[yi];
		//				sir[1] = g[yi];
		//				sir[2] = b[yi];
		//
		//				rbs = r1 - Math.abs(i);
		//
		//				rsum += r[yi] * rbs;
		//				gsum += g[yi] * rbs;
		//				bsum += b[yi] * rbs;
		//
		//				if (i > 0) {
		//					rinsum += sir[0];
		//					ginsum += sir[1];
		//					binsum += sir[2];
		//				} else {
		//					routsum += sir[0];
		//					goutsum += sir[1];
		//					boutsum += sir[2];
		//				}
		//
		//				if (i < hm) {
		//					yp += w;
		//				}
		//			}
		//			yi = x;
		//			stackpointer = radius;
		//			for (y = 0; y < h; y++) {
		//				// Preserve alpha channel: ( 0xff000000 & pix[yi] )
		//				pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];
		//
		//				rsum -= routsum;
		//				gsum -= goutsum;
		//				bsum -= boutsum;
		//
		//				stackstart = stackpointer - radius + div;
		//				sir = stack[stackstart % div];
		//
		//				routsum -= sir[0];
		//				goutsum -= sir[1];
		//				boutsum -= sir[2];
		//
		//				if (x == 0) {
		//					vmin[y] = Math.min(y + r1, hm) * w;
		//				}
		//				p = x + vmin[y];
		//
		//				sir[0] = r[p];
		//				sir[1] = g[p];
		//				sir[2] = b[p];
		//
		//				rinsum += sir[0];
		//				ginsum += sir[1];
		//				binsum += sir[2];
		//
		//				rsum += rinsum;
		//				gsum += ginsum;
		//				bsum += binsum;
		//
		//				stackpointer = (stackpointer + 1) % div;
		//				sir = stack[stackpointer];
		//
		//				routsum += sir[0];
		//				goutsum += sir[1];
		//				boutsum += sir[2];
		//
		//				rinsum -= sir[0];
		//				ginsum -= sir[1];
		//				binsum -= sir[2];
		//
		//				yi += w;
		//			}
		//		}
		//
		//		Log.e("pix", w + " " + h + " " + pix.length);
		//		bitmap.setPixels(pix, 0, w, 0, 0, w, h);
		//		return (bitmap);
	}

	public static void setBlurRadius(int blurRadius)
	{

		int calculatedSampleRadius = 0;
		if (blurRadius >= 1) // Avoid a divide-by-zero error here
		{
			// Calculate the number of pixels to sample from by setting a bottom limit for the contribution of the outermost pixel
			float minimumWeightToFindEdgeOfSamplingArea = 1.0f / 256.0f;
			calculatedSampleRadius = (int) Math.floor(Math.sqrt(-2.0 * Math.pow(blurRadius, 2.0) * Math.log(minimumWeightToFindEdgeOfSamplingArea * Math.sqrt(2.0f * Math.PI * Math.pow(blurRadius, 2.0))) ));
			calculatedSampleRadius += calculatedSampleRadius % 2; // There's nothing to gain from handling odd radius sizes, due to the optimizations I use
		}

		//	        NSLog(@"Blur radius: %f, calculated sample radius: %d", _blurRadiusInPixels, calculatedSampleRadius);
		//	        
		blurVertexShaderString = getVertexShaderForOptimizedBlurOfRadius(calculatedSampleRadius, (float)blurRadius);
		blurFragmentShaderString = getFragmentShaderForOptimizedBlurOfRadius(calculatedSampleRadius, (float)blurRadius);
	}

	public static String getVertexShaderForOptimizedBlurOfRadius(int blurRadius, float sigma)
	{
		if (blurRadius < 1)
		{
			return  "attribute vec4 position; \n" + 
					"attribute vec4 inputTextureCoordinate;\n" +
					"\n" +
					"varying vec2 textureCoordinate;\n" +
					"\n" +
					"void main() \n" +
					"{\n" +
					"gl_Position = position;\n" +
					"textureCoordinate = inputTextureCoordinate.xy;\n" +
			 		"}\n";
		}

		float []standardGaussianWeights = new float[blurRadius + 1];
		float sumOfWeights = 0.0f;
		for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
		{
			standardGaussianWeights[currentGaussianWeightIndex] = (float) ((1.0 / Math.sqrt(2.0 * Math.PI * Math.pow(sigma, 2.0))) 
					* Math.exp(-Math.pow(currentGaussianWeightIndex, 2.0) / (2.0 * Math.pow(sigma, 2.0))));

			if (currentGaussianWeightIndex == 0)
			{
				sumOfWeights += standardGaussianWeights[currentGaussianWeightIndex];
			}
			else
			{
				sumOfWeights += 2.0 * standardGaussianWeights[currentGaussianWeightIndex];
			}
		}

		for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
		{
			standardGaussianWeights[currentGaussianWeightIndex] = standardGaussianWeights[currentGaussianWeightIndex] / sumOfWeights;
		}

		int numberOfOptimizedOffsets = Math.min(blurRadius / 2 + (blurRadius % 2), 7);
		float []optimizedGaussianOffsets = new float[numberOfOptimizedOffsets];

		for (int currentOptimizedOffset = 0; currentOptimizedOffset < numberOfOptimizedOffsets; currentOptimizedOffset++)
		{
			float firstWeight = standardGaussianWeights[currentOptimizedOffset*2 + 1];
			float secondWeight = standardGaussianWeights[currentOptimizedOffset*2 + 2];

			float optimizedWeight = firstWeight + secondWeight;

			optimizedGaussianOffsets[currentOptimizedOffset] = (firstWeight * (currentOptimizedOffset*2 + 1) + secondWeight * (currentOptimizedOffset*2 + 2)) / optimizedWeight;
		}

		// First, generate the normal Gaussian weights for a given sigma

		// Next, normalize these weights to prevent the clipping of the Gaussian curve at the end of the discrete samples from reducing luminance

		// From these weights we calculate the offsets to read interpolated values from

		String shaderString ;
		// Header
		shaderString = String.format(
				"attribute vec4 position;\n" + 
						"attribute vec4 inputTextureCoordinate;\n" +
						"\n" +
						"uniform float texelWidthOffset;\n" +
						"uniform float texelHeightOffset;\n" +
						"\n" +
						"varying vec2 blurCoordinates[%d];\n" +
						"\n" +
						"void main()\n" +
						"{\n" +
						"   gl_Position = position;\n" +
						"   \n" +
						"   vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);\n", (1 + (numberOfOptimizedOffsets * 2)));

		// Inner offset loop
		shaderString += "blurCoordinates[0] = inputTextureCoordinate.xy;\n";

		for (int currentOptimizedOffset = 0; currentOptimizedOffset < numberOfOptimizedOffsets; currentOptimizedOffset++)
		{
			shaderString += String.format("blurCoordinates[%d] = inputTextureCoordinate.xy + singleStepOffset * %f;\n" +
					"blurCoordinates[%d] = inputTextureCoordinate.xy - singleStepOffset * %f;\n", 
					(currentOptimizedOffset * 2) + 1, optimizedGaussianOffsets[currentOptimizedOffset], 
					(currentOptimizedOffset * 2) + 2, optimizedGaussianOffsets[currentOptimizedOffset]);
		}

		// Footer
		shaderString += "}\n";

		return shaderString;
	}

	public static String getFragmentShaderForOptimizedBlurOfRadius(int blurRadius, float sigma)
	{
		if (blurRadius < 1)
		{
			return  "varying vec2 textureCoordinate;\n" +
					"\n" +
					"uniform sampler2D inputImageTexture;\n" +
					"\n" +
					"void main()\n" +
					"{\n" +
					"gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n" +
			 		"}\n";
		}

		// First, generate the normal Gaussian weights for a given sigma
		float []standardGaussianWeights = new float[blurRadius + 1];
		float sumOfWeights = 0.0f;
		for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
		{
			standardGaussianWeights[currentGaussianWeightIndex] = (float) ((1.0 / Math.sqrt(2.0 * Math.PI * Math.pow(sigma, 2.0))) * 
					Math.exp(-Math.pow(currentGaussianWeightIndex, 2.0) / (2.0 * Math.pow(sigma, 2.0))));

			if (currentGaussianWeightIndex == 0)
			{
				sumOfWeights += standardGaussianWeights[currentGaussianWeightIndex];
			}
			else
			{
				sumOfWeights += 2.0 * standardGaussianWeights[currentGaussianWeightIndex];
			}
		}

		// Next, normalize these weights to prevent the clipping of the Gaussian curve at the end of the discrete samples from reducing luminance
		for (int currentGaussianWeightIndex = 0; currentGaussianWeightIndex < blurRadius + 1; currentGaussianWeightIndex++)
		{
			standardGaussianWeights[currentGaussianWeightIndex] = standardGaussianWeights[currentGaussianWeightIndex] / sumOfWeights;
		}

		// From these weights we calculate the offsets to read interpolated values from
		int numberOfOptimizedOffsets = Math.min(blurRadius / 2 + (blurRadius % 2), 7);
		int trueNumberOfOptimizedOffsets = blurRadius / 2 + (blurRadius % 2);

		String shaderString;

		shaderString = String.format("uniform sampler2D inputImageTexture;\n" +
				"uniform highp float texelWidthOffset;\n" +
				"uniform highp float texelHeightOffset;\n" +
				"\n" +
				"varying highp vec2 blurCoordinates[%d];\n" +
				"\n" +
				"void main()\n" +
				"{\n" +
				"	highp vec4 sum = vec4(0.0);\n", 1 + (numberOfOptimizedOffsets * 2));

		// Inner texture loop
		shaderString += String.format("sum += texture2D(inputImageTexture, blurCoordinates[0]) * %f;\n", standardGaussianWeights[0]);

		for (int currentBlurCoordinateIndex = 0; currentBlurCoordinateIndex < numberOfOptimizedOffsets; currentBlurCoordinateIndex++)
		{
			float firstWeight = standardGaussianWeights[currentBlurCoordinateIndex * 2 + 1];
			float secondWeight = standardGaussianWeights[currentBlurCoordinateIndex * 2 + 2];
			float optimizedWeight = firstWeight + secondWeight;

			shaderString += String.format("sum += texture2D(inputImageTexture, blurCoordinates[%d]) * %f;\n", (currentBlurCoordinateIndex * 2) + 1, optimizedWeight);
			shaderString += String.format("sum += texture2D(inputImageTexture, blurCoordinates[%d]) * %f;\n", (currentBlurCoordinateIndex * 2) + 2, optimizedWeight);
		}

		// If the number of required samples exceeds the amount we can pass in via varyings, we have to do dependent texture reads in the fragment shader
		if (trueNumberOfOptimizedOffsets > numberOfOptimizedOffsets)
		{
			shaderString += String.format("highp vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);\n");

			for (int currentOverlowTextureRead = numberOfOptimizedOffsets; currentOverlowTextureRead < trueNumberOfOptimizedOffsets; currentOverlowTextureRead++)
			{
				float firstWeight = standardGaussianWeights[currentOverlowTextureRead * 2 + 1];
				float secondWeight = standardGaussianWeights[currentOverlowTextureRead * 2 + 2];

				float optimizedWeight = firstWeight + secondWeight;
				float optimizedOffset = (firstWeight * (currentOverlowTextureRead * 2 + 1) + secondWeight * (currentOverlowTextureRead * 2 + 2)) / optimizedWeight;

				shaderString += String.format("sum += texture2D(inputImageTexture, blurCoordinates[0] + singleStepOffset * %f) * %f;\n", optimizedOffset, optimizedWeight);
				shaderString += String.format("sum += texture2D(inputImageTexture, blurCoordinates[0] - singleStepOffset * %f) * %f;\n", optimizedOffset, optimizedWeight);
			}
		}

		// Footer
		shaderString += String.format("gl_FragColor = sum;\n" +
				"}\n");

		return shaderString;
	}
}
