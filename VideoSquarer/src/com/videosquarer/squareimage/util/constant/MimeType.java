package com.videosquarer.squareimage.util.constant;

public interface MimeType {

	String IMAGE_TYPE = "image/*";
	String VIDEO_TYPE = "video/*";

}
