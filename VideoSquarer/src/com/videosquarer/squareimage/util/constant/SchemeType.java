package com.videosquarer.squareimage.util.constant;

public interface SchemeType {

	String SCHEME_CONTENT = "content";
	String SCHEME_FILE = "file";

}
