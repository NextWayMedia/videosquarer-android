package com.videosquarer.squareimage.util.constant;

public interface SharedPrefKeys {

	String SHARED_PREF_PICKED_COLOR = "picked_color";
	String SHARED_PREF_PREVIOUSLY_USED_COLORS = "previous_colors";

}
