package com.videosquarer.squareimage.gui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.videosquarer.squareimage.R;

public class UnlockProDialog extends Dialog {

    private IOnButtonClickListener clickListener;

    public UnlockProDialog(Context context, IOnButtonClickListener clickListener) {
        super(context, R.style.FullScreenDialog);
        this.clickListener = clickListener;
    }

    public UnlockProDialog(Context context, int theme) {
        super(context, theme);
    }

    protected UnlockProDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_unlock_pro, null);

        TextView descr = (TextView) view.findViewById(R.id.text);
        descr.setText(String.format(getContext().getString(R.string.pro_description), getContext().getString(R.string.app_name)));
        Button btnUnlock = (Button) view.findViewById(R.id.btnUnlockPro);
        ImageButton btnClose = (ImageButton) view.findViewById(R.id.btnClose);
        btnUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onUnlockClicked();
                }
                dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setContentView(view);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public interface IOnButtonClickListener {
        void onUnlockClicked();
    }
}
