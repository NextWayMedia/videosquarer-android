package com.videosquarer.squareimage.gui;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.actionbarsherlock.widget.ShareActionProvider.OnShareTargetSelectedListener;
import com.flurry.android.FlurryAgent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.InterstitialAd;
import com.google.ads.AdRequest.ErrorCode;
import com.videosquarer.ffmpeg.FfmpegJob;
import com.videosquarer.ffmpeg.FrameJob;
import com.videosquarer.ffmpeg.ProcessRunnable;
import com.videosquarer.ffmpeg.WatermarkJob;
import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.element.SquareImageView;
import com.videosquarer.squareimage.gui.element.colorpicker.Picker.OnColorChangedListener;
import com.videosquarer.squareimage.gui.element.colorpicker.PickerDialog;
import com.videosquarer.squareimage.gui.element.predefinedcolorsdialog.ColorPickerPredefindedAdapter;
import com.videosquarer.squareimage.gui.element.predefinedcolorsdialog.ColorPickerPredefinedDialog;
import com.videosquarer.squareimage.gui.element.predefinedcolorsdialog.OnPredefinedColorSelectedListener;
import com.videosquarer.squareimage.gui.highencoder.AudioGrabber;
import com.videosquarer.squareimage.gui.highencoder.EncodeThread;
import com.videosquarer.squareimage.gui.highencoder.EncodeThread.EncodeException;
import com.videosquarer.squareimage.gui.highencoder.GLPlayerScreen;
import com.videosquarer.squareimage.gui.highencoder.VideoGrabber;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.BITMAPINFOHEADER;
import com.videosquarer.squareimage.util.BitmapWrapper;
import com.videosquarer.squareimage.util.FastBlur;
import com.videosquarer.squareimage.util.MediaFrame;
import com.videosquarer.squareimage.util.MovieWriterParam;
import com.videosquarer.squareimage.util.VIDEOINFOHEADER;
import com.videosquarer.squareimage.util.WAVEFORMATEX;
import com.videosquarer.squareimage.util.constant.MimeType;
import com.videosquarer.squareimage.util.constant.SchemeType;
import com.videosquarer.squareimage.util.constant.SharedPrefKeys;
import com.videosquarer.squareimage.util.task.BitmapLoadTask;
import com.videosquarer.squareimage.util.task.CreateSquareFullSizeImageTask;


public class DisplayingActivity extends UnlockProActivity {
	private InterstitialAd interstitial;
	final private static String INTERSTITIAL_AD_PUB_ID = "ca-app-pub-1561400000155036/1980395700";

	public class MediaScannerReceiver extends BroadcastReceiver {  
		private boolean restart = false;
		@Override  
		public void onReceive(Context context, Intent intent) {  
			String action = intent.getAction();
			if(action.equals("android.intent.action.MEDIA_SCANNER_FINISHED")) {  
				//update finish
			} 
		}  

		public void setRestart(boolean r) {
			this.restart = r;
		}
	}  

	private static final String TAG = "Displaying activity";

	private static final int BLUR_RADIUS_MAX = 20;
	private static int blurRadius = 10;
	private SquareImageView squareImageView;
	private PickerDialog colorPickerDialog;
	private ColorPickerPredefinedDialog colorPickDialog;
	private ShareActionProvider shareActionProvider;

	private String originalImageFilePath;
	private String processedImageFilePath;	
	private int imageInSampleSize;

	private LinearLayout rootLayout;

	private Dialog imageSettingsDialog;
	private Dialog echoBGDialog;

	private RelativeLayout containerLayout;

	private boolean isImageProcess;
	private String originalVideoFilePath;
	private String processedVideoFilePath;
	private String ffmpegPath;
	private String watermarkFilePath;
	private String watermarkVerticalFilePath;
	private String watermarkVerticalInvertedFilePath;
	private String processedFolderPath;
	private int squareSize;

	private FfmpegJob job;
	public static Intent shareIntent;

	private String progressMessage;

	/* High Encoder Variables */
	public Thread 	m_hVideoPlayThread;
	public Thread 	m_hAudioPlayThread;

	public double 	m_nCurrentVideoTime;
	public double 	m_nCurrentAudioTime;
	public boolean 	m_bProcessVideo;
	public boolean 	m_bProcessAudio;

	public int colorFormat = -1;
	public String encodeCodecName = "video/avc";
	private VideoGrabber videoClip;
	private AudioGrabber audioClip;
	private int videoWidth;
	private int videoHeight;
	public final static int EXPORTED_AUDIO_RATE = 44100;
	public final static int EXPORTED_AUDIO_BITS = 16;
	public final static int EXPORTED_AUDIO_CHANNELS = 2;

	ProgressDialog progressDialog;

	public AudioTrack mAudioTrack = null;

	public WAVEFORMATEX m_AudioFormat = new WAVEFORMATEX();
	public VIDEOINFOHEADER m_VideoFormat = new VIDEOINFOHEADER();
	public MovieWriterParam m_ExportParam = new MovieWriterParam();

	public GLPlayerScreen m_pPlayerScreen;

	private int m_nExportTic = 0;
	private int m_nPostSkip = 10;
	private boolean m_bExportDrawStart = false;

	private EncodeThread	m_ExportThread = null;

	private static final int HEAD_SKIP_FRAME_COUNT = 0;
	private int 	m_bSkipFrameCnt;

	public int		m_nOutAudioBuffSize = 0;

	private long exportDuration = 0;

	private boolean m_bExportSuccess = false;
	private boolean m_bExportError = false;
	public static boolean isBlur = false;
	public static Bitmap blurBitmap = null;

	int adsShowIndex = 0;
	final private int ADS_SHARE_ALL = 1;
	final private int ADS_SAVE = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		initUI();
		
	    // Create the interstitial.
	    interstitial = new InterstitialAd(this, INTERSTITIAL_AD_PUB_ID);

	    interstitial.setAdListener(new AdListener() {
            
			@Override
			public void onDismissScreen(Ad arg0) {
				// TODO Auto-generated method stub
			    // Create ad request.
			    AdRequest adRequest = new AdRequest();

			    // Begin loading your interstitial.
			    interstitial.loadAd(adRequest);

			    if(adsShowIndex == ADS_SHARE_ALL)
			    {
					saveImage(shareIntent);
			    }
			    else if(adsShowIndex == ADS_SAVE)
			    {
			    	saveImage(null);
			    }
			}

			@Override
			public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onLeaveApplication(Ad arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPresentScreen(Ad arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onReceiveAd(Ad arg0) {
				// TODO Auto-generated method stub
				
			}
        });

	    // Create ad request.
	    AdRequest adRequest = new AdRequest();

	    // Begin loading your interstitial.
	    interstitial.loadAd(adRequest);

	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, AppUtil.getStringRes(R.string.flurry_api_key));
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}

	private void setProcessedImageFilePath()
	{
		String fileName = new File(originalImageFilePath).getName();
		int lastIndexOfDot = fileName.lastIndexOf('.');
		if (lastIndexOfDot == -1) {
			AppUtil.showLongToast(R.string.wrong_filename);
			finish();
			return;
		}

		String fileExtension = fileName.substring(lastIndexOfDot);		
		fileName = fileName.substring(0, lastIndexOfDot);

		//		int lastIndexOfSlash = originalImageFilePath.lastIndexOf('/');		
		//		processedFolderPath = originalImageFilePath.substring(0, lastIndexOfSlash);		
		//		processedImageFilePath = processedFolderPath
		//				+ "/PhotoSquarer_" + System.currentTimeMillis() + fileExtension;

		processedFolderPath = AppUtil
				.getExternalPublicDirectory(Environment.DIRECTORY_PICTURES);		
		processedImageFilePath = processedFolderPath
				+ "PhotoSquarer_" + System.currentTimeMillis() + fileExtension;			

		File path = new File(processedFolderPath);
		if(!path.isDirectory()) 
		{
			path.mkdirs();
		}		
	}

	private void setProcessedVideoFilePath()
	{
//		int index = originalVideoFilePath.lastIndexOf("\\");
//		String fileName = originalVideoFilePath.substring(index + 1);
//		//String fileName = new File(originalVideoFilePath).getName();
//		int lastIndexOfDot = fileName.lastIndexOf('.');
//		if (lastIndexOfDot == -1) {
//			AppUtil.showLongToast(R.string.wrong_filename);
//			finish();
//			return;
//		}
//
//		fileName = fileName.substring(0, lastIndexOfDot);

		//		int lastIndexOfSlash = originalVideoFilePath.lastIndexOf('/');		
		//		processedFolderPath = originalVideoFilePath.substring(0, lastIndexOfSlash);		
		//		processedVideoFilePath = processedFolderPath
		//				+ "/MediaSquarer_" + System.currentTimeMillis() + fileExtension;s

		String fileExtension = ".mp4";

		processedFolderPath = AppUtil
				.getExternalPublicDirectory(Environment.DIRECTORY_MOVIES);		
		processedVideoFilePath = processedFolderPath
				+ "VideoSquarer_" + System.currentTimeMillis() + fileExtension;			

		File path = new File(processedFolderPath);
		if(!path.isDirectory()) 
		{
			path.mkdirs();
		}
	}

	private void initUI() {

		containerLayout = (RelativeLayout) findViewById(R.id.act_displaying_rl_container);
		rootLayout = (LinearLayout) findViewById(R.id.act_displaying_ll_root_layout);
		squareImageView = (SquareImageView) findViewById(R.id.act_displaying_siv_square);
		AppUtil.putSharedPref(SharedPrefKeys.SHARED_PREF_PICKED_COLOR, 0);

		if(AppUtil.isSystemHighVersion())
		{
			m_pPlayerScreen = (GLPlayerScreen)findViewById(R.id.GLExportScreen);
			m_pPlayerScreen.setVisibility(View.VISIBLE);
		}

		originalImageFilePath = grabFilePathFromIntent();
		if (originalImageFilePath == null) {
			return;
		}

		setProcessedImageFilePath();

		squareSize = AppUtil.getDisplayMetrics().widthPixels -
				(containerLayout.getPaddingLeft() + containerLayout.getPaddingRight());

		CopyWaterMarkFile();
		if (ffmpegPath != null)
		{
			job = new FfmpegJob(ffmpegPath, watermarkFilePath);
			LoadVideoJob();
		}

		new BitmapLoadTask(this, squareSize).execute();
	}

	private void CopyWaterMarkFile()
	{			
		Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.watermark);
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		File file = new File(getCacheDir(), "watermark.png");
		try
		{
			OutputStream outStream = new FileOutputStream(file);
			bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		bm.recycle();
		bm = BitmapFactory.decodeResource(getResources(), R.drawable.watermark_vertical);
		file = new File(getCacheDir(), "watermark_vertical.png");
		try
		{
			OutputStream outStream = new FileOutputStream(file);
			bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		bm.recycle();
		bm = BitmapFactory.decodeResource(getResources(), R.drawable.watermark_vertical_inverted);
		file = new File(getCacheDir(), "watermark_vertical_inverted.png");
		try
		{
			OutputStream outStream = new FileOutputStream(file);
			bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		watermarkFilePath = getCacheDir() + "/watermark.png";
		watermarkVerticalFilePath = getCacheDir() + "/watermark_vertical.png";
		watermarkVerticalInvertedFilePath = getCacheDir() + "/watermark_vertical_inverted.png";
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.menu_activity_display, menu);
		MenuItem menuItem = menu.findItem(R.id.menu_act_display_share_image);
		shareActionProvider = (ShareActionProvider) menuItem.getActionProvider();
		// We need this to prevent disappearing fifth action in mdpi 2.2 devices
		shareActionProvider.setShareHistoryFileName(null);
		shareActionProvider.setOnShareTargetSelectedListener(new OnShareTargetSelectedListener() {

			@Override
			public boolean onShareTargetSelected(ShareActionProvider source, Intent intent) {
				if(interstitial.isReady())
				{
					adsShowIndex = ADS_SHARE_ALL;
					interstitial.show();
				}
				else
				{
					shareIntent = intent;
					saveImage(intent);
				}
				return true;
			}
		});
		setUpShareAction();

		return true;
	}

	public void setUpShareAction() {
		if (isImageProcess)
		{
			if (processedImageFilePath == null) {
				return;
			}
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType(MimeType.IMAGE_TYPE);
			Uri uri = Uri.fromFile(new File(processedImageFilePath));
			shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
			shareActionProvider.setShareIntent(shareIntent);
		}
		else
		{
			if (processedVideoFilePath == null) {
				return;
			}

			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType(MimeType.VIDEO_TYPE);
			Uri uri = Uri.fromFile(new File(processedVideoFilePath));					
			shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
			shareActionProvider.setShareIntent(shareIntent);
		}
	}

	private String getThumbnail(String videoFilePath)
	{		
		Bitmap fullScreenThumb = ThumbnailUtils.createVideoThumbnail(videoFilePath, MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);

		String thumbImageFilePath = processedFolderPath + "/thumb.jpg";
		File thumbImageFile = new File(thumbImageFilePath);		

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		fullScreenThumb.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] byteArray = stream.toByteArray();	
		try
		{
			OutputStream out= new FileOutputStream(thumbImageFile.getPath());
			out.write(byteArray);
			out.close();
		}
		catch (Exception e)
		{            
		}

		return thumbImageFilePath;
	}

	private String grabFilePathFromIntent() 
	{		
		String extraFilePath = getIntent().getStringExtra(MainActivity.EXTRA_FILE_PATH);

		if (!AppUtil.isValidText(extraFilePath)) {
			Uri imageUri = (Uri) getIntent().getExtras().get(Intent.EXTRA_STREAM);
			if (imageUri != null) {
				String scheme = imageUri.getScheme();

				if (scheme.equals(SchemeType.SCHEME_CONTENT)) {
					String[] filePathColumn = { MediaStore.Images.Media.DATA };
					Cursor cursor = getContentResolver()
							.query(imageUri, filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					extraFilePath = cursor.getString(columnIndex);
					cursor.close();
				} else if (scheme.equals(SchemeType.SCHEME_FILE)) {
					extraFilePath = imageUri.getPath();
				}
			}
		}

		if (extraFilePath == null) {
			AppUtil.showLongToast(R.string.cant_load_image);
			finish();
			return null;
		}

		ffmpegPath = getIntent().getStringExtra(MainActivity.FFMPEG_PATH);
		if (ffmpegPath == null)
		{
			isImageProcess = true;			
		}
		else
		{
			isImageProcess = false;
			originalVideoFilePath = extraFilePath;
			setProcessedVideoFilePath();

			extraFilePath = getThumbnail(extraFilePath);								
		}
		return extraFilePath;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_act_display_pick_color:
			showPredefinedColorDialog();
			return true;
		case R.id.menu_act_display_rotate_image:
			if (isImageProcess == false)
			{
				int nTrans = job.nTranspose;
				nTrans = nTrans + 1;
				if(nTrans > 3) nTrans = 0;
				job.nTranspose = nTrans;
			}

			squareImageView.rotateImage();
			return true;
		case R.id.menu_act_display_save_image:
			if(interstitial.isReady())
			{
				adsShowIndex = ADS_SAVE;
				interstitial.show();
			}
			else
				saveImage(null);
			return true;
		case R.id.menu_act_display_border_size:
			showBorderSizeDialog();
			return true;
		case android.R.id.home:
			onBackPressed();
			return true;
		case R.id.menu_act_display_echo_background:
			showEchoBGDialog();
			return true;
		default:
			Log.w(TAG, "Undefined menu item");
		}

		return super.onMenuItemSelected(featureId, item);
	}

	private Map<String, Integer> getVideoDimension(String path) {
		int width = 0, height = 0;
		try {
			MediaPlayer mp = new MediaPlayer();
			mp.setDataSource(path);
			mp.prepare();
			width = mp.getVideoWidth();
			height = mp.getVideoHeight();
		} catch (IOException e) {

		}
		Map<String, Integer> ret = new HashMap<String, Integer>();
		ret.put("height", height);
		ret.put("width", width);
		return ret;
	}

	private void saveImage(Intent intent) 
	{					

		if (isImageProcess == true)
		{
			new CreateSquareFullSizeImageTask(this, intent, isPro).execute();
		}
		else
		{
			if(AppUtil.isSystemHighVersion())
			{
				nativeInitFFMpeg();

				m_ExportParam.strFileFormat = "mp4";
				m_ExportParam.strFileName = processedVideoFilePath;

				exportDuration = VideoGrabber.getDuration(originalVideoFilePath) / 1000;
				inspectDeviceColorFormat();

				Map<String, Integer> videoDimen = getVideoDimension(originalVideoFilePath);
				int width = videoDimen.get("width");
				int height = videoDimen.get("height");

				videoWidth = videoHeight = 640;
				int ratioSize = 0;
				if(width > height)
					ratioSize = width;
				else
					ratioSize = height;

				videoClip = new VideoGrabber();
				videoClip.LoadMedia(originalVideoFilePath, 0, 0, true, true);
				int scaleSize = squareSize;
				Rect videoRect = squareImageView.getVideoRect();
				int frameWidth = videoRect.right - videoRect.left;
				int frameHeight = videoRect.bottom - videoRect.top;

				Rect cropRect = new Rect(videoRect);
				cropRect.bottom = cropRect.bottom * height / scaleSize;
				cropRect.top = cropRect.top * height / scaleSize;
				cropRect.left = cropRect.left * width / scaleSize;
				cropRect.right = cropRect.right * width / scaleSize;

				if(job.nTranspose == 1 || job.nTranspose == 3)
				{
					videoClip.setVideoInfo(height, width, height, width, 
							0, 0, videoRect.left * videoWidth / scaleSize, videoRect.top * videoWidth / scaleSize, videoWidth * frameWidth / scaleSize);

				}
				else
					videoClip.setVideoInfo(width, height, width, height, 
							0, 0, videoRect.left * videoWidth / scaleSize, videoRect.top * videoWidth / scaleSize, videoWidth * frameWidth / scaleSize);
				videoClip.setVideoRotation(job.nTranspose * 90);
				audioClip = new AudioGrabber();
				audioClip.LoadMedia(originalVideoFilePath, 0, true);

				Init(videoWidth, videoHeight);

				Bitmap backBitmap = squareImageView.getBackgroundBitmap(this, squareSize);
				if(backBitmap != null)
				{
					nativeLoadImage(backBitmap, backBitmap.getWidth(), backBitmap.getHeight(), 
							backBitmap.getWidth(), backBitmap.getHeight(), 0, 0, 0, 0, videoWidth);
				}

				Bitmap maskBitmap = squareImageView.getMaskBitmap(this, squareSize);
				if(maskBitmap != null)
				{
					nativeSetMaskImage(maskBitmap, maskBitmap.getWidth(), maskBitmap.getHeight());
					maskBitmap.recycle();
				}

				if(isBlur)
				{
					nativeSetBlurEnable(1);
					FastBlur.setBlurRadius(blurRadius);
					nativeSetBlurValues(blurRadius, FastBlur.blurVertexShaderString, FastBlur.blurFragmentShaderString);
				}
				else
					nativeSetBlurEnable(0);

				Export(m_ExportParam);

			}
			else
			{
				final ProgressDialog progressDialog = ProgressDialog.show(this, "Rendering Video", "Please wait.", true, false);

				new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... arg0) {


						// making square video
						new ProcessRunnable(job.RenderingVideo()){}.run();
						if (!isPro) {
							WatermarkJob watermarkJob = getWatermarkJob();
							Bitmap watermark = AppUtil.getBitmapWithoutMemoryError(watermarkJob.getWatermarkPath());
							if (watermark != null) {
								if (watermark.getWidth() >= watermark.getHeight() && watermark.getWidth() > squareImageView.getScaledSize() / 3) {
									int newWidth = squareImageView.getScaledSize() / 3;
									int newHeight = (int)((float)newWidth / watermark.getWidth() * watermark.getHeight());
									Bitmap scaledWatermark = BitmapWrapper.createScaledBitmap(watermark, newWidth, newHeight, true);
									watermark.recycle();
									System.gc();
									watermark = scaledWatermark;
								} else if (watermark.getHeight() >= watermark.getWidth() && watermark.getHeight() > squareImageView.getScaledSize() / 3) {
									int newHeight = squareImageView.getScaledSize() / 3;
									int newWidth = (int)((float)newHeight / watermark.getHeight() * watermark.getWidth());
									Bitmap scaledWatermark = Bitmap.createScaledBitmap(watermark, newWidth, newHeight, true);
									watermark.recycle();
									System.gc();
									watermark = scaledWatermark;
								}
								try {
									watermark.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(watermarkJob.getWatermarkPath()));
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								}
							}
							// saving video file to temp file
							File bkpFile = new File(processedVideoFilePath + ".bak");
							AppUtil.copyFile(new File(processedVideoFilePath), bkpFile);
							// applying watermark
							new ProcessRunnable(watermarkJob.getWatermarkTask()){}.run();
							// removing backup file
							if (bkpFile.exists()) {
								bkpFile.delete();
							}
						}

						return null;
					}		

					@Override
					protected void onPostExecute(Void result) {
						progressDialog.dismiss();					
						//					if (result) {
						if (DisplayingActivity.shareIntent != null) {
							startActivity(DisplayingActivity.shareIntent);
							DisplayingActivity.shareIntent = null;
						} else {
							Toast.makeText(DisplayingActivity.this, AppUtil.getStringRes(R.string.saved_to) + " " + processedVideoFilePath , Toast.LENGTH_LONG).show();
						}
						//					}

						// refresh video gallery	
						IntentFilter intentfilter = new IntentFilter(Intent.ACTION_MEDIA_SCANNER_STARTED);
						intentfilter.addDataScheme("file");
						MediaScannerReceiver scanSdReceiver = new MediaScannerReceiver();
						scanSdReceiver.setRestart(false);
						registerReceiver(scanSdReceiver, intentfilter);
						sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + processedFolderPath)));

						String thumbImageFilePath = processedFolderPath + "thumb.jpg";
						File thumbImageFile = new File(thumbImageFilePath);		
						if (thumbImageFile.exists())
						{
							thumbImageFile.delete();
						}
					}

				}.execute();	
			}
		}
	}

	/**
	 * Get configured job for adding watermark
	 * @return configured watermark job
	 */
	private WatermarkJob getWatermarkJob() {
		// extracting original image from video for determining video orientation without rotation metadata tag
		new ProcessRunnable(FrameJob.getFrameTask(ffmpegPath, originalVideoFilePath, processedFolderPath + "th.jpg")).run();
		boolean isHorizontalOriginal = false;
		File th = new File(processedFolderPath + "th.jpg");
		if (th.exists()) {
			Bitmap thumb = AppUtil.getBitmapWithoutMemoryError(th.getAbsolutePath());
			isHorizontalOriginal = thumb.getWidth() >= thumb.getHeight();
			thumb.recycle();
			th.delete();
		}

		// getting information about video orientation with rotation metadata tag
		Bitmap thumb = AppUtil.getBitmapWithoutMemoryError(originalImageFilePath);
		int width = thumb.getWidth();
		int height = thumb.getHeight();
		thumb.recycle();
		boolean isHorizontal = width >= height; // is video horizontal with rotation metadata tag
		boolean isHorizontalAfterRotation;      // is video horizontal after user manipulated with it
		if (job.nTranspose % 2 == 0) {
			isHorizontalAfterRotation = isHorizontal;
		} else {
			isHorizontalAfterRotation = !isHorizontal;
		}

		WatermarkJob watermarkJob = new WatermarkJob();
		watermarkJob.setFfmpegPath(ffmpegPath);
		watermarkJob.setInPath(processedVideoFilePath + ".bak");    // backup for squared video
		watermarkJob.setOutPath(processedVideoFilePath);            // final video file with watermark

		watermarkJob.setWatermarkPath(watermarkFilePath);
		watermarkJob.setWatermarkY(squareImageView.getWatermarkY());
		watermarkJob.setWatermarkX(squareImageView.getWatermarkX());

		// video rotated by user
		//		if (isHorizontalAfterRotation) {
		//			if (isHorizontalOriginal) {
		//				if (isHorizontal) {
		//					// S3 horizontal w/o rotation
		//					watermarkJob.setWatermarkPath(watermarkFilePath);
		//					watermarkJob.setWatermarkX(squareImageView.getWatermarkY());
		//					watermarkJob.setWatermarkY(squareImageView.getWatermarkX());
		//				} else {
		//					// Nexus vertical video w/ rotation
		//					// S3 vertical video w/ rotation
		//					watermarkJob.setWatermarkPath(watermarkVerticalFilePath);
		//					watermarkJob.setWatermarkX(0);
		//					watermarkJob.setWatermarkY(squareImageView.getWatermarkY());
		//				}
		//			} else {
		//				// QT sample w/ rotation
		//				watermarkJob.setWatermarkPath(watermarkFilePath);
		//				watermarkJob.setWatermarkX(squareImageView.getWatermarkY());
		//				watermarkJob.setWatermarkY(squareImageView.getWatermarkX());
		//			}
		//
		//		} else {
		//			if (isHorizontal) {
		//				// S3 horizontal w/ rotation
		//				watermarkJob.setWatermarkPath(watermarkVerticalInvertedFilePath);
		//				watermarkJob.setWatermarkX(squareImageView.getWatermarkY());
		//				watermarkJob.setWatermarkY(squareImageView.getWatermarkX());
		//			} else {
		//				if (isHorizontalOriginal) {
		//					// Nexus vertical video
		//					// S3 rotated video
		//					watermarkJob.setWatermarkPath(watermarkFilePath);
		//					watermarkJob.setWatermarkY(squareImageView.getWatermarkY());
		//					watermarkJob.setWatermarkX(squareImageView.getScaledSize() - squareImageView.getWatermarkX() - squareImageView.getWatermarkWidth());
		//				} else {
		//					// QT sample w/o rotation
		//					watermarkJob.setWatermarkPath(watermarkVerticalInvertedFilePath);
		//					watermarkJob.setWatermarkX(squareImageView.getWatermarkY());
		//					watermarkJob.setWatermarkY(squareImageView.getScaledSize() - squareImageView.getWatermarkWidth());
		//				}
		//			}
		//		}
		return watermarkJob;
	}

	public void showColorPickDialog(ColorPickerPredefindedAdapter adapter) {
		if (colorPickerDialog == null) {
			colorPickerDialog = new PickerDialog(this, adapter,
					new OnColorChangedListener() {

				@Override
				public void colorChanged(final int color) {
					squareImageView.setBordersColor(color);

					AppUtil.putSharedPref(
							SharedPrefKeys.SHARED_PREF_PICKED_COLOR, color);
				}

			}, AppUtil.getSharedPrefInteger(SharedPrefKeys.SHARED_PREF_PICKED_COLOR));
		}

		colorPickerDialog.show();
	}

	private void showPredefinedColorDialog() {
		if (colorPickDialog == null) {
			colorPickDialog = new ColorPickerPredefinedDialog(this,
					new OnPredefinedColorSelectedListener() {

				@Override
				public void onColorSelected(int color) {

					if (isImageProcess == false)
					{
						String strColor = Integer.toHexString(color);
						if(color < 0) strColor = strColor.substring(2);
						if(!AppUtil.isSystemHighVersion())
							job.borderColor = strColor;
					}

					squareImageView.setBordersColor(color);

					AppUtil.putSharedPref(
							SharedPrefKeys.SHARED_PREF_PICKED_COLOR, color);
				}
			});
		}

		colorPickDialog.show();
	}

	private void showBorderSizeDialog() {
		if (imageSettingsDialog == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			View dialogView = LayoutInflater.from(this)
					.inflate(R.layout.dialog_image_settings, null);
			builder.setView(dialogView);
			builder.setTitle(R.string.change_border_action);
			builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {					
				}
			});

			SeekBar borderSizeSeekBar = (SeekBar) dialogView
					.findViewById(R.id.dlg_image_settings_sb_border_size);
			borderSizeSeekBar.setProgress(100);
			borderSizeSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {									
					progress = AppUtil.convertToPixels(100) - AppUtil.convertToPixels(progress);
					SquareImageView.smallestBorderSize = progress;
					squareImageView.invalidate();

					if (isImageProcess == false && !AppUtil.isSystemHighVersion())
					{
						//						job.nBorderSize = progress;
						job.nBorderSize = squareImageView.getScaledSize();
					}					
				}
			});

			imageSettingsDialog = builder.create();
		}
		imageSettingsDialog.show();
	}


	private void showEchoBGDialog() {
		if (echoBGDialog == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			View dialogView = LayoutInflater.from(this)
					.inflate(R.layout.dialog_echobg_settings, null);
			builder.setView(dialogView);
			builder.setTitle(R.string.change_echobg_action);
			builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {					
				}
			});

			final SeekBar echoBGSeekBar = (SeekBar) dialogView
					.findViewById(R.id.dlg_echobg_seekbar);
			echoBGSeekBar.setProgress(blurRadius);
			echoBGSeekBar.setMax(BLUR_RADIUS_MAX);

			final CheckBox blurCheckBtn = (CheckBox)dialogView.findViewById(R.id.blurCheckBoxButton);
			if(isBlur)
				echoBGSeekBar.setEnabled(true);
			else
				echoBGSeekBar.setEnabled(false);
			blurCheckBtn.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(!AppUtil.isSystemHighVersion() && !isImageProcess)
					{
						AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(DisplayingActivity.this);

						dlgAlert.setMessage("This is possible on Android 4.1.2 later!");
						dlgAlert.setTitle("Warning");
						dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() 
						{                   
							@Override
							public void onClick(DialogInterface arg0, int arg1) 
							{
							}//end onClick()
						}).create();                

						dlgAlert.create().show();
					}
					else
					{
						if(DisplayingActivity.isBuyBlur)
						{
							isBlur = !isBlur;
							if(isBlur)
								echoBGSeekBar.setEnabled(true);
							else
								echoBGSeekBar.setEnabled(false);
							applyBlurEffect();
						}
						else
						{
							blurCheckBtn.setChecked(false);
							showBuyBlurDialog();
						}
					}
				}
			});

			echoBGSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {	
					if(progress > BLUR_RADIUS_MAX)
						progress = BLUR_RADIUS_MAX;

					if(blurRadius != progress)
					{
						blurRadius = progress;
						applyBlurEffect();
					}

				}
			});

			imageSettingsDialog = builder.create();
		}
		imageSettingsDialog.show();
	}

	public void applyBlurEffect()
	{
		if(isBlur)
		{
			if(blurBitmap != null && !blurBitmap.isRecycled())
				blurBitmap.recycle();

			Bitmap rb = squareImageView.getBitmap();
			if(rb == null)
				return;
			int width = rb.getWidth(), height = rb.getHeight();
			int small_size = 0;
			Bitmap smallBitmap = null;
			if(width > height)
			{
				small_size = height * 8 / 10;
			}
			else if(height > width)
			{
				small_size = width * 8 / 10;
			}
			else
			{
				small_size = width * 8 / 10;
			}

			smallBitmap = Bitmap.createBitmap(rb, (width - small_size) / 2, (height - small_size) / 2, small_size, small_size);

			Bitmap tempBitmap = smallBitmap.copy(Bitmap.Config.ARGB_8888, true);
			smallBitmap.recycle();
			//functionToBlur(rb, bitmapOut, 2);
			Bitmap dstBitmap = null;
			if(blurRadius > 0)
			{
				dstBitmap = FastBlur.fastblur(getApplicationContext(), tempBitmap, blurRadius);
				tempBitmap.recycle();
			}
			else
				dstBitmap = tempBitmap;

			System.gc();
			blurBitmap = dstBitmap;
		}
		squareImageView.invalidate();		
	}

	public Dialog getPredefinedColorPickDialog() {
		return colorPickDialog;
	}

	public String getFileName() {
		return new File(processedImageFilePath).getName();
	}

	public String getOriginalImageFilePath() {
		return originalImageFilePath;
	}

	public String getProcessedImageFilePath() {
		return processedImageFilePath;
	}

	public int getImageInSampleSize() {
		return imageInSampleSize;
	}

	public void setImageInSampleSize(int inSampleSize) {
		this.imageInSampleSize = inSampleSize;
	}

	public SquareImageView getSquareImageView() {
		return squareImageView;
	}

	public void setProcessedImageFilePath(String processedImageFilePath) {
		this.processedImageFilePath = processedImageFilePath;
	}

	public String getProcessedFolderPath()
	{
		return processedFolderPath;		
	}

	private void LoadVideoJob()
	{
		job.originalVideoFilePath = originalVideoFilePath;
		job.processedVideoFilePath = processedVideoFilePath;
		job.squareSize = squareSize;
		job.ffmpegPath = ffmpegPath;
		job.nTranspose = 0;
		job.borderColor = "000000";
		//		job.nBorderSize = 100;
		job.nBorderSize = squareSize;
	}

	@Override
	public void onDestroy() {

		super.onDestroy();

		if(blurBitmap != null && !blurBitmap.isRecycled())
		{
			blurBitmap.recycle();
			System.gc();
			blurBitmap = null;
		}
		isBlur = false;
	}

	public boolean Init(int width, int height) {

		m_VideoFormat.bmiHeader.biSize = BITMAPINFOHEADER.size();
		m_VideoFormat.bmiHeader.biCompression = 0;// BI_RGB;
		m_VideoFormat.bmiHeader.biBitCount = 32;
		m_VideoFormat.bmiHeader.biPlanes = 1;
		m_VideoFormat.bmiHeader.biWidth = width;
		m_VideoFormat.bmiHeader.biHeight = height;
		m_VideoFormat.bmiHeader.biSizeImage = (int) (m_VideoFormat.bmiHeader.biBitCount * width * height / 8);
		m_VideoFormat.dwBitErrorRate = 15;
		m_ExportParam.VideoParam.vih = m_VideoFormat;

		m_AudioFormat.nSamplesPerSec = EXPORTED_AUDIO_RATE;
		m_AudioFormat.wBitsPerSample = EXPORTED_AUDIO_BITS;
		m_AudioFormat.nChannels = EXPORTED_AUDIO_CHANNELS;
		m_AudioFormat.nBlockAlign = (short) (m_AudioFormat.wBitsPerSample * m_AudioFormat.nChannels / 8);
		m_AudioFormat.nAvgBytesPerSec = m_AudioFormat.nBlockAlign * m_AudioFormat.nSamplesPerSec;
		m_ExportParam.AudioParam.wfx = m_AudioFormat;
		m_ExportParam.AudioParam.bitrate = m_AudioFormat.nAvgBytesPerSec * 8;

		m_VideoFormat.rcSource.right = m_VideoFormat.rcTarget.right = width;
		m_VideoFormat.rcSource.bottom = m_VideoFormat.rcTarget.bottom = height;

		m_AudioFormat.cbSize = 0;

		return true;
	}

	public void Release() {
		closeVideos();
		closeAudios();


		if(m_ExportThread != null)
		{
			m_ExportThread.AbortThread();
			m_ExportThread.closeEncoder();
		}
	}

	// get encode color format,
	public void inspectDeviceColorFormat() {
		Integer[] colorInfo = new Integer[2];
		encodeCodecName = AppUtil.getDeviceColorFormat(colorInfo);

		colorFormat = colorInfo[0];
		colorInfo = null;
	}


	private void showErrorAndFinish(String message)
	{
		AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		dlgAlert.setMessage(message);
		dlgAlert.setTitle("Error");
		dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() 
		{                   
			@Override
			public void onClick(DialogInterface arg0, int arg1) 
			{
				Release();
				finish();
			}//end onClick()
		}).create();                

		dlgAlert.create().show();

	}

	public int Export(MovieWriterParam param) {

		m_ExportThread = null;
		Log.e("Export", "Start Export");

		if (initBasicPlayer() < 0) {
			return -1;
		}

		m_nExportTic = 0;
		m_bExportDrawStart = false;

		m_bSkipFrameCnt = 0;

		m_nCurrentVideoTime = 0;
		m_nCurrentAudioTime = 0;
		// setting up movie params
		int framerate = nativeGetFrameRate(videoClip.m_nMediaIndex);
		setStreamFrameRate(framerate);
		SetFPS(framerate);
		param.AudioParam.bitrate = 128;

		int width = videoWidth;
		int height = videoHeight;
		int abitrate = param.AudioParam.bitrate;

		int vbitrate;

		// control bitrate for quality & velocity

		if (width > 1200){ 	//1280*720
			m_nPostSkip = 5;
			//vbitrate = 8000;
			vbitrate = 2500;
		}else if (width > 900){	//960*720
			m_nPostSkip = 10;
			//vbitrate = 5500;
			vbitrate = 1600;
		}else if (width > 600){	//640*480
			m_nPostSkip = 15;
			//vbitrate = 3000;
			vbitrate = 750;
		}else if (width > 400){	//480*360
			m_nPostSkip = 15;
			//vbitrate = 1200;
			vbitrate = 400;
		}else{				//400*300
			m_nPostSkip = 20;
			//vbitrate = 800;
			vbitrate = 350;
		}

		progressDialog = new ProgressDialog(this); 
		//		setProgressDialogTitle("2 of 2 Processing...");
		//		setProgressDialogMessage("Preparing video. Can take 5-10 minutes...\n"
		//				+ "If you receive issues. Please click 'Videos Issues?' button on previous screen.");
		//		setNotificationIcon(R.drawable.ic_launcher);
		//		setNotificationMessage("2 of 2 Preparing video");
		//		setNotificationTitle("RepostWhiz");

		progressDialog.setTitle("Processing...");
		progressDialog.setMessage("Preparing video. Can take a few seconds...\n");
		progressDialog.setMax(100);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.show();

		m_bExportSuccess = false;
		m_bExportError = false;

		vbitrate = vbitrate * 1000;		
		abitrate = abitrate * 1000;

		try{
			if ((m_nOutAudioBuffSize = createMovieFile(param.strFileName, param.strFileFormat, height, width, vbitrate, abitrate)) < 0){
				progressDialog.cancel();
				showErrorAndFinish("Fail to create the output video file (" + param.strFileName + ")");
				return -1;
			}

			m_ExportThread = new EncodeThread();
			if (!m_ExportThread.createEncoder(width, height, vbitrate, framerate, colorFormat, encodeCodecName)){
				progressDialog.cancel();
				showErrorAndFinish("Fail to export video file (" + param.strFileName + ")");

				m_ExportThread = null;
				return -1;
			}
		}
		catch(Exception e)
		{
			m_ExportThread = null;
			progressDialog.dismiss();

			return -1;
		}
		m_ExportThread.start();

		m_ExportParam.VideoParam = param.VideoParam;
		m_ExportParam.AudioParam = param.AudioParam;

		m_hVideoPlayThread = new Thread() {
			public void run() {
				Log.d("EncodeManager", "export m_hVideoPlayThread start ");

				long nNextSleepTime;
				double nDefaultSleepTime = 1000.0 / (double) GetFPS();
				int nCounter = 0;

				while (true) 
				{
					try {
						sleep(10);
					} catch (InterruptedException e) {}

					nNextSleepTime = Calendar.getInstance().getTimeInMillis();
					m_bProcessVideo = true;

					if(m_bExportSuccess)
					{
						if(m_bProcessAudio)
						{
							try {
								sleep(100);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							continue;
						}
						else
							break;

					}
					_ProcForVideoPlaying();
					Log.d("EncodeManager", "m_hVideoPlayThread step 1");

					updateVideos(m_nCurrentVideoTime);
					Log.d("EncodeManager", "m_hVideoPlayThread step 2");

					if (m_pPlayerScreen != null)
					{
						OnRenderExportFrame();
					}
					//Log.d("EncodeManager", "m_hVideoPlayThread step 3");

					try{
						OnEncodeBuffer();				
					}catch(EncodeException e){
						m_bExportError = true;
					}

					if (m_bExportError){
						break;
					}

					if ( nCounter ++ >= m_nPostSkip) {
						nCounter = 0;
						if(progressDialog != null)
							progressDialog.setProgress((int) ((m_nCurrentVideoTime / (double)exportDuration) * 100));
					}

					int sleepTime;
					sleepTime = (int)(nDefaultSleepTime - (Calendar.getInstance().getTimeInMillis() - nNextSleepTime));
					if (sleepTime <= 0)
						sleepTime = 1;

					m_nCurrentVideoTime += nDefaultSleepTime;

					//Log.d("EncodeManager", "m_hVideoPlayThread step 2, m_nCurrentVideoTime=" + m_nCurrentVideoTime + ", m_lCurExportVideoTime=" + m_lCurExportVideoTime);

					try {
						sleep(sleepTime);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//Log.d("EncodeManager", "m_hVideoPlayThread step 4");

					if (m_nCurrentVideoTime >= (double) exportDuration) {

						Log.e("EncodeManager", "m_hVideoPlayThread, break");

						m_bExportSuccess = true;

						closeVideos();

						if (m_ExportThread != null){
							m_ExportThread.closeEncoder();
							m_ExportThread = null;
						}

						continue;
					}
				}
				closeMovieFile();
				progressDialog.dismiss();

				ContentValues values = new ContentValues();
				values.put(MediaStore.Video.Media.DATA, m_ExportParam.strFileName);
				getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);

				//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(m_ExportParam.strFileName));
				//				intent.setDataAndType(Uri.parse(m_ExportParam.strFileName), "video/mp4");
				//				startActivity(intent);
				//				finish();
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (DisplayingActivity.shareIntent != null) {
							startActivity(DisplayingActivity.shareIntent);
							DisplayingActivity.shareIntent = null;
						} else {
							Toast.makeText(DisplayingActivity.this, AppUtil.getStringRes(R.string.saved_to) + " " + processedVideoFilePath , Toast.LENGTH_LONG).show();
						}
						//					}

						// refresh video gallery	
						IntentFilter intentfilter = new IntentFilter(Intent.ACTION_MEDIA_SCANNER_STARTED);
						intentfilter.addDataScheme("file");
						MediaScannerReceiver scanSdReceiver = new MediaScannerReceiver();
						scanSdReceiver.setRestart(false);
						registerReceiver(scanSdReceiver, intentfilter);
						sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + processedFolderPath)));

						String thumbImageFilePath = processedFolderPath + "thumb.jpg";
						File thumbImageFile = new File(thumbImageFilePath);		
						if (thumbImageFile.exists())
						{
							thumbImageFile.delete();
						}

						nativeFinitFFMpeg();

						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(processedVideoFilePath));
						intent.setDataAndType(Uri.parse(m_ExportParam.strFileName), "video/mp4");
						startActivity(intent);

					}
				});
			}
		};

		m_hVideoPlayThread.setPriority(Thread.MAX_PRIORITY);
		m_hVideoPlayThread.start();

		m_hAudioPlayThread = new Thread() {
			public void run() {

				MediaFrame AFrame = new MediaFrame();
				MediaFrame AFrameBlank = new MediaFrame();

				double nDefaultSleepTime = 0;
				int nFrameSize = m_nOutAudioBuffSize;

				nDefaultSleepTime = 1000.0 * (double) nFrameSize / (double) m_AudioFormat.nSamplesPerSec;

				nFrameSize = nFrameSize * m_AudioFormat.nBlockAlign;
				AFrame.cbData = nFrameSize;
				AFrame.pbData = new byte[(int) AFrame.cbData];
				AFrameBlank.cbData = nFrameSize;
				AFrameBlank.pbData = new byte[(int) AFrameBlank.cbData];

				int finish = 0;
				m_nCurrentAudioTime = 0.0;
				m_bProcessAudio = true;

				while (true) {
					try {
						sleep(100);
					} catch (InterruptedException e) {
						continue;
					}

					if(m_bExportError || (finish == 1))
						break;

					if(finish == 1)
						break;
					try{
						while(true){

							if(m_bExportError)
							{
								finish = 1;
								break;
							}

							Log.d("AudioThread", "m_hAudioPlayThread-while, m_nCurrentVideoTime=" + m_nCurrentVideoTime + ",m_nCurrentAudioTime=" + 
									(int)m_nCurrentAudioTime + ", nDefaultSleepTime=" + (int)nDefaultSleepTime);

							if(_ProcForAudioPlaying(m_nCurrentAudioTime) == 1)
							{
								finish = 1;
								break;
							}

							if((m_nCurrentAudioTime > (double)exportDuration) || m_bExportSuccess)
							{
								finish = 1;
								break;
							}

							if (m_nCurrentAudioTime + nDefaultSleepTime > m_nCurrentVideoTime)
								break;

							Log.d("AudioThread", "Get Audio Frame at time = " + m_nCurrentAudioTime);

							int bRes = -1;
							if(audioClip.IsPlayable((long)m_nCurrentAudioTime))
							{
								bRes = (int)audioClip.GetFrame(AFrame, (int)m_nCurrentAudioTime, 0, true);
								//Log.d("AudioThread", "Get Frame " + i);
							}

							Log.d("AudioThread", "writeAudioSample");
							if (AFrame.cbData > 0){
								writeAudioSample(AFrame.pbData, (int) AFrame.cbData, (double)m_nCurrentAudioTime/1000);
							}else{
								writeAudioSample(AFrameBlank.pbData, (int) AFrameBlank.cbData, (double)m_nCurrentAudioTime/1000);
							}

							AFrame.cbData = nFrameSize;
							for(int j = 0; j < nFrameSize; j++)
								AFrame.pbData[j] = 0;
							m_nCurrentAudioTime += nDefaultSleepTime;
						}
					}
					catch(Exception e)
					{
						AFrame.MediaFrame_Free();
						AFrame = null;
						AFrameBlank.MediaFrame_Free();
						AFrameBlank = null;
						Log.e("Exception", e.getMessage()+":Exception: step3 at"+TAG);
						break;
					}

					try {
						sleep(5);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				m_bProcessAudio = false;

				AFrame.MediaFrame_Free();
				AFrame = null;
				AFrameBlank.MediaFrame_Free();
				AFrameBlank = null;

				closeAudios();
			}
		};

		m_hAudioPlayThread.setPriority(Thread.MAX_PRIORITY);
		m_hAudioPlayThread.start();
		return 0;
	}

	private int _ProcForVideoPlaying() {

		if(!videoClip.IsPlayable((long)m_nCurrentVideoTime))
			videoClip.Unload();
		return 0;
	}

	private int _ProcForAudioPlaying() {

		if(!audioClip.IsPlayable((long)m_nCurrentAudioTime))
			audioClip.Unload();
		return 0;
	}

	private int updateVideos(double sec) {

		if(videoClip.IsPlayable((long)sec))
			nativeUpdateVideo(videoClip.m_nMediaIndex, sec / (double)1000);
		return 0;
	}

	private int closeVideos()
	{
		videoClip.Unload();
		return 0;
	}

	private int closeAudios()
	{
		audioClip.Unload();
		return 0;
	}

	private int _ProcForAudioPlaying(double nCurrentTime) {

		int ret = 1;
		if(!audioClip.IsPlayable((long)nCurrentTime))
			audioClip.Unload();
		else
			ret = 0;
		return ret;
	}

	public int GetFPS() {
		return (int) (10000000 / m_VideoFormat.AvgTimePerFrame);
	}

	public void SetFPS(int val) {
		m_VideoFormat.AvgTimePerFrame = 10000000 / val;
	}

	public MovieWriterParam GetExportParam() {
		return m_ExportParam;
	}

	//--------------------------------------------------------------------------------------//
	private void OnRenderExportFrame(){

		if (m_pPlayerScreen != null) {

			if (!m_bExportDrawStart){
				m_pPlayerScreen.glDraw();
				m_bExportDrawStart = true;

				int nCnt = 0;
				while((nCnt ++) < 20){	// delay is no more than 1000ms

					try{
						Thread.sleep(50);
					}catch(Exception e){					
					}

					if (nativeGetExportTic() > 0){
						break;
					}					
					//Log.d("CPlayer", "OnRenderExportFrame while nCnt=" + nCnt );
				}

			}else{

				synchronized(this){
					int tic = nativeGetExportTic();

					if (m_nExportTic >= tic){				

						int nCnt = 0;
						while((nCnt ++) < 100){	// delay is no more than 1000ms

							try{
								Thread.sleep(5);
							}catch(Exception e){					
							}

							tic = nativeGetExportTic();
							if (tic > m_nExportTic){
								break;
							}	
						}
					}

					//Log.d("CPlayer", "OnRenderExportFrame 2, tic=" + tic + ", m_nExportTic=" + m_nExportTic);
					m_nExportTic = tic;

					if (m_pPlayerScreen != null)
						m_pPlayerScreen.glDraw();
				}
			}
		}
	}

	private void OnEncodeBuffer() throws EncodeException{
		if (m_ExportThread != null){
			m_ExportThread.pushBuffer((long)m_nCurrentVideoTime, false);
		}
	}

	static {
		if(AppUtil.isSystemHighVersion())
			System.loadLibrary("ffplayer");
	}

	//---------------------------------------------------------------------------------------------------------------//
	public static native void nativeInitFFMpeg();
	public static native void nativeFinitFFMpeg();

	public static native void nativeThreadStart(int nOpenIndex);

	public static native int nativeOpenVideo(String amediaFile);
	public static native int nativeOpenAudio(String amediaFile);
	public static native void nativeClose(int nOpenIndex);

	public static native int nativeUpdateVideo(int nOpenIndex, double sec);
	public static native int nativeUpdateAudio(int nOpenIndex, double sec, int cbData, byte[] buffer);

	public static native double nativeGetTotalTime(int nOpenIndex);
	public static native int nativeSeek(int nOpenIndex, double sec);
	public static native int nativeGetFrameRate(int nOpenIndex);
	private static native int nativeGetExportTic();

	public static native void nativeFastBlur(Bitmap bitmap, int width, int height, int radius);
	public static native void nativeSetBlurEnable(int enable);
	public static native void nativeSetBlurValues(int blurRadius, String vertexString, String fragmentString);
	//---------------------------------------------------------------------------------------------------------------//

	protected native int initBasicPlayer();
	protected native int setStreamFrameRate(int val);
	private native int createMovieFile(String filePath, String fileExt, int nVideoHeight, int nVideoWidth, int nVBitRate, int nABitRate);
	private native int writeAudioSample(byte[] pshData, int len, double nowTime);
	private native void closeMovieFile();
	private native void nativeSetMaskImage(Bitmap bitmap, int width, int height);
	private native void nativeLoadImage(Bitmap bitmap, int width, int height, int drawWidth, int drawHeight,
			int framePosX, int framePosY, int showPosX, int showPosY, int frameWidth);
	public static native void nativeSetVideoInfo(int open_index, int width, int height, int drawWidth, int drawHeight,
			int framePosX, int framePosY, int showPosX, int showPosY, int frameWidth);
	public static native void nativeSetVideoRotation(int open_index, int rotation);

	//--------------------------------------------------------------------------------------//

}