package com.videosquarer.squareimage.gui;

import com.crashlytics.android.Crashlytics;
import java.io.File;
import java.io.IOException;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;

import com.videosquarer.ffmpeg.Utils;
import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.constant.MimeType;
import com.videosquarer.squareimage.util.constant.SchemeType;

public class MainActivity extends UnlockProActivity {
	private static final String TAG = "MainActivity";
	
	public static final String EXTRA_FILE_PATH = "filePath";
	public static final String EXTRA_VIDEO_FILE_PATH = "videoFilePath";	
	public static final String FFMPEG_PATH = "ffmpegPath";
	
	private String mFfmpegInstallPath;
	
	public static final int REQUEST_CODE_PICK_IMAGE = 4242;
	public static final int REQUEST_CODE_PICK_VIDEO = 4243;
	
	static final String LOG_TAG = "test";	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);

		setContentView(R.layout.activity_main);

		installFfmpeg();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}		
	
	@Override
	protected void onStart() {
		super.onStart();
//		FlurryAgent.onStartSession(this, AppUtil.getStringRes(R.string.flurry_api_key));
	}

	@Override
	protected void onStop() {
		super.onStop();
//		FlurryAgent.onEndSession(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.menu_activity_main, menu);
		return true;
	}
	
	public void onClickChoosePhotoButton(View v) {
		Intent intentPickImage = new Intent(Intent.ACTION_GET_CONTENT);
		intentPickImage.setType(MimeType.IMAGE_TYPE);
		startActivityForResult(
				Intent.createChooser(intentPickImage, getString(R.string.pick_image_from)),
				REQUEST_CODE_PICK_IMAGE);
	}

	public void onClickChooseVideoButton(View v) {			
		Intent intentPickImage = new Intent(Intent.ACTION_GET_CONTENT);
		intentPickImage.setType(MimeType.VIDEO_TYPE);	
		startActivityForResult(
				Intent.createChooser(intentPickImage, getString(R.string.pick_video_from)),
				REQUEST_CODE_PICK_VIDEO);			
	}
		
	public void onClickTakeCameraShotButton(View v) {
		AppUtil.showNearFutureToast();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case REQUEST_CODE_PICK_IMAGE:
				passFilePathToDisplayActivity(data);
				break;
			case REQUEST_CODE_PICK_VIDEO:
				passVideoFilePathToDisplayActivity(data);
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void passFilePathToDisplayActivity(Intent data) {
		if (data.getData() == null) {
			AppUtil.showLongToast(R.string.cant_load_image);
			return;
		}

		String imageFilePath;
		Uri selectedImageURI = data.getData();
		String uriScheme = selectedImageURI.getScheme();

		if (uriScheme.equals(SchemeType.SCHEME_CONTENT)) {
			String[] filePathColumn = { MediaStore.Images.Media.DATA };
			Cursor cursor = getContentResolver().query(selectedImageURI, filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			imageFilePath = cursor.getString(columnIndex);
			cursor.close();
		} else if (uriScheme.equals(SchemeType.SCHEME_FILE)) {
			imageFilePath = selectedImageURI.getPath();
		} else {
			AppUtil.showLongToast(R.string.cant_load_image);
			return;
		}

		Intent intentStartActivity = new Intent(this, DisplayingActivity.class);
		intentStartActivity.putExtra(EXTRA_FILE_PATH, imageFilePath);
		startActivity(intentStartActivity);
	}
		
	private void passVideoFilePathToDisplayActivity(Intent data)
	{
		if (data.getData() == null) {
			AppUtil.showLongToast(R.string.cant_load_image);
			return;
		}		
		
		String videoFilePath;
		Uri selectedVideoURI = data.getData();
		videoFilePath = selectedVideoURI.getPath();
		String uriScheme = selectedVideoURI.getScheme();

		if (uriScheme.equals(SchemeType.SCHEME_CONTENT)) {
			String[] filePathColumn = { MediaStore.Video.Media.DATA };
			
			Cursor cursor = getContentResolver().query(selectedVideoURI, filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			videoFilePath = cursor.getString(columnIndex);
			cursor.close();
		} else if (uriScheme.equals(SchemeType.SCHEME_FILE)) {
			videoFilePath = selectedVideoURI.getPath();
		} else {
			AppUtil.showLongToast(R.string.cant_load_image);
			return;
		}
						
		Intent intentStartActivity = new Intent(this, DisplayingActivity.class);
		intentStartActivity.putExtra(EXTRA_FILE_PATH, videoFilePath);
		intentStartActivity.putExtra(FFMPEG_PATH, mFfmpegInstallPath);
		startActivity(intentStartActivity);
	}
	
	private void installFfmpeg() {
		File ffmpegFile = new File(getCacheDir(), "ffmpeg");
		mFfmpegInstallPath = ffmpegFile.toString();
		Log.d(TAG, "ffmpeg install path: " + mFfmpegInstallPath);
		
		if (!ffmpegFile.exists()) {
			try {
				ffmpegFile.createNewFile();
			} catch (IOException e) {
				Log.e(TAG, "Failed to create new file!", e);
			}
			Utils.installBinaryFromRaw(this, R.raw.ffmpeg, ffmpegFile);			
		}
		
		ffmpegFile.setExecutable(true);
	}
}
