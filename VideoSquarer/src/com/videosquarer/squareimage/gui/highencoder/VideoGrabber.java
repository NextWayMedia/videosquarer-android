package com.videosquarer.squareimage.gui.highencoder;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.BitmapWrapper;
import com.videosquarer.squareimage.util.MediaFrame;


/*
 * Video Grabber using FFmpeg.
 * use this class in Amazon Kindle.
 * */

public class VideoGrabber extends MediaGrabber {


	public final static long VIDEO_MIN_DURATION = 0; // 3s
	private final static int VIDEO_MAX_WIDTH = 1920;
	public static String[] VIDEO_EXTENTION = {"avi", "mp4", "3gp", "mov"};

	private String 	mFileName;

	public VideoGrabber()
	{
		mFileName = "";
	}

	// load video file
	public int LoadMedia( String szFileName, long nStartTime, int nRotateAngle, boolean blOriginAspect, boolean runThread)
	{
		if (m_bLoaded)
			return super.Load(szFileName, nStartTime);

		if ( m_bCanLoad == false )
			return -1;

		try{
			m_nMediaIndex = DisplayingActivity.nativeOpenVideo(szFileName);
		}catch(Exception e){
			return -1;
		}

		if (m_nMediaIndex < 0)
			return -1;

		mFileName = szFileName;
		m_nDuration = (long)(DisplayingActivity.nativeGetTotalTime(m_nMediaIndex) * 1000.0f);

		DisplayingActivity.nativeThreadStart(m_nMediaIndex);

		return super.Load(szFileName, nStartTime);
	}

	public void setVideoInfo(int width, int height, int drawWidth, int drawHeight, int framePosX, int framePosY, int showPosX, int showPosY, int frameWidth)
	{
		if ( m_bCanLoad == false )
			return ;

		DisplayingActivity.nativeSetVideoInfo(m_nMediaIndex, width, height, drawWidth, drawHeight, framePosX, framePosY, showPosX, showPosY, frameWidth);

	}
	
	public void setVideoRotation(int rotation)
	{
		if ( m_bCanLoad == false )
			return ;

		DisplayingActivity.nativeSetVideoRotation(m_nMediaIndex, rotation);
	}
	// unload video file
	public int Unload()
	{
		if (m_nDuration == 0 || !m_bLoaded)
			return 0;

		DisplayingActivity.nativeClose(m_nMediaIndex);
		return super.Unload();
	}

	public int UpdateVideo(long militime, int videoIndex, int nRotateAngle, int nOriginAspect)
	{
		if ( !m_bCanLoad || !m_bLoaded)
			return -1;

		double secs = (double) militime;

		if (secs < 0)
			secs = 0;
		secs = secs / 1000.0;

		//int ret = nativeUpdateVideo(m_nMediaIndex, m_nSettingVideoOrder, secs, nRotateAngle, nOriginAspect);
		int ret = DisplayingActivity.nativeUpdateVideo(m_nMediaIndex, secs);

		return ret;
	}

	// nSeekTime is milisecond
	public int MediaSeek(long nSeekTime) {

		if ( !m_bCanLoad || !m_bLoaded)
			return -1;

		double secs = (double) nSeekTime;

		if (secs < 0)
			secs = 0;

		secs = secs / 1000.0;

		int ret = DisplayingActivity.nativeSeek(m_nMediaIndex, secs);

		return ret;
	}

	// get media frame for thumb image
	public long GetMediaFrame( long nFrameTime, MediaFrame pMediaFrame)
	{
		if ( m_bCanLoad == false )
			return -1;

		if (pMediaFrame != null){

			// if time small than 1000ms, return first frame.
			if (nFrameTime < 1000)
				nFrameTime = 1000;

			// for amazon kindle.
			if (AppUtil.getSystemVer() >= 17)
			{
				if (getThumb(mFileName, pMediaFrame.bmpData, nFrameTime) < 0)
				{
					Canvas canvas = new Canvas(pMediaFrame.bmpData);
					canvas.drawRGB(128, 128, 128);
				}
			}
		} 

		return 0;
	}

	// return video duration, else error code
	// get media frame for thumb image
	public long GetMediaFrame( String path, long milliSec, MediaFrame pMediaFrame)
	{
		if (AppUtil.getSystemVer() >= 17){
			return getThumb(path, pMediaFrame.bmpData, milliSec);
		}

		return -1;
	}

	private long getThumb(String path, Bitmap bmpData, long milliSec)
	{
		long _duration = -1;
		if (bmpData != null){
			if ( CheckVideoType(path) < 0){
				Log.e("CVideoGrabber", "GetMediaFrame CheckVideoType err, path=" + path);
				return AppUtil.VIDEO_ERR_BADVIDEOTYPE;
			}
			try{
				MediaMetadataRetriever _retriever = new MediaMetadataRetriever();
				_retriever.setDataSource(path);
				String videoW = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
				String videoH = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
				String duration = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
				//				String rotation = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
				String rotation = null;
				if (videoW != null && Integer.parseInt(videoW) > VIDEO_MAX_WIDTH){

					_retriever.release();
					_retriever = null;

					Log.e("CVideoGrabber", "GetMediaFrame path=" + path + ",videoW small=" + videoW);
					return AppUtil.VIDEO_ERR_BIGSIZE;
				}

				if (videoH != null && Integer.parseInt(videoH) > VIDEO_MAX_WIDTH){

					_retriever.release();
					_retriever = null;

					Log.e("CVideoGrabber", "GetMediaFrame path=" + path + ",videoH small=" + videoH);
					return AppUtil.VIDEO_ERR_BIGSIZE;
				}

				int _rotation = 0;
				try{
					_duration = Long.parseLong(duration);
					if (rotation == null)
						_rotation = 0;
					else
						_rotation = Integer.parseInt(rotation);
				}catch(Exception e){

					_retriever.release();
					_retriever = null;

					return -1;
				}

				Bitmap frame = _retriever.getFrameAtTime(milliSec * 1000, MediaMetadataRetriever.OPTION_PREVIOUS_SYNC);
				_retriever.release();
				_retriever = null;

				if (_duration < VIDEO_MIN_DURATION){
					Log.e("CVideoGrabber", "GetMediaFrame path=" + path + ", video duration small=" + _duration);
					return AppUtil.VIDEO_ERR_SHORTTIME;
				}

				if (_duration < 1 || frame == null){
					Log.e("CVideoGrabber", "GetMediaFrame getFrameAtTime err, path=" + path);
					return -1;
				}
				Canvas canvas = new Canvas(bmpData);
				Paint paint = new Paint();
				paint.setFilterBitmap(true);
				if (_rotation == 0){
					canvas.drawBitmap(frame, null, new Rect(0, 0, bmpData.getWidth(), bmpData.getHeight()), paint);
				}else{
					Matrix matrix = new Matrix();
					matrix.setRotate(-1 * _rotation, frame.getWidth()/2, frame.getHeight()/2);
					Bitmap bmp = BitmapWrapper.createBitmap(frame, 0, 0, frame.getWidth(), frame.getHeight(), matrix, true);
					canvas.drawBitmap(bmp, new Rect(0, 0, bmp.getWidth(), bmp.getHeight()), 
							new Rect(0, 0, bmpData.getWidth(), bmpData.getHeight()), paint);
				}
				paint = null;
				Log.d("CVideoGrabber", "GetMediaFrame path=" + path + ", rotation=" + rotation + ",frame=" + ((frame==null) ? "null" : "not null"));
			}catch(Exception e2){				
			}
		} 
		return _duration;
	}

	public static long getDuration(String path)
	{
		long _duration = -1;
			if ( CheckVideoType(path) < 0){
				Log.e("CVideoGrabber", "GetDuration CheckVideoType err, path=" + path);
				return AppUtil.VIDEO_ERR_BADVIDEOTYPE;
			}
			try{
				MediaMetadataRetriever _retriever = new MediaMetadataRetriever();
				_retriever.setDataSource(path);
				String videoW = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
				String videoH = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
				String duration = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
				//				String rotation = _retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
				String rotation = null;
				if (videoW != null && Integer.parseInt(videoW) > VIDEO_MAX_WIDTH){

					_retriever.release();
					_retriever = null;

					Log.e("CVideoGrabber", "GetDuration BIG SIZE path=" + path + ",videoW small=" + videoW);
					return AppUtil.VIDEO_ERR_BIGSIZE;
				}

				if (videoH != null && Integer.parseInt(videoH) > VIDEO_MAX_WIDTH){

					_retriever.release();
					_retriever = null;

					Log.e("CVideoGrabber", "GetDuration BIGSIZE path=" + path + ",videoH small=" + videoH);
					return AppUtil.VIDEO_ERR_BIGSIZE;
				}

				int _rotation = 0;
				try{
					_duration = Long.parseLong(duration);
					if (rotation == null)
						_rotation = 0;
					else
						_rotation = Integer.parseInt(rotation);
				}catch(Exception e){

					_retriever.release();
					_retriever = null;

					return -1;
				}

				_retriever.release();
				_retriever = null;

				if (_duration < VIDEO_MIN_DURATION){
					Log.e("CVideoGrabber", "GetDuration SHORTTIME path=" + path + ", video duration small=" + _duration);
					return AppUtil.VIDEO_ERR_SHORTTIME;
				}

				Log.d("CVideoGrabber", "GetDuration path=" + path + ", duration=" + _duration);
			}catch(Exception e2){				
			}
		return _duration * 1000;
	}

	// return -1 when bad type.
	@SuppressLint("DefaultLocale")
	public static long CheckVideoType( String szFileName )
	{
		if (szFileName.length() < 4)
			return -1;

		String strFileName = szFileName.toLowerCase();
		String[] strFileExtArray = VIDEO_EXTENTION;

		for (int i = 0; i < strFileExtArray.length; i++)
		{
			if (strFileName.substring(strFileName.length() - 3, strFileName.length()).contentEquals(strFileExtArray[i]))
				return i;
		}

		return -1;
	}
}