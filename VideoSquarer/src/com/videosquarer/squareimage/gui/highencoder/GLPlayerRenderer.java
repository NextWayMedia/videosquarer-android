package com.videosquarer.squareimage.gui.highencoder;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;

public class GLPlayerRenderer implements GLSurfaceView.Renderer {

	public GLPlayerRenderer() {

	}

	public void onDrawFrame(GL10 gl) {
		// TODO Auto-generated method stub
		NativeRenderFrame();
	}

	public void onSurfaceChanged(GL10 gl, int width, int height) {
		// TODO Auto-generated method stub
		NativeRenderResize();
	}

	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		// TODO Auto-generated method stub
		NativeRenderInit();
	}
	
	public void OnDestroy()
	{
		NativeRenderFinit();
	}
	
	public native void NativeRenderInit();
	public native void NativeRenderFinit();
	public native void NativeRenderResize();
	public native void NativeRenderFrame();
}
