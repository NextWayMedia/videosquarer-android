package com.videosquarer.squareimage.gui.highencoder;

import android.media.MediaMetadataRetriever;
import android.util.Log;

import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.util.MediaFrame;
import com.videosquarer.squareimage.util.WAVEFORMATEX;


public class AudioGrabber extends MediaGrabber {
	
	public WAVEFORMATEX		m_audioInfo = new WAVEFORMATEX();
	public WAVEFORMATEX		m_wfxDest = new WAVEFORMATEX();
	public long				m_nAudioType;

	public final static byte ATYPE_UNKNOWN = -1;
	public final static byte ATYPE_PCM = 0;
	public final static byte ATYPE_VIDEO = 1;
	
	public final static int EXPORTED_AUDIO_RATE = 44100;
	public final static int EXPORTED_AUDIO_BITS = 16;
	public final static int EXPORTED_AUDIO_CHANNELS = 2;

	public final static String[] AUDIO_EXTENTION = {"mp3", "wma", "wav", "aac"};

    public AudioGrabber()
	{
		m_nAudioType = ATYPE_UNKNOWN;

		m_wfxDest.nSamplesPerSec  = EXPORTED_AUDIO_RATE;
		m_wfxDest.wBitsPerSample  = EXPORTED_AUDIO_BITS;
		m_wfxDest.nChannels		  = EXPORTED_AUDIO_CHANNELS;
		m_wfxDest.nBlockAlign	  = (short) (m_wfxDest.wBitsPerSample * m_wfxDest.nChannels / 8);
		m_wfxDest.nAvgBytesPerSec = m_wfxDest.nBlockAlign * m_wfxDest.nSamplesPerSec;
	}

	public int LoadMedia( String szFileName, long nStartTime)
	{
		return LoadMedia( szFileName, nStartTime, false );
	}
	
	public int LoadMedia( String szFileName, long nStartTime, boolean bVideoFile )
	{
		if (m_bLoaded)
			return super.Load(szFileName, nStartTime);
		
		if ( m_bCanLoad == false )
			return -1;

		if (bVideoFile){
			m_nAudioType = ATYPE_VIDEO;
		}else{
			
			m_nAudioType = GetAudioType(szFileName);
			
			if ( m_nAudioType == ATYPE_UNKNOWN )
			{
				m_bCanLoad = false;
				return -1;
			}
		}

		try{
			m_nMediaIndex = DisplayingActivity.nativeOpenAudio(szFileName);
		}catch(Exception e){
			return -1;
		}

        if (m_nMediaIndex < 0)
            return -1;

		m_nDuration = (long)(DisplayingActivity.nativeGetTotalTime(m_nMediaIndex) * 1000.0f);

		DisplayingActivity.nativeThreadStart(m_nMediaIndex);
		
		return super.Load(szFileName, nStartTime);
	}

	public int Unload()
	{
		if (m_nDuration == 0 || !m_bLoaded)
			return 0;

		Log.e("AudioGrabber", "AudioGrabber::nativeClose");
		
		int ret = super.Unload();
		DisplayingActivity.nativeClose(m_nMediaIndex);
		return ret;
	}
	
	//get audio duration by MediaMetadataRetriever
	public int GetPromptDuration(String szFileName, boolean bVideoFile)
	{
		if (bVideoFile){
			m_nAudioType = ATYPE_VIDEO;
		}else{
			
			m_nAudioType = GetAudioType(szFileName);
			
			if ( m_nAudioType == ATYPE_UNKNOWN )
			{
				m_bCanLoad = false;
				return -1;
			}
		}
		
		int duration = -1;
		MediaMetadataRetriever retriever; 
		// load MediaMetadataRetriever
		try{
			retriever = new MediaMetadataRetriever();
			retriever.setDataSource(szFileName);
			
			String val = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
			duration = Integer.parseInt(val);
		}catch(Exception e){
			return -1;
		}
		
		retriever.release();
		retriever = null;
		
		m_nDuration = (long)duration;
		
		return duration;
	}

	public long GetMediaFrame( long nFrameTime, MediaFrame pMediaFrame, Object hAbortEvent, boolean bWaitLong )
	{
		if ( !m_bCanLoad || !m_bLoaded)
			return -1;

		if (nFrameTime < 0)
			nFrameTime = 0;
		
		long ret = -1;
		double secs = (double) nFrameTime;
		secs = secs / 1000.0;

		if (pMediaFrame != null){
			Log.d("AudioGrabber", "GetMediaFrame 1");

			ret = DisplayingActivity.nativeUpdateAudio(m_nMediaIndex, secs, (int) pMediaFrame.cbData, pMediaFrame.pbData);
			Log.d("AudioGrabber", "GetMediaFrame 2");

			if (ret == -2){
				
				DisplayingActivity.nativeSeek(m_nMediaIndex, secs);	
				pMediaFrame.cbData = 0;
				Log.d("AudioGrabber", "GetMediaFrame 3");

			}else if(ret >= 0){
				
				pMediaFrame.cbData = ret;
				ret = 0;
			}
			
			return ret;
		}

		//Define.LOGD("QditorActivity", "AudioGrabber::GetMediaFrame 2, pMediaFrame is null");
		Log.d("AudioGrabber", "GetMediaFrame End");

		return 0;
	}

	public long GetFrame(MediaFrame pFrame, long nTime, int hAbortEvent, boolean bWaitLong)
	{
		if ( m_bLoaded == false ||GetCanLoad() == false)
		{
			return 0;
		}

		long nResult;
		Log.d("AudioGrabber", "Get Frame Step 1");

		nResult = GetMediaFrame(nTime, pFrame, hAbortEvent, bWaitLong);
		Log.d("AudioGrabber", "Get Frame Step 2");

		if ( nResult == 0 )
			mmaudio.ProcessVolume(m_wfxDest, pFrame.pbData, pFrame.pbData, (int) pFrame.cbData, 100);
		else
			nResult = 0;
		Log.d("AudioGrabber", "Get Frame Step End");

		return nResult;
	}

	public int MediaSeek(long nSeekTime) {
		
		if ( !m_bCanLoad || !m_bLoaded)
			return -1;
		
		double secs = (double) nSeekTime;
		
		if (secs < 0)
			secs = 0;
		
		secs = secs / 1000.0;

		int ret = DisplayingActivity.nativeSeek(m_nMediaIndex, secs);
		
		return ret;
	}

	long GetAudioType( String szFileName )
	{
		if ( m_nAudioType != ATYPE_UNKNOWN )
			return m_nAudioType;

		if (szFileName.length() < 4)
			return ATYPE_UNKNOWN;
		
		String strFileName = szFileName.toLowerCase();
		String[] strFileExtArray = AUDIO_EXTENTION;
		
		for (int i = 0; i < strFileExtArray.length; i++)
		{
			if (strFileName.substring(strFileName.length() - 3, strFileName.length()).contentEquals(
					strFileExtArray[i]))
				return i;
		}
		
		return ATYPE_UNKNOWN;
	}
}