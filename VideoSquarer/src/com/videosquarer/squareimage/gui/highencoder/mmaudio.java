package com.videosquarer.squareimage.gui.highencoder;

import com.videosquarer.squareimage.util.WAVEFORMATEX;

public class mmaudio {

	public	static	int	FADE_IN_TIME = 4000;
	public	static	int	FADE_OUT_TIME = 4000;


	public	static	long glCachedBalance = 0; //last balance level
	public	static	boolean gfInternalGenerated = false;

	public static int GetWaveOutID(boolean[] pfPreferred)
	{
		return 0;
	}

	public static void SetWaveOutID(int dwWaveID, boolean fPrefOnly)
	{
//		int       dwFlags = fPrefOnly ? 0x00000001 : 0x00000000;
	}

	public static int GetWaveInID(boolean[] pfPreferred)
	{
		return 0;
	}

	public static void SetWaveInID(int dwWaveID, boolean fPrefOnly)
	{
	}

	public static int GetMIDIOutID()
	{
		return 0;
	}

	public static void SetMIDIOutID(int dwWaveID)
	{
	}

	public static int[] GetWaveID(int[] puWaveID_Count, boolean bIn)
	{
		return null;
	}

	public static boolean GetVolume(int[] uMxId, int dwVolID, int dwChannel, int[] pdwVol)
	{
		return false;
	}

	public static void SetVolume(int[] uMxId, int dwVolID, int dwChannel, int dwVol)
	{
	}

	public static int MergeWaveData(WAVEFORMATEX pwfx, byte[] pbDestData, byte[] pbSrcData, int cbData1, int cbData2)
	{
		int step = pwfx.wBitsPerSample / 8;

		return nativeMergeWaveData( pbDestData, pbSrcData, cbData1, cbData2, step );
	}

	public static int ProcessVolume( WAVEFORMATEX pwfx, byte[] pbDestData, byte[] pbSrcData, int cbData, long nVolume )
	{
		int step = pwfx.wBitsPerSample / 8;
		
		return nativeProcessVolume( pbDestData, cbData, step, (int)nVolume );
	}

	public static int ProcessFadeIn( WAVEFORMATEX pwfx, byte[] pbDestData, byte[] pbSrcData, int cbData, long nDuration, long nCurrentTime )
	{
		if ( nCurrentTime > FADE_IN_TIME )
		{
			return cbData;
		}

		long nVolume = nCurrentTime * 100 / FADE_IN_TIME;

		return ProcessVolume(pwfx, pbDestData, pbSrcData, cbData, nVolume);
	}

	public static int ProcessFadeOut( WAVEFORMATEX pwfx, byte[] pbDestData, byte[] pbSrcData, int cbData, long nDuration, long nCurrentTime )
	{
		if ( nDuration - nCurrentTime > FADE_OUT_TIME ){
			return cbData;
		}

		long nVolume = (nDuration - nCurrentTime) * 100 / FADE_OUT_TIME;

		return ProcessVolume(pwfx, pbDestData, pbSrcData, cbData, nVolume);
	}

	public native static int nativeMergeWaveData( byte[] pbDestData, byte[] pbSrcData, int cbData1, int cbData2, int step );
	public native static int nativeProcessVolume( byte[] pbDestData, int cbData, int step, int nVolume );
}
