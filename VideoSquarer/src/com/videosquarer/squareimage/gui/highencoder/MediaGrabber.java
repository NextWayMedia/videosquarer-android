package com.videosquarer.squareimage.gui.highencoder;

import com.videosquarer.squareimage.util.AM_MEDIA_TYPE;

public abstract class MediaGrabber
{
	
	public	long				m_nDuration;
	public	long				m_nStartTime;
	public	long				m_nCurrentTime;
	public	boolean				m_bEOS;
	public	boolean				m_bCanLoad;
	public	boolean				m_bPlaying;
	
	public	int					m_nMediaIndex = -1;

	public	AM_MEDIA_TYPE		m_mediaType;
	
	public final static int AUDIO_DATA_ID = 1;
	public final static int VIDEO_DATA_ID = 2;

	public final static int VIDEO_AUDIO_ID = 0;
	public final static int AUDIO_ID = 1;
	public final static int VIDEO_ID = 2;

	public final static int WAIT_ABANDONED = 0x80;
	public final static int WAIT_OBJECT_0 = 0;
	public final static int WAIT_TIMEOUT = 0x102;
	public final static int WAIT_FAILED = -1;
	
	public final static int m_nRangeTime = 500;
	
	public static int[] g_nMediaIndexArray = new int[100];
	
	public Object		m_csListFrame = new Object();
	
	protected boolean 	m_bLoaded;
	
	public MediaGrabber()
	{
		m_nDuration				= 0;
		m_nCurrentTime			= 0;
		m_bEOS					= true;
		m_bCanLoad				= true;
		m_bPlaying				= false;
		
		m_bLoaded 				= false;
	}

	public int Load( String szFileName, long nStartTime )
	{
		if ( m_bCanLoad == false )
			return -1;

		m_nStartTime = nStartTime;
		m_bEOS = false;

		m_bLoaded = true;

		return 0;
	}

	public int Unload()
	{
		m_bEOS = true;
		m_bLoaded = false;

        return 0;
	}

	public int StartGrabber()
	{
		m_bEOS = false;
		
		return 0;
	}

	public int StopGrabber()
	{
		m_bEOS = true;
		return 0;
	}

	public long Seek( long nSeekTime )
	{
		m_nCurrentTime = nSeekTime;
		m_bEOS = false;

		int nRet = MediaSeek( nSeekTime);

    	if (nRet < 0)
    		return -1;

    	return nSeekTime;
	}

	public boolean IsPlayable(long nTime) {
		if (m_nDuration < 1){
			return false;
		}

		if ( nTime >= m_nStartTime && nTime <= GetDuration() )
			return true;
		else
			return false;
	}

	public long GetDuration() 
	{
		return m_nDuration;
	}
	
	public boolean GetCanLoad()
	{
		return m_bCanLoad;
	}
	
	// abstract function 
	public abstract int MediaSeek(long nSeekTime);
	
	
	//---------------------------------------------------------------------------------------------------------------//	
	//---------------------------------------------------------------------------------------------------------------//
	/*
	 return int[7]
	 int[0] : ret code 1: success, 0: fail
	 int[1] : video duration (ms)
	 int[2] : video width
	 int[3] : video height
	 int[4] : video rotation angle (0, 90, 180, 270)
	 int[5] : video codec is possible
	 int[6] : audio codec is possible
	 */
//	public static native int[] nativeGetVideoInfo(String vmediaFile);
	
	/*
	 return int[3]
	 int[0] : ret code 1: success, 0: fail
	 int[1] : video duration (ms)
	 int[2] : audio codec is possible
	 */
//	public static native int[] nativeGetAudioInfo(String amediaFile);
	 
//	protected native void nativeThreadStart(int nOpenIndex);
//    protected native void nativeClose(int nOpenIndex);
//    
//    protected native int nativeOpenAudio(String mediaFile);
//    protected native int nativeUpdateAudio(int nOpenIndex, double sec, int cbData, byte[] buffer);
    
    // previous video functions using ffmpeg. 	---------------------------------------------------------------------//
//    protected native int nativeOpenVideo(String mediaFile);
//    protected native int nativeUpdateVideo(int nOpenIndex, int nVideoIndex, double sec, int nRotateAngle, int nOriginAspect);
//    
//    protected native double nativeGetTotalTime(int nOpenIndex);
//    protected native int nativeSeek(int nOpenIndex, double sec);
    
}