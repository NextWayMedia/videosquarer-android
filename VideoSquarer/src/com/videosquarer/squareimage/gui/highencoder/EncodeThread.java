package com.videosquarer.squareimage.gui.highencoder;

import java.nio.ByteBuffer;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.util.Log;

public class EncodeThread extends Thread{

	// video encode exception class.
	public static class EncodeException extends Exception
	{
		public EncodeException(){
			super();
		}
	}
	
	private final String TAG ="EncodeThread";
	private MediaCodec 		mEncoder;
	private ByteBuffer[] encoderInputBuffers;
	private ByteBuffer[] encoderOutputBuffers;
	private boolean 	mAbort = false;
	private boolean		mCreated = false;
	private boolean 	mRunning = false;
	private int			m_nExportWidth = 0;;
	private int			m_nExportHeight = 0;
	byte[] 				m_buffExport;
	private long		m_lastTime = 0;
	
	public EncodeThread() {
		mCreated = false;
		mRunning = false;
	}

	public void AbortThread(){
		mAbort = true;
		
		while(mRunning){
		
			try{
				sleep(10);
			}catch(Exception e){				
			}			
		}
	}

	public boolean createEncoder(int width, int height, int bitrate, int framerate, int colorFormat, String codecName)
	{
		Log.d(TAG, "createEncoder s");
		
		if (colorFormat < 0) {
			Log.e(TAG, "No color format for encode");
			return false;
		}
	
		m_nExportWidth = width;
		m_nExportHeight = height;
	
		Log.d(TAG, "width=" + width + ",height=" + height + ",bitrate=" + bitrate + ",framerate=" + framerate);

		//mEncoder = MediaCodec.createEncoderByType("video/avc");	
	    mEncoder = MediaCodec.createByCodecName(codecName);
	    MediaFormat mediaFormat = MediaFormat.createVideoFormat("video/avc", width, height);
	     
	    mediaFormat.setInteger(MediaFormat.KEY_WIDTH, width);
	    mediaFormat.setInteger(MediaFormat.KEY_HEIGHT, height);
	    mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, bitrate);
	    mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, framerate);
	    mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
	    mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);
	    
	    mEncoder.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
	    mEncoder.start();
	    
	    encoderInputBuffers = mEncoder.getInputBuffers();
	    encoderOutputBuffers = mEncoder.getOutputBuffers();
	    	
	    double yuvBitRate = 1.5;
	    int size = (int)(width * height * yuvBitRate);
	    m_buffExport = new byte[size];
	    
	    mCreated = true;
	    mRunning = false;
	    
	    m_lastTime = 0;
	    
	    Log.d(TAG, "createEncoder e");
		return true;
	}
	
	public void closeEncoder()
	{
		Log.d(TAG, "closeEncoder s");
		
		try{
			pushBuffer(m_lastTime + 30, true);
		}catch(EncodeException e){
			Log.e(TAG, "pushBuffer EncodeException e=" + e.toString());
		}
		
		//sleep for last buffer
		try{
			Thread.sleep(100);
		}catch(Exception e){				
		}
		
		AbortThread();
		
		if (!mCreated)
			return;
		
	    try {
	    	mEncoder.stop();
	    	mEncoder.release();

	    } catch (Exception e){ 
	        e.printStackTrace();
	    }
	    
	    mEncoder = null;
	    
	    m_buffExport = null;
	    mCreated = false;
	    
		//sleep for last buffer
		try{
			Thread.sleep(200);
		}catch(Exception e){
		}
			
	    Log.d(TAG, "closeEncoder e");
	}
	
	public boolean pushBuffer(long nowTime /*millisecond*/, boolean isLast) throws EncodeException
	{
		if (!mRunning)
			return false;
		
		m_lastTime = nowTime;
		
		if (!isLast){
			getScreenBuff(m_buffExport, m_buffExport.length);
		}
		
		try{
			// presentationTimeUs is microsecond.
			long presentationTimeUs = (long)(nowTime);
			presentationTimeUs = presentationTimeUs * 1000;
		       
	    	if (m_buffExport.length > 0){
		        
		        int inputBufferIndex = mEncoder.dequeueInputBuffer(-1);
		        if (inputBufferIndex >= 0) {
		            ByteBuffer inputBuffer = encoderInputBuffers[inputBufferIndex];
		            
		            inputBuffer.clear();
		            inputBuffer.put(m_buffExport, 0, m_buffExport.length);
		            
		            //Log.d(TAG, "pushBuffer step1 nowTime=" + nowTime + ",len=" + len);
		            
		            mEncoder.queueInputBuffer(inputBufferIndex, 0, m_buffExport.length, 
		            			presentationTimeUs, isLast ? MediaCodec.BUFFER_FLAG_END_OF_STREAM : 0);
		        }
		    }
		}catch(Exception e){
			throw new EncodeException();
		}
    	
		return true;
	}
	
	public void run()
	{
		final long kTimeOutUs = -1;
		MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
		
		mAbort = false;
		mRunning = true;
		
		int size = (int)(m_nExportWidth * m_nExportHeight * 2);
		byte[] encodOutBuff = new byte[size];
		byte[] spsBuff = new byte[300];
		
		Log.d(TAG, "EncodeThread begin.");
		
		boolean bGetSPS = false;
		
		int nRepeat = 0;
		
		while(true){
	
			//Log.d(TAG, "EncodeThread while 1");
			int outputBufIndex = -1;
			
			try{
				outputBufIndex = mEncoder.dequeueOutputBuffer(info, kTimeOutUs);
			}catch(Exception e){
				Log.e(TAG, "EncodeThread error " + e.toString());
			}

	      
			// break encode thread
			if (mAbort && (nRepeat ++ > 10)){
	    	
				Log.e(TAG, "encoder ncnt over 10");
				break;
			}

			if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
	    	
				Log.e(TAG, "encoder info.flags = BUFFER_FLAG_END_OF_STREAM");
				
				// release encode buffer at last.
				if (outputBufIndex >= 0){
					mEncoder.releaseOutputBuffer(outputBufIndex, false);
				}
				
				break;
			}
	      
	        if (outputBufIndex >= 0) {
	            ByteBuffer buf = encoderOutputBuffers[outputBufIndex];
	            
	            buf.get(encodOutBuff, 0, info.size);
	            buf.clear();
	            int flag = 0;
	            
	            if (!bGetSPS){
	            	
	            	bGetSPS = true;
	            	writeEncodedVideoHeader(encodOutBuff, info.size);
	            	
	            	System.arraycopy(encodOutBuff, 0, spsBuff, 0, info.size);
	            	
	            }else{
	            	
		            long presentationTimeUs = info.presentationTimeUs;
		            double tm = (double)presentationTimeUs/1000000;
		            
		            flag = (info.flags == MediaCodec.BUFFER_FLAG_SYNC_FRAME) ? 1 : 0;
		            writeEncodedVideoBuff(encodOutBuff, info.size, tm, flag);
	            }
	            
	           //Log.d(TAG, "EncodeThread while 4, info.size=" + info.size + ",tm=" + info.presentationTimeUs + ", flag=" + flag);
	            
		       mEncoder.releaseOutputBuffer(outputBufIndex, false);
		       
		      // Log.e(TAG, "encoder writeEncodedVideoBuff 2");
	        } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
	            encoderOutputBuffers = mEncoder.getOutputBuffers();

	            Log.d(TAG, "encoder output buffers have changed.");
	        } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
	            //MediaFormat encformat = mEncoder.getOutputFormat();
	        }
	        
	        try{		        
	        	sleep(2);
	        }catch(Exception e){		        	
	        }
		}
		
		encodOutBuff = null;
		
		mRunning = false;
		Log.d(TAG, "EncodeThread end.");
	}

	private static native int writeEncodedVideoHeader(byte[] buff, int size);
	private static native int writeEncodedVideoBuff(byte[] buff, int size, double nowTime, int isKeyFrame);	
	public static native void getScreenBuff(byte[] buffScreen, int size);
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              