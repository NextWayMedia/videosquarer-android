package com.videosquarer.squareimage.gui.element.predefinedcolorsdialog;

public interface OnPredefinedColorSelectedListener {

	void onColorSelected(int color);

}
