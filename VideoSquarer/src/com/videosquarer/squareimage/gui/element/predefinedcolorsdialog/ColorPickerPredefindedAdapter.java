package com.videosquarer.squareimage.gui.element.predefinedcolorsdialog;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.constant.SharedPrefKeys;

public class ColorPickerPredefindedAdapter extends BaseAdapter
		implements OnItemClickListener, OnItemLongClickListener {

	private static final String TAG = "ColorPickerAdapter";
	private static final String COLOR_DIEZ = "#";
	private static final String COLOR_SEPARATOR = ", ";
	private static final int COLOR_SQUARE_SIZE = 42;
	private Context context;
	private Dialog dialog;
	private List<Integer> colorList;
	private String[] colors;

	private int lastColorPosition = 0;

	public ColorPickerPredefindedAdapter(Context context, Dialog dialog) {
		this.context = context;
		this.dialog = dialog;
		colorList = new ArrayList<Integer>();

		colors = context.getResources()
				.getStringArray(R.array.predefined_colors_array);

		for (String color : colors) {
			colorList.add(Color.parseColor(COLOR_DIEZ + color));
		}

		String[] restOfPalette = context.getResources()
				.getStringArray(R.array.other_palette_colors);

		for (String color : restOfPalette) {
			colorList.add(Color.parseColor(COLOR_DIEZ + color));
		}

		String previouslyUsedColor = AppUtil.getSharedPrefString(
				SharedPrefKeys.SHARED_PREF_PREVIOUSLY_USED_COLORS);

		String[] previouslyUsedColors = previouslyUsedColor.split(COLOR_SEPARATOR);

		for (int i = 0; i < previouslyUsedColors.length; i++) {
			if (!previouslyUsedColors[i].equals(colors[i + 2])) {
				if (AppUtil.isValidText(previouslyUsedColors[i])) {
					colorList.set(i + 2, Integer.parseInt(previouslyUsedColors[i]));
				}
			}
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = new ImageView(context);
			convertView.setLayoutParams(new GridView.LayoutParams(
					AppUtil.convertToPixels(COLOR_SQUARE_SIZE),
					AppUtil.convertToPixels(COLOR_SQUARE_SIZE)));

		}

		((ImageView) convertView).setImageResource(R.drawable.color_pick_states);
		convertView.setBackgroundColor(colorList.get(position));
		convertView.setId(position);

		return convertView;
	}

	@Override
	public int getCount() {
		return colorList.size();
	}

	@Override
	public Object getItem(int position) {
		return colorList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		removeColorAt(arg2);
		return true;
	}

	private void removeColorAt(int arg2) {
		// We should not touch first two colors and other rows
		if (arg2 > 1 && arg2 < 6) {
			colorList.set(arg2, Color.parseColor(COLOR_DIEZ + colors[arg2]));
			AppUtil.putSharedPref(
					SharedPrefKeys.SHARED_PREF_PREVIOUSLY_USED_COLORS,
					formRecentColorString());
			notifyDataSetChanged();
		} else {
			AppUtil.showLongToast(R.string.cant_remove_default_colors);
		}
	}

	public boolean addColor(int color) {
		if (!colorList.contains(color)) {
			if (lastColorPosition == 0 || lastColorPosition == 5) {
				lastColorPosition = 2;
			} else {
				lastColorPosition++;
			}

			boolean isGreyColorFounded = false;
			for (int i = 2; i < 6; i++) {
				if (!isGreyColorFounded) {
					for (int j = 2; j < 6; j++) {
						if (colorList.get(i).intValue() == Color.parseColor(COLOR_DIEZ + colors[j])) {
							colorList.set(i, color);
							lastColorPosition = i;
							isGreyColorFounded = true;
							break;
						}
					}
				} else {
					break;
				}
			}

			if (!isGreyColorFounded) {
				colorList.set(lastColorPosition, color);
			}
			AppUtil.putSharedPref(
					SharedPrefKeys.SHARED_PREF_PREVIOUSLY_USED_COLORS,
					formRecentColorString());

			notifyDataSetChanged();

			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		((ColorPickerPredefinedDialog) dialog).colorPicked(colorList.get(arg2));
		dialog.dismiss();
	}

	private String formRecentColorString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 2; i < 6; i++) {
			sb.append(colorList.get(i));
			if (i + 1 < colorList.size()) {
				sb.append(COLOR_SEPARATOR);
			}
		}

		return sb.toString();
	}
}