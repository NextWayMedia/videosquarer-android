package com.videosquarer.squareimage.gui.element.predefinedcolorsdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.util.AppUtil;

public class ColorPickerPredefinedDialog extends Dialog {

	private static final String TAG = "ColorPickerPredefinedDialog";
	private DisplayingActivity activity;	
	private boolean bDisplayingActivity = true;
	private OnPredefinedColorSelectedListener listener;

	public ColorPickerPredefinedDialog(DisplayingActivity activity,
			OnPredefinedColorSelectedListener listener) {
		super(activity);
		this.activity = activity;
		this.listener = listener;
		bDisplayingActivity = true;
		this.setTitle(AppUtil.getStringRes(R.string.color_picker));
	}
	
//	public ColorPickerPredefinedDialog(VideoDisplayingActivity activity,
//			OnPredefinedColorSelectedListener listener) {
//		super(activity);
//		this.videoActivity = activity;
//		this.listener = listener;
//		bDisplayingActivity = false;
//		this.setTitle(AppUtil.getStringRes(R.string.color_picker));
//	}
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.color_picker_predefined);

		GridView gridViewColors = (GridView) findViewById(R.id.gridViewColors);
		final ColorPickerPredefindedAdapter adapter;
//		if(bDisplayingActivity)
			adapter = new ColorPickerPredefindedAdapter(activity, this);
//		else
//			adapter = new ColorPickerPredefindedAdapter(videoActivity, this);
		
		gridViewColors.setAdapter(adapter);
		gridViewColors.setOnItemClickListener(adapter);
		gridViewColors.setOnItemLongClickListener(adapter);

		Button pickAColorButton = (Button) findViewById(R.id.color_picker_predef_b_pick);
		pickAColorButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				if(bDisplayingActivity)
					activity.showColorPickDialog(adapter);
//				else
//					videoActivity.showColorPickDialog(adapter);
			}
		});
	}

	public void colorPicked(int color) {
		listener.onColorSelected(color);
	}
}
