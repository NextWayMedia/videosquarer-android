package com.videosquarer.squareimage.gui.element.colorpicker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.videosquarer.squareimage.util.AppUtil;

public class SatValPicker extends View implements Picker {
	private static final int PICKER_CIRCLE_SIZE = 10;
	private Picker.OnColorChangedListener mListener = null;
	private final Paint mColor = new Paint();
	private final Paint mGradient = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Paint mTrackerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private float mHue = 0;

	public SatValPicker(Context context, AttributeSet attrs) {
		super(context, attrs);

		mGradient.setStyle(Paint.Style.STROKE);
		mGradient.setStrokeWidth(0);

		mTrackerPaint.setStyle(Style.STROKE);
		mTrackerPaint.setStrokeWidth(AppUtil.convertToPixels(3));
		mTrackerPaint.setColor(Color.WHITE);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		applyChanges(w, h);
	}

	private void applyChanges(int w, int h) {
		Shader valGradient = new LinearGradient(0, 0, 0, h, Color.WHITE,
				Color.BLACK, Shader.TileMode.CLAMP);

		int rgb = Color.HSVToColor(new float[] { mHue, 1f, 1f });
		Shader satGradient = new LinearGradient(0, 0, w, 0, Color.WHITE, rgb,
				Shader.TileMode.CLAMP);

		ComposeShader shader = new ComposeShader(valGradient, satGradient,
				PorterDuff.Mode.MULTIPLY);

		mGradient.setShader(shader);
		invalidate();
	}

	@Override
	public void setColor(int color) {
		float[] hsv = color2HSV(color);
		mHue = hsv[0];

		mColor.setColor(color);
		applyChanges(getWidth(), getHeight());

	}

	public void setHue(int color) {
		float[] hsv = color2HSV(color);
		mHue = hsv[0];

		int curColor = mColor.getColor();
		float[] curHsv = color2HSV(curColor);
		curHsv[0] = mHue;

		mColor.setColor(Color.HSVToColor(curHsv));

		applyChanges(getWidth(), getHeight());
	}

	public int getColor() {
		return mColor.getColor();
	}

	@Override
	public void setOnColorChangedListener(Picker.OnColorChangedListener listener) {
		mListener = listener;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawPaint(mGradient);

		float[] hsv = color2HSV(mColor.getColor());

		float x = hsv[1] * getWidth();
		float y = (1 - hsv[2]) * getHeight();
		canvas.drawCircle(x, y, AppUtil.convertToPixels(PICKER_CIRCLE_SIZE), mTrackerPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			float sat = event.getX() / getWidth();
			float val = 1 - event.getY() / getHeight();
			mColor.setColor(Color.HSVToColor(0xFF, new float[] { mHue, sat, val }));
			mListener.colorChanged(mColor.getColor());
			invalidate();

			return true;
		}

		return false;
	}

	private float[] color2HSV(int color) {
		float[] hsv = new float[3];

		int red = Color.red(color);
		int green = Color.green(color);
		int blue = Color.blue(color);
		Color.RGBToHSV(red, green, blue, hsv);

		return hsv;
	}
}