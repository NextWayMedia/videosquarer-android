package com.videosquarer.squareimage.gui.element;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.DisplayingActivity;
import com.videosquarer.squareimage.util.BitmapWrapper;
import com.videosquarer.squareimage.util.ImageUtil;
import com.videosquarer.squareimage.util.task.RotatingTask;

public class SquareImageView extends ImageView {

	private static final String TAG = "SquareImageView";
	public static int smallestBorderSize;

	private Context context;

	private Rect imageRectangle = new Rect();

	private boolean isImageTurned;
	private boolean isRotatingInProgress;
	private int rotatePosition;
	private int color;
	private int scaledImageWidth;
	private int scaledImageHeight;
	private float ratio;

	private Bitmap watermark;
	private int rotatedKoef = 1;
	private int watermarkX, watermarkY;
	private boolean isPro;
	Paint translucentPaint;

	public SquareImageView(Context context) {
		super(context);
		this.context = context;
		smallestBorderSize = 0;
		init();
	}

	public SquareImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		smallestBorderSize = 0;
		init();
	}

	public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		smallestBorderSize = 0;
		init();
	}

	private void init() {
		translucentPaint = new Paint();
		translucentPaint.setAlpha(85);
		watermark = BitmapFactory.decodeResource(getResources(), R.drawable.watermark);
	}

	public void setBordersColor(int color) {
		this.color = color;
		invalidate();
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		int width = right - left;
		ViewGroup.LayoutParams params = this.getLayoutParams();
		params.height = width;
		this.setLayoutParams(params);
		this.setMeasuredDimension(width, width);

		super.onLayout(changed, left, top, right, top + width);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (getDrawable() == null) {
			return;
		}

		if (getWidth() != getHeight()) {
			Log.w(TAG, "Houston, we have a problem!");
			Log.w(TAG, "Width is " + getWidth());
			Log.w(TAG, "Height is " + getHeight());
		}

		Bitmap imageBitmap = ((BitmapDrawable) getDrawable()).getBitmap();
		calculateRatio(imageBitmap, canvas);
		setRectanglePoints(imageBitmap);

		if(DisplayingActivity.isBlur)
		{
			if(DisplayingActivity.blurBitmap != null)
			{
			    Rect rect = new Rect();

			    rect.left = 0;
			    rect.top = 0;
			    rect.bottom = getWidth();
			    rect.right = getHeight();

				canvas.drawBitmap(DisplayingActivity.blurBitmap, null, rect, null);
			}
		}
		else
			canvas.drawRGB(Color.red(color), Color.green(color), Color.blue(color));
		
		canvas.drawBitmap(imageBitmap, null, imageRectangle, null);
		if (!isPro) {
			watermarkX = canvas.getWidth() - watermark.getWidth() - 10;
			watermarkY = canvas.getHeight() - watermark.getHeight() - 10;
			canvas.drawBitmap(watermark, watermarkX, watermarkY, translucentPaint);
		}
	}

	private void setRectanglePoints(Bitmap imageBitmap) {
		if (imageBitmap.getWidth() == imageBitmap.getHeight()) {
			imageRectangle.left = smallestBorderSize;
			imageRectangle.top = smallestBorderSize;
			imageRectangle.right = getWidth() - smallestBorderSize;
			imageRectangle.bottom = getHeight() - smallestBorderSize;
		} else {
			imageRectangle.left = isImageTurned
					? smallestBorderSize
							: (getWidth() - scaledImageWidth) / 2;

			imageRectangle.top = isImageTurned
					? (getHeight() - scaledImageHeight) / 2
							: smallestBorderSize;

					imageRectangle.right = isImageTurned
							? getWidth() - smallestBorderSize
									: getWidth() - imageRectangle.left;

							imageRectangle.bottom = isImageTurned
									? getHeight() - imageRectangle.top
											: getHeight() - smallestBorderSize;
		}
	}

	private void calculateRatio(final Bitmap bm, Canvas canvas) {
		int imageViewWidth = getWidth();
		int imageViewHeight = getHeight();

		if (bm.getWidth() >= bm.getHeight()) {
			isImageTurned = true;
		} else {
			isImageTurned = false;
		}

		float scaledRatio = isImageTurned
				? (float) (imageViewWidth - smallestBorderSize * 2) / (float) bm.getWidth()
				: (float) (imageViewHeight - smallestBorderSize * 2) / (float) bm.getHeight();

		scaledImageWidth = Math.round(bm.getWidth() * scaledRatio);
		scaledImageHeight = Math.round(bm.getHeight() * scaledRatio);

		ratio = isImageTurned
				? (float) bm.getWidth() / (float) scaledImageWidth
				: (float) bm.getHeight() / (float) scaledImageHeight;

		int newWidth = canvas.getWidth() / 3;
		int newHeight = (int)((float)newWidth / watermark.getWidth() * watermark.getHeight());
		Bitmap scaledWatermark = BitmapWrapper.createScaledBitmap(watermark, newWidth, newHeight, true);
		watermark.recycle();
		System.gc();
		watermark = scaledWatermark;

//						if (!isPro) {
//							if (!isImageTurned && watermark.getWidth() > watermark.getHeight()) {
//								watermark = ImageUtil.rotateImage(watermark, 90);
//								rotatedKoef = -1;
//							}
//
//							if (isImageTurned && watermark.getWidth() > canvas.getWidth() / 3) {
//								int newWidth = canvas.getWidth() / 3;
//								int newHeight = (int)((float)newWidth / watermark.getWidth() * watermark.getHeight());
//								watermark = Bitmap.createScaledBitmap(watermark, newWidth, newHeight, true);
//							} else if (!isImageTurned && watermark.getHeight() > canvas.getHeight() / 3) {
//								int newHeight = canvas.getHeight() / 3;
//								int newWidth = (int)((float)newHeight / watermark.getHeight() * watermark.getWidth());
//								watermark = Bitmap.createScaledBitmap(watermark, newWidth, newHeight, true);
//							}
//						}

		Log.v(TAG, "Showed image width: " + scaledImageWidth);
		Log.v(TAG, "Showed image height: " + scaledImageHeight);

		Log.v(TAG, "ImageView width is: " + imageViewWidth);
		Log.v(TAG, "ImageView height is: " + imageViewHeight);
	}

	public void rotateImage() {
		if (!isRotatingInProgress) {
			//watermark = ImageUtil.rotateImage(watermark, (rotatePosition % 2 == 0 ? 90 : -90) * rotatedKoef);
			new RotatingTask(this,
					((BitmapDrawable) getDrawable()).getBitmap()).execute();
		}

		if (rotatePosition == 3) {
			rotatePosition = 0;
		} else {
			rotatePosition++;
		}
	}

	public boolean isRotatingInProgress() {
		return isRotatingInProgress;
	}

	public void setIsRotatingInProgress(boolean isRotatingInProgress) {
		this.isRotatingInProgress = isRotatingInProgress;
	}

	public float getRatio() {
		return ratio;
	}

	public int getRotatePosition() {
		return rotatePosition;
	}

	public int getScaledSize() {
		return isImageTurned ? scaledImageWidth : scaledImageHeight;
	}

	public int getWatermarkX() {
		return watermarkX;
	}

	public int getWatermarkY() {
		return watermarkY;
	}

	public Bitmap getWatermark() {
		return watermark;
	}

	public boolean isWatermarkVertical() {
		return watermark.getWidth() < watermark.getHeight();
	}

	public int getWatermarkWidth() {
		return watermark.getWidth();
	}

	public int getWatermarkHeight() {
		return watermark.getHeight();
	}

	public boolean isVideoVertical() {
		return !isImageTurned;
	}

	public void setPro(boolean isPro) {
		this.isPro = isPro;
	}

	public Bitmap getMaskBitmap(DisplayingActivity activity, int size) {
		Bitmap bitmap = ((BitmapDrawable)getDrawable()).getBitmap();
		if(bitmap == null)
			return null;

		return ImageUtil.makeSquareMaskBitmap(bitmap, size, color, activity, isPro);
	}

	public Bitmap getBackgroundBitmap(DisplayingActivity activity, int size) {
		Bitmap bitmap = ((BitmapDrawable)getDrawable()).getBitmap();
		if(bitmap == null)
			return null;

		return ImageUtil.makeSquareBackgroundBitmap(bitmap, size, color, activity, true);
	}

	public Rect getVideoRect()
	{
		return imageRectangle;
	}
	
	public Bitmap getBitmap()
	{
		Bitmap imageBitmap = ((BitmapDrawable) getDrawable()).getBitmap();
		return imageBitmap;
	}
}
