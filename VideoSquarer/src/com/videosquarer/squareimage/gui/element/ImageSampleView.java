package com.videosquarer.squareimage.gui.element;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ImageSampleView extends ImageView{

	public ImageSampleView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public ImageSampleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public ImageSampleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		setMeasuredDimension(getMeasuredWidth(), (int)(getMeasuredWidth()));
	}

	
}
