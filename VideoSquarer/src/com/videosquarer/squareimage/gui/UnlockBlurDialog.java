package com.videosquarer.squareimage.gui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.videosquarer.squareimage.R;

public class UnlockBlurDialog extends Dialog{
    private IOnBuyBlurClickListener clickListener;

    public UnlockBlurDialog(Context context, IOnBuyBlurClickListener clickListener) {
        super(context, R.style.FullScreenDialog);
        this.clickListener = clickListener;
    }

    public UnlockBlurDialog(Context context, int theme) {
        super(context, theme);
    }

    protected UnlockBlurDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @SuppressLint("InflateParams")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_unlock_blur, null);

        Button btnBuy = (Button) view.findViewById(R.id.btnBuy);
        Button btnLater = (Button)view.findViewById(R.id.btnLater);
        Button btnOther = (Button)view.findViewById(R.id.btnOtherFeatures);
        
//        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams)btnLater.getLayoutParams();
//        param.width = btnBuy.getWidth();
//        btnLater.setLayoutParams(param);
//        
//        param = (RelativeLayout.LayoutParams)btnOther.getLayoutParams();
//        param.width = btnBuy.getWidth();
//        btnOther.setLayoutParams(param);

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onBuyBlurClicked();
                }
                dismiss();
            }
        });

        btnLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setContentView(view);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public interface IOnBuyBlurClickListener {
        void onBuyBlurClicked();
    }

}
