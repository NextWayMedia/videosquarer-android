package com.videosquarer.squareimage.gui;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.ads.AdView;
import com.videosquarer.squareimage.R;
import com.videosquarer.squareimage.gui.element.SquareImageView;
import com.videosquarer.squareimage.util.AppUtil;
import com.videosquarer.squareimage.util.IabHelper;
import com.videosquarer.squareimage.util.IabResult;
import com.videosquarer.squareimage.util.Inventory;
import com.videosquarer.squareimage.util.Purchase;

public class UnlockProActivity extends SherlockActivity {

    private static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk0VeSP22Zf2U5DeC5VyvrEDwssiQvOV7B4dyM+eDCov53L1aLYOihNwvW1FDPc80r89yfjcQij6SHEhxA5sjpOysCQUgmTI5USxUC1TDtz5bzvTsYtSMuCa2z9OpWk2NieMyBC4bJwWDTVP7U4d6WAM7mFHvonb2IWx+iCuARebPM1+1jsLU9Rp9wa2ViIik3iCLiiI9O5jK3PFywC0bfORVRgYCUv6AoxvSEc3ow6ef0RB1KiYdXycI4rhWazZGFYmTNlrPjzOnABJ/tWRqLBYixY26VIgC79GWEQrdhXXeU8lc4TPXUIzzvNUl8su6WlK1uNCx9ckq6Wo202el4wIDAQAB";
    private static final String TAG = UnlockProActivity.class.getSimpleName();

    private static final String SKU_PRO = "videosquarerpro";
    private static final int RC_REQUEST = 10000;
	public static final String SKU_BLUR = "BlurBG";
	private static final String BLUR_KEY = "PURCHASED_BLUR";
	public static final int RC_BLUR_REQUEST = 10001;
	public static boolean isBuyBlur = false;
	private boolean isNoFirstTime = false;
	private static final String FIRSTTIME_KEY = "LAUNCHED";

    IabHelper mHelper;
    static boolean isPro = false;
    Button btnUnlockPro;
    AdView adView;
    SquareImageView squareImageView;

    @Override
    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().setCustomView(R.layout.unlock_pro_view);
        getSupportActionBar().setTitle(R.string.title_activity_main);

        btnUnlockPro = (Button) findViewById(R.id.btnUnlockPro);
        adView = (AdView) findViewById(R.id.adView);
        btnUnlockPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUnlockProDialog();
            }
        });
        squareImageView = (SquareImageView) findViewById(R.id.act_displaying_siv_square);

		isBuyBlur = AppUtil.getSharedPrefBool(BLUR_KEY);
		isNoFirstTime = AppUtil.getSharedPrefBool(FIRSTTIME_KEY);

        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);
        try
        {
        	mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
        		public void onIabSetupFinished(IabResult result) {
        			if (!result.isSuccess()) {
        				Log.e(TAG, "Problem setting up in-app billing: " + result);
        				return;
        			}
        			if (mHelper == null) return;
        			mHelper.queryInventoryAsync(mGotInventoryListener);
        		}
        	});
        }
        catch(NullPointerException e)
        {
        	e.printStackTrace();
        }
        
		if(!isNoFirstTime)
		{
			showBuyBlurDialog();
			isNoFirstTime = true;
			AppUtil.putSharedPref(FIRSTTIME_KEY, isNoFirstTime);
		}

    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (mHelper == null) {
                return;
            }

            // Is it a failure?
            if (result.isFailure()) {
                Log.e(TAG, "Failed to query inventory: " + result);
                return;
            }

            Purchase proPurchase = inventory.getPurchase(SKU_PRO);
            isPro = (proPurchase != null && verifyDeveloperPayload(proPurchase));
            updateUI();
            
			Purchase blurpurchase = inventory.getPurchase(SKU_BLUR);
			if(blurpurchase != null && verifyDeveloperPayload(blurpurchase))
			{
				isBuyBlur = true;
				AppUtil.putSharedPref(BLUR_KEY, isBuyBlur);
			}
			else
				isBuyBlur = false;

        }
    };

    private void updateUI() {
        int visibility = isPro ? View.GONE : View.VISIBLE;
        btnUnlockPro.setVisibility(visibility);
        adView.setVisibility(visibility);
        if (squareImageView != null) {
            squareImageView.setPro(isPro);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null) return;

        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                Log.e(TAG, result.getMessage());
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                return;
            }

            if (purchase.getSku().equals(SKU_PRO)) {
                isPro = true;
                updateUI();
            }
            
			if (purchase.getSku().equals(SKU_BLUR)) {
				isBuyBlur = true;
				AppUtil.putSharedPref(BLUR_KEY, isBuyBlur);
			}
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        try
        {
        	if (mHelper != null) {
        		mHelper.dispose();
        		mHelper = null;
        	}
        }
        catch(NullPointerException e)
        {
        	e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
        	e.printStackTrace();
        }
    }

    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /**
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

	public void showBuyBlurDialog() {
		new UnlockBlurDialog(this, new UnlockBlurDialog.IOnBuyBlurClickListener() {
			
			@Override
			public void onBuyBlurClicked() {
				// TODO Auto-generated method stub
                if (mHelper == null) return;
				if (!mHelper.isSetuped()) {
					Toast.makeText(UnlockProActivity.this, R.string.billing_not_available, Toast.LENGTH_SHORT).show();
					return;
				}

                String payload = "";
                try
                {
                	mHelper.launchPurchaseFlow(UnlockProActivity.this, SKU_BLUR, RC_BLUR_REQUEST, mPurchaseFinishedListener, payload);
                }
                catch(IllegalStateException e)
                {
                	e.printStackTrace();
                	return;
                }
			}
		
		}).show();
	}

    private void showUnlockProDialog() {
        new UnlockProDialog(this, new UnlockProDialog.IOnButtonClickListener() {
            @Override
            public void onUnlockClicked() {
                if (mHelper == null) return;
                String payload = "";
                mHelper.launchPurchaseFlow(UnlockProActivity.this, SKU_PRO, RC_REQUEST, mPurchaseFinishedListener, payload);
            }
        }).show();
    }
}
